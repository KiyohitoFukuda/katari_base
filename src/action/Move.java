package action;

import java.util.ArrayList;

import story_object.Agent;
import story_object.CharacterObject;
import story_object.Environment;
import story_object.ItemObject;
import util.Function;
import util.Log;
import util.Parameter;
import util.Selection;

public class Move {


// 関数
	private Function  func_;
// 選択
	private Selection select_;


/**
 * コンストラクタ
 */
	public Move(){
		this.func_   = new Function();
		this.select_ = new Selection();
	}







/**
 * 他のオブジェクトが持つ影響度に従って移動する方向を決定し, そちらに移動する (登場人物の移動).
 * ノイマン近傍に移動する.
 * @param cAgent
 * @param aList
 */
	public void CharacterMove(CharacterObject cAgent, ArrayList<Agent> aList){
	// 各方向に移動する値 [0]:上(↑)　[1]:右(→)　[2]:下(↓)　[3]:左(←)
		double[] values = new double[4];
		int[]    myPos  = new int[2];
		int      myId   = cAgent.getObjectID();
		myPos[0] = cAgent.getFPositionX();
		myPos[1] = cAgent.getFPositionY();

		double pValue = cAgent.getPersonality()[1];
		for ( int i = 0; i < aList.size(); i++ ){
			int[]  pos  = aList.get(i).getFPosition();
			double dirX = (double)myPos[0] - (double)pos[0];
			double dirY = (double)myPos[1] - (double)pos[1];
			if ( (!cAgent.checkInterval(aList.get(i).getObjectID())) ){
				double dValueX = Math.max(1.0-(Math.abs(dirX)*(2.0-pValue)/Parameter.getFieldSizeX()), 0.0);
				double dValueY = Math.max(1.0-(Math.abs(dirY)*(2.0-pValue)/Parameter.getFieldSizeY()), 0.0);
				double iValueX = aList.get(i).getInfluenceValue(myId) * dValueX;
				double iValueY = aList.get(i).getInfluenceValue(myId) * dValueY;
				if ( cAgent.getCharacterID() == (i+1)
						|| cAgent.getSceneID() == (i+1) ){
					iValueX = 0.0;
					iValueY = 0.0;
				}
				if ( dirX > 0 ){
					values[3] += iValueX;
				}
				if ( dirX < 0 ){
					values[1] += iValueX;
				}
				if ( dirY > 0 ){
					values[2] += iValueY;
				}
				if ( dirY < 0 ){
					values[0] += iValueY;
				}
			}
		}
	// フィールド上の端にいる場合の対応
		if ( myPos[0] == Parameter.getFieldSizeX()-1 ){
			values[1] = 0.0;
		}
		if ( myPos[0] == 0 ){
			values[3] = 0.0;
		}
		if ( myPos[1] == Parameter.getFieldSizeY()-1 ){
			values[0] = 0.0;
		}
		if ( myPos[1] == 0 ){
			values[2] = 0.0;
		}

		boolean isOK      = false;
		int[]   newPos    = new int[2];
		int[]   changePos = new int[2];
		while ( !isOK ){
			newPos[0] = myPos[0];
			newPos[1] = myPos[1];
			int dIndex = this.select_.rouletteSelection(values);
			if ( dIndex == 0 ){
				newPos[1]++;
				changePos[1] = 1;
			}
			if ( dIndex == 1 ){
				newPos[0]++;
				changePos[0] = 1;
			}
			if ( dIndex == 2 ){
				newPos[1]--;
				changePos[1] = -1;
			}
			if ( dIndex == 3 ){
				newPos[0]--;
				changePos[0] = -1;
			}
			if ( (newPos[0]!=-1) && (newPos[0]!=Parameter.getFieldSizeX()) ){
				if ( (newPos[1]!=-1) && (newPos[1]!=Parameter.getFieldSizeY()) ){
					isOK = true;
				}
			}
		}
		cAgent.setFPosition(newPos);

		String[] change = new String[10];
		change[0] = "(" + changePos[0] + "_" + changePos[1] + ")";
		change[1] = String.format("%.3f", 0.000);
		change[2] = String.format("%.3f", 0.000);
		change[3] = String.format("%.3f", 0.000);
		change[4] = "[]";
		change[5] = "(0_0)";
		change[6] = String.format("%.3f", 0.000);
		change[7] = String.format("%.3f", 0.000);
		change[8] = String.format("%.3f", 0.000);
		change[9] = "[]";
		Log.saveObjectAction((Agent)cAgent, 0, 1, Parameter.getActionNumber(), change);
		Parameter.setActionNumber(Parameter.getActionNumber()+1);
	}







/**
 * 他のオブジェクトが持つ影響度に従って移動する方向を決定し, そちらに移動する (アイテムの移動).
 * ノイマン近傍に移動する.
 * 自身を所有するオブジェクトが存在した場合, そのオブジェクトの後を追う.
 * @param iAgent
 * @param aList
 */
	public void ItemMove(ItemObject iAgent, ArrayList<Agent> aList){
	// 各方向に移動する値 [0]:上(↑)　[1]:右(→)　[2]:下(↓)　[3]:左(←)
		double[] values    = new double[4];
		int[]    myPos     = new int[2];
		int[]    changePos = new int[2];
		int      myId      = iAgent.getObjectID();
		myPos[0] = iAgent.getFPositionX();
		myPos[1] = iAgent.getFPositionY();

		if ( iAgent.getOwnerID() != 0 ){
			int[] newPos   = new int[2];
			int[] ownerPos = aList.get(iAgent.getOwnerID()-1).getFPosition();
			newPos[0]    = ownerPos[0];
			newPos[1]    = ownerPos[1];
			changePos[0] = newPos[0] - myPos[0];
			changePos[1] = newPos[1] - myPos[1];
			iAgent.setFPosition(newPos);
		}else{
			for ( int i = 0; i < aList.size(); i++ ){
				int[]  pos  = aList.get(i).getFPosition();
				double dirX = (double)myPos[0] - (double)pos[0];
				double dirY = (double)myPos[1] - (double)pos[1];
				double dValueX = Math.max(1.0-(Math.abs(dirX)*1.5/Parameter.getFieldSizeX()), 0.0);
				double dValueY = Math.max(1.0-(Math.abs(dirY)*1.5/Parameter.getFieldSizeY()), 0.0);
				double iValueX = aList.get(i).getInfluenceValue(myId) * dValueX;
				double iValueY = aList.get(i).getInfluenceValue(myId) * dValueY;
				if ( dirX > 0 ){
					values[3] += iValueX;
				}
				if ( dirX < 0 ){
					values[1] += iValueX;
				}
				if ( dirY > 0 ){
					values[2] += iValueY;
				}
				if ( dirY < 0 ){
					values[0] += iValueY;
				}
			}
		// フィールド上の端にいる場合の対応
			if ( myPos[0] == Parameter.getFieldSizeX()-1 ){
				values[1] = 0.0;
			}
			if ( myPos[0] == 0 ){
				values[3] = 0.0;
			}
			if ( myPos[1] == Parameter.getFieldSizeY()-1 ){
				values[0] = 0.0;
			}
			if ( myPos[1] == 0 ){
				values[2] = 0.0;
			}

			boolean isOK      = false;
			int[]   newPos    = new int[2];
			while ( !isOK ){
				newPos[0] = myPos[0];
				newPos[1] = myPos[1];
				int dIndex = this.select_.rouletteSelection(values);
				if ( dIndex == 0 ){
					newPos[1]++;
					changePos[1] = 1;
				}
				if ( dIndex == 1 ){
					newPos[0]++;
					changePos[0] = 1;
				}
				if ( dIndex == 2 ){
					newPos[1]--;
					changePos[1] = -1;
				}
				if ( dIndex == 3 ){
					newPos[0]--;
					changePos[0] = -1;
				}
				if ( (newPos[0]!=-1) && (newPos[0]!=Parameter.getFieldSizeX()) ){
					if ( (newPos[1]!=-1) && (newPos[1]!=Parameter.getFieldSizeY()) ){
						isOK = true;
					}
				}
			}
			iAgent.setFPosition(newPos);
		}

		String[] change = new String[10];
		change[0] = "(" + changePos[0] + "_" + changePos[1] + ")";
		change[1] = String.format("%.3f", 0.000);
		change[2] = String.format("%.3f", 0.000);
		change[3] = String.format("%.3f", 0.000);
		change[4] = "[]";
		change[5] = "(0_0)";
		change[6] = String.format("%.3f", 0.000);
		change[7] = String.format("%.3f", 0.000);
		change[8] = String.format("%.3f", 0.000);
		change[9] = "[]";
		Log.saveObjectAction((Agent)iAgent, 0, 1, Parameter.getActionNumber(), change);
		Parameter.setActionNumber(Parameter.getActionNumber()+1);
	}







/**
 * ランダムに移動する. (環境エージェントの移動)
 * ノイマン近傍に移動する.
 * @param env
 */
	public void EnvironmentMove(Environment env){
	// 各方向に移動する値 [0]:上(↑)　[1]:右(→)　[2]:下(↓)　[3]:左(←)
		double[] values = new double[4];
		int[]    myPos  = new int[2];
		myPos[0] = env.getFPositionX();
		myPos[1] = env.getFPositionY();

		for ( int i = 0; i < values.length; i++ ){
			values[i] = 1.0;
		}
	// フィールド上の端にいる場合の対応
		if ( myPos[0] == Parameter.getFieldSizeX()-1 ){
			values[1] = 0.0;
		}
		if ( myPos[0] == 0 ){
			values[3] = 0.0;
		}
		if ( myPos[1] == Parameter.getFieldSizeY()-1 ){
			values[0] = 0.0;
		}
		if ( myPos[1] == 0 ){
			values[2] = 0.0;
		}

		boolean isOK      = false;
		int[]   newPos    = new int[2];
		int[]   changePos = new int[2];
		while ( !isOK ){
			newPos[0] = myPos[0];
			newPos[1] = myPos[1];
			int dIndex = this.select_.rouletteSelection(values);
			if ( dIndex == 0 ){
				newPos[1]++;
				changePos[1] = 1;
			}
			if ( dIndex == 1 ){
				newPos[0]++;
				changePos[0] = 1;
			}
			if ( dIndex == 2 ){
				newPos[1]--;
				changePos[1] = -1;
			}
			if ( dIndex == 3 ){
				newPos[0]--;
				changePos[0] = -1;
			}
			if ( (newPos[0]!=-1) && (newPos[0]!=Parameter.getFieldSizeX()) ){
				if ( (newPos[1]!=-1) && (newPos[1]!=Parameter.getFieldSizeY()) ){
					isOK = true;
				}
			}
		}
		env.setFPosition(newPos);
	}

}







