package action;

import java.util.ArrayList;

import story_object.Agent;
import story_object.CharacterObject;
import story_object.Environment;
import story_object.ItemObject;
import util.Function;
import util.Log;
import util.Parameter;
import util.Selection;

public class Interaction {

// 関数
	private Function func_;
// 選択
	private Selection select_;



	public Interaction(){
		this.func_   = new Function();
		this.select_ = new Selection();
	}







	public void characterInteraction(CharacterObject agent1, CharacterObject agent2, ArrayList<Agent> aList){
		int    id1     = agent1.getObjectID();
		int    id2     = agent2.getObjectID();
		double fValue1 = agent1.getFriendshipValue(agent2.getObjectID());
		double fValue2 = agent2.getFriendshipValue(agent1.getObjectID());
		double pValue  = agent2.getPersonality()[0];

		double myu     = 5 * (fValue1+fValue2) / (2*Parameter.getMaxFriendshipValue());
		double sigma   = 10 * (1+pValue);
		double frValue = this.func_.randomGaussian(myu, sigma);
		double faValue = 0.5 * Math.abs(frValue);

	// 所持アイテムの処理
		Integer itemNum = Integer.valueOf(0);
		boolean isItem  = false;
		if ( agent1.getRole() == 1 ){
		// 主人公 - 敵の場合
			if ( (agent2.getRole()==3) && (this.func_.randomDouble(0, 1)<0.5) ){
				ArrayList<Integer> items = agent1.getItemList();
				for ( int i = 0; i < items.size(); i++ ){
					if ( aList.get(items.get(i)-1).getRole() == 5 ){
						itemNum = items.get(i);
						isItem  = true;
						break;
					}
				}
			}
		// 主人公 - 仲間の場合
			if ( (agent2.getRole()==4) && (this.func_.randomDouble(0, 1)<0.5) ){
				ArrayList<Integer> items = agent1.getItemList();
				for ( int i = 0; i < items.size(); i++ ){
					if ( aList.get(items.get(i)-1).getRole() == 6 ){
						itemNum = items.get(i);
						isItem  = true;
						break;
					}
				}
			}
		}
	// 条件を満たすアイテムを持っていれば, そのアイテムを渡すことで変化量が変化する
		if ( itemNum != 0 ){
			frValue += this.func_.randomDouble(20, 30) * pValue;
			faValue += 0.5 * frValue;
			agent1.removeItem(itemNum);
			agent2.addItem(itemNum);
			ItemObject item = (ItemObject)aList.get(itemNum-1);
			item.renewOwner(id2);
		}

		agent2.changeFriendshipValue(frValue, id1);
		agent2.changeFamiliarityValue(faValue, id1);
		agent2.changeEmotion(frValue/100.0);
		agent1.resetInterval(id2, 50, 100);
		agent2.resetInterval(id1, 50, 100);

		String[] change = new String[10];
		change[0] = "(0_0)";
		change[1] = String.format("%.3f", 0.000);
		change[2] = String.format("%.3f", 0.000);
		change[3] = String.format("%.3f", 0.000);
		if ( isItem ){
			change[4] = "[-" + itemNum + "]";
		}else{
			change[4] = "[]";
		}
		change[5] = "(0_0)";
		change[6] = String.format("%.3f", frValue);
		change[7] = String.format("%.3f", faValue);
		change[8] = String.format("%.3f", frValue/100.0);
		if ( isItem ){
			change[9] = "[" + itemNum + "]";
		}else{
			change[9] = "[]";
		}
		if ( isItem ){
			Log.saveObjectAction((Agent)agent1, id2, 3, Parameter.getActionNumber(), change);
		}else{
			Log.saveObjectAction((Agent)agent1, id2, 2, Parameter.getActionNumber(), change);
		}
		Parameter.setActionNumber(Parameter.getActionNumber()+1);
	}







	public void itemInteraction(CharacterObject agent1, ItemObject agent2){
		int    id1    = agent1.getObjectID();
		int    id2    = agent2.getObjectID();
		double pValue = agent1.getPersonality()[0];
		double eValue = pValue * this.func_.randomGaussian(0, 2*(1+pValue));

		agent1.addItem(id2);
		agent2.renewOwner(id1);
		agent1.changeEmotion(eValue);

		String[] change = new String[10];
		change[0] = "(0_0)";
		change[1] = String.format("%.3f", 0.000);
		change[2] = String.format("%.3f", 0.000);
		change[3] = String.format("%.3f", eValue/100.0);
		change[4] = "[" + id2 + "]";
		change[5] = "(0_0)";
		change[6] = String.format("%.3f", 0.000);
		change[7] = String.format("%.3f", 0.000);
		change[8] = String.format("%.3f", 0.000);
		change[9] = "[-" + id2 + "]";
		Log.saveObjectAction((Agent)agent1, id2, 4, Parameter.getActionNumber(), change);
		Parameter.setActionNumber(Parameter.getActionNumber()+1);
	}







	public void environmentInteraction(Environment env, CharacterObject agent2){
		int    id1     = env.getEnvironmentID();
		int    id2     = agent2.getObjectID();
		double pValue  = agent2.getPersonality()[0] + 2.0;
		double frValue = pValue * this.func_.randomGaussian(0, env.getInfluenceValue());

		int oID = id2;
		while ( oID == id2 ){
			oID = this.func_.randomInt(1, agent2.getFrinedshipValues().length);
		}
		agent2.changeFriendshipValue(frValue, oID);
		agent2.changeEmotion(frValue);
	}







	public void itemUse(CharacterObject agent, ItemObject item){
		int    id1   = agent.getObjectID();
		int    id2   = item.getObjectID();
		double value = item.getCondition() / 2.0;

		agent.addCondition(value);
		agent.removeItem(Integer.valueOf(id2));
		agent.resetInterval(id2, 500, 600);
		item.resetOwner();
		item.reset(item.getCondition());

		String[] change = new String[10];
		change[0] = "(0_0)";
		change[1] = String.format("%.3f", 0.000);
		change[2] = String.format("%.3f", 0.000);
		change[3] = String.format("%.3f", 0.000);
		change[4] = "[-" + id2 + "]";
		change[5] = "(0_0)";
		change[6] = String.format("%.3f", 0.000);
		change[7] = String.format("%.3f", 0.000);
		change[8] = String.format("%.3f", 0.000);
		change[9] = "[" + id2 + "]";
		Log.saveObjectAction((Agent)agent, id2, 5, Parameter.getActionNumber(), change);
		Parameter.setActionNumber(Parameter.getActionNumber()+1);
	}

}







