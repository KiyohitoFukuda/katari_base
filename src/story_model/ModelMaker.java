package story_model;

import java.util.ArrayList;
import java.util.HashMap;

import util.Log;
import util.Parameter;

public class ModelMaker {

	private String[] sceParams_ = {"SID", "場面", "オブジェクト", "場所", "時間", "関係性"};		// シーンパラメータ
	private String[] objParams_ = {"OID", "種別", "役割", "名前", "年齢", "性別", "性格", "感情",		// オブジェクトパラメータ
			"姿勢", "表情", "状態", "物理的位置"};
	private String[] relParams_ = {"RID", "行動", "行動番号", "因果関係", "結果", "心情", "立場"};		// 関係性パラメータ
	private String[] p1_        = {"敏感", "鈍感"};		// 性格[0]
	private String[] p2_        = {"好奇心旺盛な", "用心深い"};		// 性格[1]
	private String[] pos_       = {"0-5-5-5-", "5-5-5-5-", "2-4-5-5-",			// 物理的位置
			"7-8-3-2-", "1-6-3-3-", "6-6-3-3-", "2-5-5-5-", "3-5-5-5-"};		// 現在は決まった位置に固定

	private HashMap<String, String[]> sModels_;			// ストーリーモデル (シーン)
	private HashMap<String, String[][]> oModels_;		// ストーリーモデル (オブジェクト)
	private HashMap<String, String[][]> rModels_;		// ストーリーモデル (関係性)
	private String[]   scene_;			// シーン
	private String[][] object_;			// オブジェクト
	private String[][] relation_;		// 関係性

	private int all_;		// 全エージェント数 + シーン数
	private int sID_;		// SID (場面番号)
	private int aID_;		// AID (行動)

	private int[] lTurn_ = new int[6];		// 各イベントの最初のターン数
	private int[] fTurn_ = new int[6];		// 各イベントの最後のターン数
	private int   bAct_  = 0;		// 以前の行動

	private LogReader reader_;		// ログリーダ

// フラグ
	private boolean encountEnemy1_ = false;		// 敵との遭遇の有無
	private int     encountEnemy2_ = 0;			// 敵との遭遇の場面番号
	private boolean encountAlly1_  = false;		// 仲間との遭遇の有無
	private int     encountAlly2_  = 0;			// 仲間との遭遇の場面番号
	private boolean getFood1_    = false;		// 食べ物の取得の有無
	private int     getFood2_    = 0;			// 食べ物の取得の場面番号
	private boolean passFood1_   = false;		// 食べ物の譲渡の有無
	private int     passFood2_   = 0;			// 食べ物の譲渡の場面番号
	private boolean getWeapon1_  = false;		// 武器の取得の有無
	private int     getWeapon2_  = 0;			// 武器の取得の場面番号
	private boolean passWeapon1_ = false;		// 武器の譲渡の有無
	private int     passWeapon2_ = 0;			// 武器の譲渡の場面番号


/**
 * コンストラクタ
 * @param cNum
 * @param iNum
 * @param sNum
 */
	public ModelMaker(int cNum, int iNum, int sNum){
		this.sModels_ = new HashMap<String, String[]>();
		this.oModels_ = new HashMap<String, String[][]>();
		this.rModels_ = new HashMap<String, String[][]>();
		this.all_ = cNum + iNum + sNum;
		this.sID_ = 1;
		this.aID_ = 1;

		this.reader_ = new LogReader();

		for ( int i = 0; i < this.lTurn_.length; i++ ){
			this.lTurn_[i] = 0;
			this.fTurn_[i] = 0;
		}
	}







/**
 * アクションログ, ステータスログからストーリーモデルを生成する.
 * @param aLogFile
 * @param sLogFile
 */
	public void makeStoryModel(String aLogFile, String sLogFile){
		this.reader_.readStatusLog(sLogFile);
		this.reader_.readActionLog(aLogFile);
		String[] sParams = this.reader_.getStatusParameter();
		String[] aParams = this.reader_.getActionParameter();
//		for ( int i = 0; i < sParams.length; i++ ){
//			System.out.print(sParams[i] + "--");
//		}
//		System.out.println();

	// スタート処理
		String[] startStatus = this.reader_.getStatusLog(1, 0);
	// スタート　シーン
		String key1 = "S-" + this.sID_;
		this.scene_ = new String[this.sceParams_.length];
		this.scene_[0] = String.valueOf(this.sID_);
		this.scene_[1] = startStatus[12];
		this.scene_[2] = "O-" + this.sID_;
		this.scene_[3] = "赤ずきんの家の前";
		this.scene_[4] = "朝";
		this.scene_[5] = "R-" + this.sID_;
		this.sModels_.put(key1, this.scene_);

	// スタート　オブジェクト
		String key2 = "O-" + this.sID_;
		this.object_ = new String[1][this.objParams_.length];
		this.object_[0] = this.createObjectModel(startStatus, 1);
		this.oModels_.put(key2, this.object_);

	// スタート　関係性
		String key3 = "R-" + this.sID_;
		this.relation_ = new String[1][this.relParams_.length];
		this.relation_[0][0] = "1_1";
		this.relation_[0][1] = "null";
		this.relation_[0][2] = "null";
		this.relation_[0][3] = "開始";
		this.relation_[0][4] = "null";
		this.relation_[0][5] = "null";
		this.relation_[0][6] = Parameter.getRoleName(Integer.valueOf(startStatus[4]));
		this.rModels_.put(key3, this.relation_);

		this.sID_++;

	// ターン経過 (イベント蓄積)
		for ( int t = 1; t < Parameter.getTurn(); t++ ){
			String[] sLog = this.reader_.getStatusLog(1, t);
			if ( !sLog[12].equals("スタート") && !sLog[12].equals("森の中") ){
				if ( sLog[12].contains("2") ){
					this.encountEnemy1_ = true;
					this.lTurn_[0] = Integer.valueOf(sLog[0]) - 1;
					if ( this.fTurn_[0] == 0 ){
						this.fTurn_[0] = Integer.valueOf(sLog[0]) - 1;
					}
				}
				if ( sLog[12].contains("3") ){
					this.encountAlly1_ = true;
					this.lTurn_[1] = Integer.valueOf(sLog[0]) - 1;
					if ( this.fTurn_[1] == 0 ){
						this.fTurn_[1] = Integer.valueOf(sLog[0]) - 1;
					}
				}
			}

			ArrayList<String[]> actionLogs = this.reader_.getActionLog(1, t);
			for ( int j = 0; j < actionLogs.size(); j++ ){
				if ( actionLogs.get(j) != null ){
					String[] aLog = actionLogs.get(j);
					if ( aLog[1].equals("4") ){
						if ( this.reader_.getDataOfStatusLog(Integer.valueOf(aLog[4]), 0, sParams[4]).equals("5") ){
							this.getFood1_ = true;
							this.lTurn_[2]  = Integer.valueOf(aLog[0]);
							if ( this.fTurn_[2] == 0 ){
								this.fTurn_[2] = Integer.valueOf(aLog[0]);
							}
						}
						if ( this.reader_.getDataOfStatusLog(Integer.valueOf(aLog[4]), 0, sParams[4]).equals("6") ){
							this.getWeapon1_ = true;
							this.lTurn_[4]    = Integer.valueOf(aLog[0]);
							if ( this.fTurn_[4] == 0 ){
								this.fTurn_[4] = Integer.valueOf(aLog[0]);
							}
						}
					}
					if ( aLog[1].equals("3") ){
						String item = aLog[16].split("\\[")[1].split("\\]")[0];
						if ( this.reader_.getDataOfStatusLog(Integer.valueOf(item), 0, sParams[4]).equals("5") ){
							this.passFood1_ = true;
							this.passFood2_ = t;
							this.lTurn_[3]  = Integer.valueOf(aLog[0]);
							if ( this.fTurn_[3] == 0 ){
								this.fTurn_[3] = Integer.valueOf(aLog[0]);
							}
						}
						if ( this.reader_.getDataOfStatusLog(Integer.valueOf(item), 0, sParams[4]).equals("6") ){
							this.passWeapon1_ = true;
							this.lTurn_[5]    = Integer.valueOf(aLog[0]);
							if ( this.fTurn_[5] == 0 ){
								this.fTurn_[5] = Integer.valueOf(aLog[0]);
							}
						}
					}
				}
			}
		}

	// イベント処理
		ArrayList<Integer> tabooList = new ArrayList<Integer>();
		for ( int i = 0; i < this.fTurn_.length; i++ ){
			int num = 0;
			int min = 99999;
			for ( int j = 0; j < this.fTurn_.length; j++ ){
				if ( tabooList.contains(j) ){
					continue;
				}
				if ( min > this.fTurn_[j] ){
					num = j;
					min = this.fTurn_[num];
				}
			}
//			System.out.println(this.fTurn_[i] + "--" + this.lTurn_[i] + "--" + num);
			tabooList.add(num);
			if ( this.fTurn_[num] == 0 ){
				continue;
			}

		// オオカミとの遭遇, 食べ物の譲渡
			if ( num == 0 ){
				String[] fLog1 = this.reader_.getStatusLog(1, this.fTurn_[num]);
				String[] fLog2 = this.reader_.getStatusLog(2, this.fTurn_[num]);
				String[] sLog1 = this.reader_.getStatusLog(1, this.lTurn_[num]+1);
				String[] sLog2 = this.reader_.getStatusLog(2, this.lTurn_[num]+1);
				int id1 = Integer.valueOf(sLog1[1]);
				int id2 = Integer.valueOf(sLog2[1]);

//				if ( (this.lTurn_[0]>this.fTurn_[3]) && this.isFood_[1] ){
				if ( (this.lTurn_[0]>this.fTurn_[3]) && this.passFood1_ ){
					String[] sLog3 = this.reader_.getStatusLog(4, this.lTurn_[num]);
					this.createModelWithItem(sLog1, sLog2, sLog3);
				}else{
					this.passFood1_ = false;
					if ( Double.valueOf(fLog1[12+id2]) < Double.valueOf(sLog1[12+id2]) ){
						this.createModelVerChara(sLog1, sLog2, false);
					}else{
						this.createModelVerChara(sLog1, sLog2, true);
					}
				}
		// 狩人との遭遇, 武器の譲渡
			}else if ( num == 1 ){
				String[] fLog1 = this.reader_.getStatusLog(1, 0);
				String[] fLog2 = this.reader_.getStatusLog(3, 0);
				String[] sLog1 = this.reader_.getStatusLog(1, this.lTurn_[num]+1);
				String[] sLog2 = this.reader_.getStatusLog(3, this.lTurn_[num]+1);
				int id1 = Integer.valueOf(sLog1[1]);
				int id2 = Integer.valueOf(sLog2[1]);

//				if ( (this.lTurn_[1]>this.fTurn_[5]) && this.isWeapon_[1] ){
				if ( (this.lTurn_[1]>this.fTurn_[5]) && this.passWeapon1_ ){
					String[] sLog3 = this.reader_.getStatusLog(5, this.lTurn_[num]);
					this.createModelWithItem(sLog1, sLog2, sLog3);
				}else{
					this.passWeapon1_ = false;
					if ( Double.valueOf(fLog1[12+id2]) < Double.valueOf(sLog1[12+id2]) ){
						this.createModelVerChara(sLog1, sLog2, false);
					}else{
						this.createModelVerChara(sLog1, sLog2, true);
					}
				}
		// 食べ物の取得
			}else if ( num == 2 ){
				String[] sLog1 = this.reader_.getStatusLog(1, this.fTurn_[num]+1);
				String[] sLog2 = this.reader_.getStatusLog(4, this.fTurn_[num]+1);
				this.createModelVerItem(sLog1, sLog2);
		// 武器の取得
			}else if ( num == 4 ){
				String[] sLog1 = this.reader_.getStatusLog(1, this.fTurn_[num]+1);
				String[] sLog2 = this.reader_.getStatusLog(5, this.fTurn_[num]+1);
				this.createModelVerItem(sLog1, sLog2);
			}
		}

	// ゴール処理
		int      type  = 1;
		String[] lLog1 = this.reader_.getStatusLog(1, Parameter.getTurn());
		String[] lLog2 = this.reader_.getStatusLog(2, Parameter.getTurn());
		String[] lLog3 = this.reader_.getStatusLog(3, Parameter.getTurn());
		String[] lLog4 = this.reader_.getStatusLog(4, Parameter.getTurn());
		String[] lLog5 = this.reader_.getStatusLog(5, Parameter.getTurn());
		String[] pos1 = lLog1[11].split("_");
		String[] pos2 = lLog2[11].split("_");
		String[] pos3 = lLog3[11].split("_");
		int d_enemy = Math.abs(Integer.parseInt(pos1[0])-Integer.parseInt(pos2[0])) +
				Math.abs(Integer.parseInt(pos1[1])-Integer.parseInt(pos2[1]));
		int d_ally  = Math.abs(Integer.parseInt(pos1[0])-Integer.parseInt(pos3[0])) +
				Math.abs(Integer.parseInt(pos1[1])-Integer.parseInt(pos3[1]));
		if ( d_enemy < 20 ){
			double frValue21 = Double.valueOf(lLog2[12+1]);
			double faValue21 = Double.valueOf(lLog2[12+this.all_+1]);
			double frValue31 = Double.valueOf(lLog3[12+1]);
			double faValue31 = Double.valueOf(lLog3[12+this.all_+1]);
			if ( d_ally < 20 ){
				if ( (frValue31>20.0) && (faValue31>20.0) && this.passWeapon1_ ){
					type = 3;
				}else{
					type = 2;
				}
			}else{
				if ( (frValue21>40.0) && (faValue21>20.0) && this.passFood1_ ){
					type = 4;
				}else{
					type = 2;
				}
			}
		}
		this.createModelJunction(lLog1, lLog2, lLog3, lLog4, lLog5, type);

	// エンド処理 (現在, エンドはシーンのみ)
		ArrayList<String[]> endStatus = new ArrayList<String[]>();
		endStatus.add(this.reader_.getStatusLog(1, Parameter.getTurn()));
		endStatus.add(this.reader_.getStatusLog(2, Parameter.getTurn()));
		endStatus.add(this.reader_.getStatusLog(3, Parameter.getTurn()));
	// エンド　シーン
		String key = "S-" + this.sID_;
		this.scene_ = new String[this.sceParams_.length];
		this.scene_[0] = String.valueOf(this.sID_);
		if ( type == 1 ){
			this.scene_[1] = "エンド";
			this.scene_[2] = "O-" + this.sID_;
			this.scene_[3] = "おばあさんの家";
			this.scene_[4] = "夜";
			this.scene_[5] = "R-" + this.sID_;
		}
		if ( type == 2 ){
			this.scene_[1] = "バッドエンド";
			this.scene_[2] = "O-" + this.sID_;
			this.scene_[3] = "森の中";
			this.scene_[4] = "夜";
			this.scene_[5] = "R-" + this.sID_;
		}
		if ( type == 3 ){
			this.scene_[1] = "ハッピーエンド";
			this.scene_[2] = "O-" + this.sID_;
			this.scene_[3] = "おばあさんの家";
			this.scene_[4] = "夜";
			this.scene_[5] = "R-" + this.sID_;
		}
		if ( type == 4 ){
			this.scene_[1] = "意外エンド";
			this.scene_[2] = "O-" + this.sID_;
			this.scene_[3] = "おばあさんの家";
			this.scene_[4] = "夜";
			this.scene_[5] = "R-" + this.sID_;
		}
		this.sModels_.put(key, this.scene_);

	// エンド　オブジェクト
		key2 = "O-" + this.sID_;
		if ( (type==1) || (type==2) ){
			this.object_ = new String[1][this.objParams_.length];
			this.object_[0] = this.createObjectModel(endStatus.get(0), 0);
		}
		if ( type == 3 ){
			this.object_ = new String[2][this.objParams_.length];
			this.object_[0] = this.createObjectModel(endStatus.get(0), 0);
			this.object_[1] = this.createObjectModel(endStatus.get(2), 1);
		}
		if ( type == 4 ){
			this.object_ = new String[3][this.objParams_.length];
			this.object_[0] = this.createObjectModel(endStatus.get(0), 2);
			this.object_[1] = this.createObjectModel(endStatus.get(1), 0);
			this.object_[2] = this.createObjectModel(endStatus.get(2), 1);
		}
		this.oModels_.put(key2, this.object_);

	// エンド　関係性
		key3 = "R-" + this.sID_;
		if ( (type==1) || (type==2) ){
			this.relation_ = new String[1][this.relParams_.length];
			this.relation_[0][0] = "1_1";
			this.relation_[0][1] = "null";
			this.relation_[0][2] = "null";
			this.relation_[0][3] = "連鎖";
			this.relation_[0][4] = "null";
			this.relation_[0][5] = "null";
			this.relation_[0][6] = "自身";
		}
		if ( type == 3 ){
			int[] ids = {1, 3};
			this.relation_ = new String[ids.length*ids.length][this.relParams_.length];
			for ( int i = 0; i < ids.length; i++ ){
				for ( int j = 0; j < ids.length; j++ ){
					this.relation_ = new String[1][this.relParams_.length];
					this.relation_[0][0] = ids[i] + "_" + ids[j];
					this.relation_[0][1] = "null";
					this.relation_[0][2] = "null";
					this.relation_[0][3] = "連鎖";
					this.relation_[0][4] = "null";
					this.relation_[0][5] = "null";
					if ( i == j ){
						this.relation_[0][6] = "自身";
					}else{
						this.relation_[0][6] = Parameter.getRoleName(Integer.valueOf(endStatus.get(i)[4]));
					}
				}
			}
		}
		if ( type == 4 ){
			int[] ids = {1, 2, 3};
			this.relation_ = new String[ids.length*ids.length][this.relParams_.length];
			for ( int i = 0; i < ids.length; i++ ){
				for ( int j = 0; j < ids.length; j++ ){
					this.relation_ = new String[1][this.relParams_.length];
					this.relation_[0][0] = ids[i] + "_" + ids[j];
					this.relation_[0][1] = "null";
					this.relation_[0][2] = "null";
					this.relation_[0][3] = "連鎖";
					this.relation_[0][4] = "null";
					this.relation_[0][5] = "null";
					if ( i == j ){
						this.relation_[0][6] = "自身";
					}else{
						this.relation_[0][6] = Parameter.getRoleName(Integer.valueOf(endStatus.get(i)[4]));
					}
				}
			}
		}
		this.rModels_.put(key3, this.relation_);

	// 保存
		Log.saveSceneModel(this.sceParams_, this.sModels_, this.sID_);
		Log.saveObjectModel(this.objParams_, this.oModels_, this.sID_);
		Log.saveRelationModel(this.relParams_, this.rModels_, this.sID_);
		Log.saveEndType(type, this.sID_);
	}







/**
 * アイテム譲渡のシーンを生成する.
 * @param sLog1
 * @param sLog2
 * @param sLog3
 */
	public void createModelWithItem(String[] sLog1, String[] sLog2, String[] sLog3){
		String item = "";
		if ( sLog3[4].equals("5") ){
			item = "食べ物";
			this.passFood2_ = this.aID_;
		}
		if ( sLog3[4].equals("6") ){
			item = "武器";
			this.passWeapon2_ = this.aID_;
		}
		int[] ids = new int[3];
		ids[0] = Integer.valueOf(sLog1[1]);
		ids[1] = Integer.valueOf(sLog2[1]);
		ids[2] = Integer.valueOf(sLog3[1]);

	// シーンモデル
		String key1 = "S-" + this.sID_;
		this.scene_ = new String[this.sceParams_.length];
		this.scene_[0] = String.valueOf(this.sID_);
		this.scene_[1] = Parameter.getRoleName(Integer.valueOf(sLog2[4])) + "との遭遇";
		this.scene_[2] = "O-" + this.sID_;
		this.scene_[3] = "森の中";
		this.scene_[4] = "昼";
		this.scene_[5] = "R-" + this.sID_;
		this.sModels_.put(key1, this.scene_);
	// オブジェクトモデル
		String key2 = "O-" + this.sID_;
		this.object_ = new String[3][this.objParams_.length];
		this.object_[0] = this.createObjectModel(sLog1, 0);
		this.object_[1] = this.createObjectModel(sLog2, 1);
		this.object_[2] = this.createObjectModel(sLog3, 4);
		this.oModels_.put(key2, this.object_);
	// 関係性モデル
		String key3 = "R-" +  this.sID_;
		this.relation_ = new String[ids.length*ids.length][this.relParams_.length];
		for ( int i = 0; i < ids.length; i++ ){
			for ( int j = 0; j < ids.length; j++ ){
				int count = ids.length*i + j;
				this.relation_[count][0] = ids[i] + "_" + ids[j];
				this.relation_[count][1] = "null";
				this.relation_[count][2] = "null";
				this.relation_[count][3] = "null";
				this.relation_[count][4] = "null";
				this.relation_[count][5] = "null";
				this.relation_[count][6] = "null";
				if ( (i==0) & (j==1) ){
					this.relation_[count][1] = item + "を渡す";
					this.relation_[count][2] = String.valueOf(this.aID_);
					this.relation_[count][3] = "連鎖";
					this.relation_[count][4] = "所有権を渡す";
					this.relation_[count][5] = "他人";
					this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(sLog1[4]));
					if ( Double.valueOf(sLog1[12+this.all_+ids[1]]) > 20.0 ){
						if ( Double.valueOf(sLog1[12+ids[1]]) > 20.0 ){
							this.relation_[count][5] = "仲が良い";
						}else if ( Double.valueOf(sLog1[12+ids[1]]) < -20.0 ){
							this.relation_[count][5] = "仲が悪い";
						}else{
							this.relation_[count][5] = "普通の仲";
						}
					}
					if ( sLog3[4].equals("5") && this.getFood1_ ){
						if ( this.getFood2_ != (this.aID_-1) ){
							this.relation_[count][3] = "フラグ (" + this.getFood2_ + ")";
						}
					}
					if ( sLog3[4].equals("6") && this.getWeapon1_ ){
						if ( this.getWeapon2_ != (this.aID_-1) ){
							this.relation_[count][3] = "フラグ (" + this.getWeapon2_ + ")";
						}
					}
				}
				if ( (i==1) & (j==0) ){
					this.relation_[count][1] = "-" + item + "を渡す";
					this.relation_[count][2] = String.valueOf(this.aID_);
					this.relation_[count][3] = "連鎖";
					this.relation_[count][4] = "所有権を得る";
					this.relation_[count][5] = "他人";
					this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(sLog2[4]));
					if ( Double.valueOf(sLog2[12+this.all_+ids[0]]) > 20.0 ){
						if ( Double.valueOf(sLog2[12+ids[0]]) > 20.0 ){
							this.relation_[count][5] = "仲が良い";
						}else if ( Double.valueOf(sLog2[12+ids[0]]) < -20.0 ){
							this.relation_[count][5] = "仲が悪い";
						}else{
							this.relation_[count][5] = "普通の仲";
						}
					}
					if ( sLog3[4].equals("5") && this.getFood1_ ){
						if ( this.getFood2_ != (this.aID_-1) ){
							this.relation_[count][3] = "フラグ (" + this.getFood2_ + ")";
						}
					}
					if ( sLog3[4].equals("6") && this.getWeapon1_ ){
						if ( this.getWeapon2_ != (this.aID_-1) ){
							this.relation_[count][3] = "フラグ (" + this.getWeapon2_ + ")";
						}
					}
				}
				if ( (i==0) & (j==2) ){
					this.relation_[count][6] = "所有者";
				}
				if ( (i==2) & (j==0) ){
					this.relation_[count][6] = "所有物";
				}
				if ( i == j ){
					this.relation_[count][6] = "自身";
				}
			}
		}
		this.rModels_.put(key3, this.relation_);

		this.sID_++;
		this.aID_++;
		if ( item.equals("食べ物") ){
			this.bAct_ = 3;
		}
		if ( item.equals("武器") ){
			this.bAct_ = 4;
		}
	}







/**
 * 他登場人物との遭遇のシーンを生成する.
 * @param sLog1
 * @param sLog2
 * @param isNegative
 */
	public void createModelVerChara(String[] sLog1, String[] sLog2, boolean isNegative){
		int[] ids = new int[2];
		ids[0] = Integer.valueOf(sLog1[1]);
		ids[1] = Integer.valueOf(sLog2[1]);
		if ( Parameter.getRoleName(Integer.valueOf(sLog1[4])).equals("敵") ){
			this.encountEnemy2_ = this.aID_;
		}
		if ( Parameter.getRoleName(Integer.valueOf(sLog1[4])).equals("味方") ){
			this.encountAlly2_ = this.aID_;
		}

	// シーンモデル
		String key1 = "S-" + this.sID_;
		this.scene_ = new String[this.sceParams_.length];
		this.scene_[0] = String.valueOf(this.sID_);
		this.scene_[1] = Parameter.getRoleName(Integer.valueOf(sLog2[4])) + "との遭遇";
		this.scene_[2] = "O-" + this.sID_;
		this.scene_[3] = "森の中";
		this.scene_[4] = "昼";
		this.scene_[5] = "R-" + this.sID_;
		this.sModels_.put(key1, this.scene_);
	// オブジェクトモデル
		String key2 = "O-" + this.sID_;
		this.object_ = new String[2][this.objParams_.length];
		this.object_[0] = this.createObjectModel(sLog1, 0);
		this.object_[1] = this.createObjectModel(sLog2, 1);
		this.oModels_.put(key2, this.object_);
	// 関係性モデル
		String key3 = "R-" +  this.sID_;
		this.relation_ = new String[ids.length*ids.length][this.relParams_.length];
		for ( int i = 0; i < ids.length; i++ ){
			for ( int j = 0; j < ids.length; j++ ){
				int count = ids.length*i + j;
				this.relation_[count][0] = ids[i] + "_" + ids[j];
				this.relation_[count][1] = "null";
				this.relation_[count][2] = "null";
				this.relation_[count][3] = "null";
				this.relation_[count][4] = "null";
				this.relation_[count][5] = "null";
				this.relation_[count][6] = "null";
				if ( (i==0) & (j==1) ){
					this.relation_[count][1] = "話す";
					this.relation_[count][2] = String.valueOf(this.aID_);
					this.relation_[count][3] = "連鎖";
					this.relation_[count][4] = "仲が良くなる";
					this.relation_[count][5] = "他人";
					this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(sLog1[4]));
					if ( isNegative ){
						this.relation_[count][4] = "仲が悪くなる";
					}
					if ( Double.valueOf(sLog1[12+this.all_+ids[1]]) > 20.0 ){
						if ( Double.valueOf(sLog1[12+ids[1]]) > 20.0 ){
							this.relation_[count][5] = "仲が良い";
						}else if ( Double.valueOf(sLog1[12+ids[1]]) < -20.0 ){
							this.relation_[count][5] = "仲が悪い";
						}else{
							this.relation_[count][5] = "普通の仲";
						}
					}
				}
				if ( (i==1) & (j==0) ){
					this.relation_[count][1] =  "話す";
					this.relation_[count][2] = String.valueOf(this.aID_);
					this.relation_[count][3] = "連鎖";
					this.relation_[count][5] = "他人";
					this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(sLog2[4]));
					if ( Double.valueOf(sLog2[12+this.all_+ids[0]]) > 20.0 ){
						if ( Double.valueOf(sLog2[12+ids[0]]) > 20.0 ){
							this.relation_[count][5] = "仲が良い";
						}else if ( Double.valueOf(sLog2[12+ids[0]]) < -20.0 ){
							this.relation_[count][5] = "仲が悪い";
						}else{
							this.relation_[count][5] = "普通の仲";
						}
					}
				}
				if ( i == j ){
					this.relation_[count][6] = "自身";
				}
			}
		}
		this.rModels_.put(key3, this.relation_);

		this.sID_++;
		this.aID_++;
		if ( ids[1] == 2 ){
			this.bAct_ = 5;
		}
		if ( ids[1] == 3 ){
			this.bAct_ = 4;
		}
	}







/**
 * アイテム取得のシーンを生成する.
 * @param sLog1
 * @param sLog3
 */
	public void createModelVerItem(String[] sLog1, String[] sLog3){
		String item = "";
		if ( sLog3[4].equals("5") ){
			item = "食べ物";
			this.getFood2_ = this.aID_;
		}
		if ( sLog3[4].equals("6") ){
			item = "武器";
			this.getWeapon2_ = this.aID_;
		}
		int[] ids = new int[2];
		ids[0] = Integer.valueOf(sLog1[1]);
		ids[1] = Integer.valueOf(sLog3[1]);

	// シーンモデル
		String key1 = "S-" + this.sID_;
		this.scene_ = new String[this.sceParams_.length];
		this.scene_[0] = String.valueOf(this.sID_);
//		this.scene_[1] = sLog3[3] + "の取得";
		this.scene_[1] = item + "の取得";
		this.scene_[2] = "O-" + this.sID_;
		this.scene_[3] = "森の中";
		this.scene_[4] = "昼";
		this.scene_[5] = "R-" + this.sID_;
		this.sModels_.put(key1, this.scene_);
	// オブジェクトモデル
		String key2 = "O-" + this.sID_;
		this.object_ = new String[2][this.objParams_.length];
		this.object_[0] = this.createObjectModel(sLog1, 0);
		this.object_[1] = this.createObjectModel(sLog3, 3);
		this.oModels_.put(key2, this.object_);
	// 関係性モデル
		String key3 = "R-" +  this.sID_;
		this.relation_ = new String[ids.length*ids.length][this.relParams_.length];
		for ( int i = 0; i < ids.length; i++ ){
			for ( int j = 0; j < ids.length; j++ ){
				int count = ids.length*i + j;
				this.relation_[count][0] = ids[i] + "_" + ids[j];
				this.relation_[count][1] = "null";
				this.relation_[count][2] = "null";
				this.relation_[count][3] = "null";
				this.relation_[count][4] = "null";
				this.relation_[count][5] = "null";
				this.relation_[count][6] = "null";
				if ( (i==0) & (j==1) ){
					this.relation_[count][1] = "拾う";
					this.relation_[count][2] = String.valueOf(this.aID_);
					this.relation_[count][3] = "連鎖";
					this.relation_[count][4] = "所有権を得る";
					this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(sLog1[4]));
				}
				if ( (i==1) & (j==0) ){
					this.relation_[count][1] =  "-拾う";
					this.relation_[count][2] = String.valueOf(this.aID_);
					this.relation_[count][3] = "連鎖";
					this.relation_[count][4] = "-所有権を得る";
					this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(sLog3[4]));
				}
				if ( i == j ){
					this.relation_[count][6] = "自身";
				}
			}
		}
		this.rModels_.put(key3, this.relation_);

		this.sID_++;
		this.aID_++;
		if ( item.equals("食べ物") ){
			this.bAct_ = 1;
		}
		if ( item.equals("武器") ){
			this.bAct_ = 2;
		}
	}








/**
 * 最後の分岐シーンを生成する.
 * @param lLog1
 * @param lLog2
 * @param lLog3
 * @param lLog4
 * @param lLog5
 * @param type
 */
	public void createModelJunction(String[] lLog1, String[] lLog2, String[] lLog3, String[] lLog4, String[] lLog5, int type){
		int id1 = Integer.valueOf(lLog1[1]);
		int id2 = Integer.valueOf(lLog2[1]);
		int id3 = Integer.valueOf(lLog3[1]);
		int id4 = Integer.valueOf(lLog4[1]);
		int id5 = Integer.valueOf(lLog5[1]);

	// ただのエンド
		if ( type == 1 ){
		// シーンモデル
			String key1 = "S-" + this.sID_;
			this.scene_ = new String[sceParams_.length];
			this.scene_[0] = String.valueOf(this.sID_);
			this.scene_[1] = "おばあさんの家への到着";
			this.scene_[2] = "O-" + this.sID_;
			this.scene_[3] = "おばあさんの家の前";
			this.scene_[4] = "夜";
			this.scene_[5] = "R-" + this.sID_;
			this.sModels_.put(key1, this.scene_);
		// オブジェクトモデル
			String key2 = "O-" + this.sID_;
			this.object_ = new String[1][this.objParams_.length];
			this.object_[0] = this.createObjectModel(lLog1, 0);
			this.oModels_.put(key2, this.object_);
		// 関係性モデル
			String key3 = "R-" + this.sID_;
			this.relation_ = new String[1][this.relParams_.length];
			this.relation_[0][0] = "1_1";
			this.relation_[0][1] = "null";
			this.relation_[0][2] = "null";
			this.relation_[0][3] = "連鎖";
			this.relation_[0][4] = "null";
			this.relation_[0][5] = "null";
			this.relation_[0][6] = Parameter.getRoleName(Integer.valueOf(lLog1[4]));
			this.rModels_.put(key3, this.relation_);

	// バッドエンド
		}else if ( type == 2 ){
		// シーンモデル
			String key1 = "S-" + this.sID_;
			this.scene_ = new String[this.sceParams_.length];
			this.scene_[0] = String.valueOf(this.sID_);
//			this.scene_[1] = lLog2[3] + "の襲撃";
			this.scene_[1] = Parameter.getRoleName(Integer.valueOf(lLog2[4])) + "の襲撃";
			this.scene_[2] = "O-" + this.sID_;
			this.scene_[3] = "森の中";
			this.scene_[4] = "昼";
			this.scene_[5] = "R-" + this.sID_;
			this.sModels_.put(key1, this.scene_);
		// オブジェクトモデル
			String key2 = "O-" + this.sID_;
			this.object_ = new String[2][this.objParams_.length];
			this.object_[0] = this.createObjectModel(lLog1, 0);
			this.object_[1] = this.createObjectModel(lLog2, 1);
			this.oModels_.put(key2, this.object_);
		// 関係性モデル
			int[] ids = {id1, id2};
			String key3 = "R-" +  this.sID_;
			this.relation_ = new String[ids.length*ids.length][this.relParams_.length];
			for ( int i = 0; i < ids.length; i++ ){
				for ( int j = 0; j < ids.length; j++ ){
					int count = ids.length*i + j;
					this.relation_[count][0] = ids[i] + "_" + ids[j];
					this.relation_[count][1] = "null";
					this.relation_[count][2] = "null";
					this.relation_[count][3] = "null";
					this.relation_[count][4] = "null";
					this.relation_[count][5] = "null";
					this.relation_[count][6] = "null";
					if ( (i==0) & (j==1) ){
						this.relation_[count][1] = "-襲う";
						this.relation_[count][2] = String.valueOf(this.aID_);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog1[4]));
//						System.out.println(Double.valueOf(lLog1[12+ids[1]]));
//						System.out.println(Double.valueOf(lLog1[12+this.all_+ids[1]]));
						if ( Double.valueOf(lLog1[12+this.all_+ids[1]]) > 20.0 ){
							if ( Double.valueOf(lLog1[12+ids[1]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog1[12+ids[1]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
					}
					if ( (i==1) & (j==0) ){
						this.relation_[count][1] = "襲う";
						this.relation_[count][2] = String.valueOf(this.aID_);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][4] = "襲撃成功";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog2[4]));
						if ( Double.valueOf(lLog2[12+this.all_+ids[0]]) > 20.0 ){
							if ( Double.valueOf(lLog2[12+ids[0]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog2[12+ids[0]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
					}
					if ( i == j ){
						this.relation_[count][6] = "自身";
					}
				}
			}
			this.rModels_.put(key3, this.relation_);

	// ハッピーエンド
		}else if ( type == 3 ){
		// シーンモデル
			String key1 = "S-" + this.sID_;
			this.scene_ = new String[sceParams_.length];
			this.scene_[0] = String.valueOf(this.sID_);
//			this.scene_[1] = lLog2[3] + "の襲撃";
			this.scene_[1] = Parameter.getRoleName(Integer.valueOf(lLog2[4])) + "の襲撃";
			this.scene_[2] = "O-" + this.sID_;
			this.scene_[3] = "森の中";
			this.scene_[4] = "昼";
			this.scene_[5] = "R-" + this.sID_;
			this.sModels_.put(key1, this.scene_);
		// オブジェクトモデル
			String key2 = "O-" + this.sID_;
			this.object_ = new String[4][this.objParams_.length];
			this.object_[0] = this.createObjectModel(lLog1, 0);
			this.object_[1] = this.createObjectModel(lLog2, 1);
			this.object_[2] = this.createObjectModel(lLog3, 2);
			this.object_[3] = this.createObjectModel(lLog5, 2);
			this.oModels_.put(key2, this.object_);
		// 関係性モデル
			int[] ids = {id1, id2, id3, id5};
			String key3 = "R-" +  this.sID_;
			this.relation_ = new String[ids.length*ids.length][this.relParams_.length];
			for ( int i = 0; i < ids.length; i++ ){
				for ( int j = 0; j < ids.length; j++ ){
					int count = ids.length*i + j;
					this.relation_[count][0] = ids[i] + "_" + ids[j];
					this.relation_[count][1] = "null";
					this.relation_[count][2] = "null";
					this.relation_[count][3] = "null";
					this.relation_[count][4] = "null";
					this.relation_[count][5] = "null";
					this.relation_[count][6] = "null";
					if ( (i==0) & (j==1) ){
						this.relation_[count][1] = "-襲う";
						this.relation_[count][2] = String.valueOf(this.aID_);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog1[4]));
						if ( Double.valueOf(lLog1[12+this.all_+ids[1]]) > 20.0 ){
							if ( Double.valueOf(lLog1[12+ids[1]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog1[12+ids[1]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
					}
					if ( (i==1) & (j==0) ){
						this.relation_[count][1] = "襲う";
						this.relation_[count][2] = String.valueOf(this.aID_);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][4] = "襲撃失敗";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog2[4]));
						if ( Double.valueOf(lLog2[12+this.all_+ids[0]]) > 20.0 ){
							if ( Double.valueOf(lLog2[12+ids[0]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog2[12+ids[0]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
					}
					if ( (i==0) & (j==2) ){
						this.relation_[count][1] = "-助ける";
						this.relation_[count][2] = String.valueOf(this.aID_+1);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog1[4]));
//						System.out.println(Double.valueOf(lLog1[12+ids[1]]));
//						System.out.println(Double.valueOf(lLog1[12+this.all_+ids[1]]));
						if ( Double.valueOf(lLog1[12+this.all_+ids[1]]) > 20.0 ){
							if ( Double.valueOf(lLog1[12+ids[1]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog1[12+ids[1]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
						if ( this.passWeapon1_ ){
							if ( this.passWeapon2_ != (this.aID_-1) ){
								this.relation_[count][3] = "フラグ (" + this.passWeapon2_ + ")";
							}
						}
					}
					if ( (i==2) & (j==0) ){
						this.relation_[count][1] = "助ける";
						this.relation_[count][2] = String.valueOf(this.aID_+1);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][4] = "救助成功";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog3[4]));
						if ( Double.valueOf(lLog2[12+this.all_+ids[0]]) > 20.0 ){
							if ( Double.valueOf(lLog2[12+ids[0]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog2[12+ids[0]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
						if ( this.passWeapon1_ ){
							if ( this.passWeapon2_ != (this.aID_-1) ){
								this.relation_[count][3] = "フラグ (" + this.passWeapon2_ + ")";
							}
						}
					}
					if ( (i==1) & (j==2) ){
						this.relation_[count][1] = "-退治する";
						this.relation_[count][2] = String.valueOf(this.aID_+1);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog2[4]));
//						System.out.println(Double.valueOf(lLog1[12+ids[1]]));
//						System.out.println(Double.valueOf(lLog1[12+this.all_+ids[1]]));
						if ( Double.valueOf(lLog1[12+this.all_+ids[1]]) > 20.0 ){
							if ( Double.valueOf(lLog1[12+ids[1]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog1[12+ids[1]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
					}
					if ( (i==2) & (j==1) ){
						this.relation_[count][1] = "退治する";
						this.relation_[count][2] = String.valueOf(this.aID_+1);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][4] = "撃退成功";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog3[4]));
						if ( Double.valueOf(lLog2[12+this.all_+ids[0]]) > 20.0 ){
							if ( Double.valueOf(lLog2[12+ids[0]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog2[12+ids[0]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
					}
					if ( (i==2) & (j==3) ){
						this.relation_[count][1] = "使用する";
						this.relation_[count][2] = String.valueOf(this.aID_+1);
						this.relation_[count][6] = "所有者";
					}
					if ( (i==3) & (j==2) ){
						this.relation_[count][1] = "-使用する";
						this.relation_[count][2] = String.valueOf(this.aID_+1);
						this.relation_[count][6] = "所有物";
					}
					if ( i == j ){
						this.relation_[count][6] = "自身";
					}
				}
			}
			this.rModels_.put(key3, this.relation_);

	// 意外エンド
		}else if ( type == 4 ){
		// シーンモデル
			String key1 = "S-" + this.sID_;
			this.scene_ = new String[sceParams_.length];
			this.scene_[0] = String.valueOf(this.sID_);
//			this.scene_[1] = lLog2[3] + "の襲撃";
			this.scene_[1] = Parameter.getRoleName(Integer.valueOf(lLog2[4])) + "との和解";
			this.scene_[2] = "O-" + this.sID_;
			this.scene_[3] = "森の中";
			this.scene_[4] = "昼";
			this.scene_[5] = "R-" + this.sID_;
			this.sModels_.put(key1, this.scene_);
		// オブジェクトモデル
			String key2 = "O-" + this.sID_;
			this.object_ = new String[2][this.objParams_.length];
			this.object_[0] = this.createObjectModel(lLog1, 6);
			this.object_[1] = this.createObjectModel(lLog2, 7);
			this.oModels_.put(key2, this.object_);
		// 関係性モデル
			int[] ids = {id1, id2};
			String key3 = "R-" +  this.sID_;
			this.relation_ = new String[ids.length*ids.length][this.relParams_.length];
			for ( int i = 0; i < ids.length; i++ ){
				for ( int j = 0; j < ids.length; j++ ){
					int count = ids.length*i + j;
					this.relation_[count][0] = ids[i] + "_" + ids[j];
					this.relation_[count][1] = "null";
					this.relation_[count][2] = "null";
					this.relation_[count][3] = "null";
					this.relation_[count][4] = "null";
					this.relation_[count][5] = "null";
					this.relation_[count][6] = "null";
					if ( (i==0) & (j==1) ){
						this.relation_[count][1] = "-謝る";
						this.relation_[count][2] = String.valueOf(this.aID_);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][4] = "仲直りする";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog1[4]));
						if ( Double.valueOf(lLog1[12+this.all_+ids[1]]) > 20.0 ){
							if ( Double.valueOf(lLog1[12+ids[1]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog1[12+ids[1]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
					}
					if ( (i==1) & (j==0) ){
						this.relation_[count][1] = "謝る";
						this.relation_[count][2] = String.valueOf(this.aID_);
						this.relation_[count][3] = "連鎖";
						this.relation_[count][4] = "仲直りする";
						this.relation_[count][5] = "他人";
						this.relation_[count][6] = Parameter.getRoleName(Integer.valueOf(lLog2[4]));
						if ( Double.valueOf(lLog2[12+this.all_+ids[0]]) > 20.0 ){
							if ( Double.valueOf(lLog2[12+ids[0]]) > 20.0 ){
								this.relation_[count][5] = "仲が良い";
							}else if ( Double.valueOf(lLog2[12+ids[0]]) < -20.0 ){
								this.relation_[count][5] = "仲が悪い";
							}else{
								this.relation_[count][5] = "普通の仲";
							}
						}
					}

					if ( i == j ){
						this.relation_[count][6] = "自身";
					}
				}
			}
			this.rModels_.put(key3, this.relation_);
			this.aID_++;
		}

		this.sID_++;
		this.aID_++;
	}








/**
 * ストーリーモデルのオブジェクトを生成する.
 *
 * @param sLog
 * @param positionNum 0: 人物(左1), 1: 人物(右1), 2: 人物(左2), 3: アイテム(右1), 4: アイテム(左1), 5: アイテム(右2)
 * @param positionNum 6: 人物(左3), 7: 人物(右2)
 * @return
 */
	public String[] createObjectModel(String[] sLog, int positionNum){
	// 登場人物の場合
		if ( Parameter.getTypeName(Integer.valueOf(sLog[2])).equals("登場人物") ){
			String[] oModel = new String[this.objParams_.length];
			oModel[0] = sLog[1];
			oModel[1] = Parameter.getTypeName(Integer.valueOf(sLog[2]));
			oModel[2] = Parameter.getRoleName(Integer.valueOf(sLog[4]));
			oModel[3] = sLog[3];
			oModel[4] = sLog[6];
			oModel[5] = Parameter.getSexName(Integer.valueOf(sLog[5]));
			if ( Double.valueOf(sLog[7]) > 0.7 ){
				if ( Double.valueOf(sLog[8]) > 0.7 ){
					oModel[6] = this.p1_[0] + "-" + this.p2_[0];
				}else if ( Double.valueOf(sLog[8]) < 0.3 ){
					oModel[6] = this.p1_[0] + "-" + this.p2_[1];
				}else{
					oModel[6] = this.p1_[0] + "- ";
				}
			}else if ( Double.valueOf(sLog[7]) < 0.3 ){
				if ( Double.valueOf(sLog[8]) > 0.7 ){
					oModel[6] = this.p1_[1] + "-" + this.p2_[0];
				}else if ( Double.valueOf(sLog[8]) < 0.3 ){
					oModel[6] = this.p1_[1] + "-" + this.p2_[1];
				}else{
					oModel[6] = this.p1_[1] + "- ";
				}
			}else{
				if ( Double.valueOf(sLog[8]) > 0.7 ){
					oModel[6] = " -" + this.p2_[0];
				}else if ( Double.valueOf(sLog[8]) < 0.3 ){
					oModel[6] = " -" + this.p2_[1];
				}else{
					oModel[6] = " - ";
				}
			}
			if ( Double.valueOf(sLog[9]) > 0.2 ){
				oModel[7] = "機嫌が良い";
				oModel[9] = "楽しい";
			}else if ( Double.valueOf(sLog[9]) < -0.2 ){
				oModel[7] = "機嫌が悪い";
				if ( Double.valueOf(sLog[10]) < 0.5 ){
					oModel[9] = "悲しい";
				}else{
					oModel[9] = "怒り";
				}
			}else{
				oModel[7] = "デフォルト";
				oModel[9] = "デフォルト";
				if ( Double.valueOf(sLog[10]) < 0.5 ){
					oModel[9] = "悲しい";
				}
			}
			oModel[8] = "立っている";
			if ( Double.valueOf(sLog[10]) < 0.3 ){
				oModel[10] = "悪い";
			}else if ( Double.valueOf(sLog[10]) > 0.7 ){
				oModel[10] = "良い";
			}else{
				oModel[10] = "普通";
			}
			oModel[11] = this.pos_[positionNum];
			if ( Parameter.getRoleName(Integer.valueOf(sLog[4])).equals("主人公") ){
				oModel[11] += "5";
			}else if ( Parameter.getRoleName(Integer.valueOf(sLog[4])).equals("敵") ){
				oModel[11] += "6";
			}else if ( Parameter.getRoleName(Integer.valueOf(sLog[4])).equals("仲間") ){
				oModel[11] += "7";
			}
			return oModel;

	// アイテムの場合
		}else if ( Parameter.getTypeName(Integer.valueOf(sLog[2])).equals("アイテム") ){
			String[] oModel = new String[this.objParams_.length];
			oModel[0] = sLog[1];
			oModel[1] = Parameter.getTypeName(Integer.valueOf(sLog[2]));
			oModel[2] = Parameter.getRoleName(Integer.valueOf(sLog[4]));
			oModel[3] = sLog[3];
			oModel[4] = sLog[6];
			oModel[5] = Parameter.getSexName(Integer.valueOf(sLog[5]));
			oModel[6] = "未設定";
			oModel[7] = "未設定";
			oModel[8] = "未設定";
			oModel[9] = "未設定";
			if ( Double.valueOf(sLog[10]) < 0.3 ){
				oModel[10] = "悪い";
			}else if ( Double.valueOf(sLog[10]) > 0.7 ){
				oModel[10] = "良い";
			}else{
				oModel[10] = "普通";
			}
			oModel[11] = this.pos_[positionNum];
			if ( Parameter.getRoleName(Integer.valueOf(sLog[4])).equals("食べ物") ){
				oModel[11] += "4";
			}else if ( Parameter.getRoleName(Integer.valueOf(sLog[4])).equals("武器") ){
				oModel[11] += "3";
			}
			return oModel;
		}

		return null;
	}







// ゲッター関数

/**
 * シーンパラメータを返す.
 * @return
 */
	public String[] getSceneParameter(){
		return this.sceParams_;
	}



/**
 * オブジェクトパラメータを返す.
 * @return
 */
	public String[] getObjectParameter(){
		return this.objParams_;
	}



/**
 * 関係性パラメータを返す.
 * @return
 */
	public String[] getRelationParameter(){
		return this.relParams_;
	}



/**
 * ストーリーモデル (シーン) を返す.
 * @return
 */
	public HashMap<String, String[]> getSceneModel(){
		return this.sModels_;
	}



/**
 * ストーリーモデル (オブジェクト) を返す.
 * @return
 */
	public HashMap<String, String[][]> getObjectModel(){
		return this.oModels_;
	}



/**
 * ストーリーモデル (関係性) を返す.
 * @return
 */
	public HashMap<String, String[][]> getRelationModel(){
		return this.rModels_;
	}
}








