package story_model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class LogReader {

// ステータスログ用パラメータ
	private String[] staParams_;
// アクションログ用パラメータ
	private String[] actParams_;
// ステータスログ
	private HashMap<String, String[]> statusLog_;
// アクションログ
	private HashMap<String, String[]> actionLog_;



/**
 * コンストラクタ
 */
	public LogReader(){
		this.statusLog_ = new HashMap<String, String[]>();
		this.actionLog_ = new HashMap<String, String[]>();
	}







/**
 * ステータスログを読み込む
 * @param fileName
 */
	public void readStatusLog(String fileName){
		try{
			FileInputStream   fis = new FileInputStream(fileName);
			InputStreamReader isr = new InputStreamReader(fis, "SJIS");
			BufferedReader    br = new BufferedReader(isr);

		// パラメータ設定
			String   line   = br.readLine();
			String[] params = line.split(",");
			this.staParams_ = new String[params.length];
			for ( int i = 0; i < params.length; i++ ){
				this.staParams_[i] = params[i];
			}

		// ログデータ設定
			while ( (line=br.readLine()) != null ){
				String[] temp  = line.split(",");
				String   key   = temp[1] + "_" + temp[0];
				String[] value = new String[temp.length];
				for ( int i = 0; i < temp.length; i++ ){
					value[i] = temp[i];
				}
				this.statusLog_.put(key, value);
			}
			br.close();
			isr.close();
			fis.close();

			System.out.println("Status log data is set : " + fileName);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}







/**
 * アクションログを読み込む
 * @param fileName
 */
	public void readActionLog(String fileName){
		try{
			FileInputStream   fis = new FileInputStream(fileName);
			InputStreamReader isr = new InputStreamReader(fis, "SJIS");
			BufferedReader    br = new BufferedReader(isr);

		// パラメータ設定
			String   line   = br.readLine();
			String[] params = line.split(",");
			this.actParams_ = new String[params.length];
			for ( int i = 0; i < params.length; i++ ){
				this.actParams_[i] = params[i];
			}

		// ログデータ設定
			int    count = 0;
			String oID   = "-1";
			String turn  = "-1";
			while ( (line=br.readLine()) != null ){
				String[] temp  = line.split(",");
				if ( oID.equals(temp[3]) && turn.equals(temp[0]) ){
					count++;
				}else{
					count = 0;
				}
				oID  = temp[3];
				turn = temp[0];
				String   key   = oID + "_" + turn + "_" + count;
				String[] value = new String[temp.length];
				for ( int i = 0; i < temp.length; i++ ){
					value[i] = temp[i];
				}
				this.actionLog_.put(key, value);
			}
			br.close();
			isr.close();
			fis.close();

			System.out.println("Action log data is set : " + fileName);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}







// ゲッター関数

/**
 * ステータスパラメータを返す.
 * @return
 */
	public String[] getStatusParameter(){
		return this.staParams_;
	}



/**
 * アクションパラメータを返す.
 * @return
 */
	public String[] getActionParameter(){
		return this.actParams_;
	}





/**
 * OID が oID, ターン数が turn のステータスログを返す.
 * @param oID
 * @param turn
 * @return
 */
	public String[] getStatusLog(int oID, int turn){
		String key = oID + "_" + turn;
		String[] log = this.statusLog_.get(key);

		return log;
	}



/**
 * OID が oID, ターン数が turn のアクションログを返す.
 * @param oID
 * @param turn
 * @return
 */
	public ArrayList<String[]> getActionLog(int oID, int turn){
		ArrayList<String[]> logs = new ArrayList<String[]>();

		int      count = 0;
		while ( true ){
			String key = oID + "_" + turn + "_" + count;
			if ( this.actionLog_.get(key) != null ){
				logs.add(this.actionLog_.get(key));
			}else{
				break;
			}
			count++;
		}

		return logs;
	}





/**
 * OID が oID, ターン数が turn のステータスログの中で, パラメータが param であるデータを返す.
 * パラメータに param が存在しない場合は null を返す.
 * @param oID
 * @param turn
 * @param param
 * @return
 */
	public String getDataOfStatusLog(int oID, int turn, String param){
		String ans = null;
		for ( int i = 0; i < this.staParams_.length; i++ ){
			if ( this.staParams_[i].equals(param) ){
				String key = oID + "_" + turn;
				ans = this.statusLog_.get(key)[i];
				break;
			}
		}

		return ans;
	}



/**
 * OID が oID, ターン数が turn の count 回目のステータスログの中で, パラメータが param であるデータを返す.
 * パラメータに param が存在しない場合は null を返す.
 * @param oID
 * @param turn
 * @param count
 * @param param
 * @return
 */
	public String getDataOfActionLog(int oID, int turn, int count, String param){
		String ans = null;
		for ( int i = 0; i < this.actParams_.length; i++ ){
			if ( this.actParams_[i].equals(param) ){
				String key = oID + "_" + turn + "_" + count;
				if ( this.actionLog_.get(key) != null ){
					ans = this.statusLog_.get(key)[i];
				}
				break;
			}
		}

		return ans;
	}

}







