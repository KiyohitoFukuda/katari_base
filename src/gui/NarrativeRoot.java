package gui;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import util.Log;
import util.Parameter;

public class NarrativeRoot extends BorderPane{

	public ArrayList<String[]> stories_;		// 文章
	public ArrayList<String[]> pictures_; 		// 絵の保存先と貼り付け位置
	public int size_;		// 背景画像の一片の長さ
	public int uSize_;		// 一片の単位長さ
	public int sectionNum_;


/**
 * コンストラクタ
 */
	public NarrativeRoot(){
		this.stories_  = new ArrayList<String[]>();
		this.pictures_ = new ArrayList<String[]>();
		this.setPrefSize(1180, 700);
		this.size_       = 450;
		this.uSize_      = this.size_ / 10;
		this.sectionNum_ = 0;
	}







	public void createDefaultStoryPart(){
		TextArea tArea = new TextArea();

		tArea.setPrefSize(this.size_, this.size_);
		tArea.setWrapText(true);
		tArea.setText("ここに文章が表示される予定です");
		tArea.setStyle("-fx-font-size:16pt;");

		this.setRight(tArea);
//		return tArea;
	}


	public void createStoryPart(int sectionNum){
		TextArea tArea = new TextArea();
		String[] texts = this.stories_.get(sectionNum);

		tArea.setPrefSize(this.size_, this.size_);
		tArea.setWrapText(true);
		String str = "";
		for ( int i = 0; i < texts.length; i++ ){
			str += texts[i] + "\n";
		}
		tArea.setText(str);
		tArea.setStyle("-fx-font-size:16pt;");

		this.setRight(tArea);
//		return tArea;
	}







	public void createDefaultPicturePart(){
		TextArea pArea = new TextArea();

		pArea.setPrefSize(450, 450);
		pArea.setWrapText(true);
		pArea.setText("ここに画像が表示される予定です");
		pArea.setStyle("-fx-font-size:16pt;");

		this.setCenter(pArea);
//		return tArea;
	}


	public void createPicturePart(int sectionNum){
		try{
			Canvas   canvas = new Canvas(this.size_, this.size_);
			String[] parts  = this.pictures_.get(sectionNum);
			String[] fileNames = new String[parts.length];
			int[][]  positions = new int[parts.length][4];

			GraphicsContext gc = canvas.getGraphicsContext2D();
			for ( int i = 0; i < parts.length; i++ ){
				String[] data = parts[i].split(",");
				fileNames[i]    = data[0];
				positions[i][0] = Integer.valueOf(data[1].split("-")[0]);
				positions[i][1] = Integer.valueOf(data[1].split("-")[1]);
				positions[i][2] = Integer.valueOf(data[1].split("-")[2]);
				positions[i][3] = Integer.valueOf(data[1].split("-")[3]);
			}

			FileInputStream fis   = new FileInputStream(fileNames[0]);
			Image           image = new Image(fis);
			double startX = positions[0][0] * this.uSize_;
			double startY = positions[0][1] * this.uSize_;
			double xSize  = this.size_;
			double ySize  = this.size_;

			gc.drawImage(image, startX, startY, xSize, ySize);
		// エンドは現在背景のみ
			if ( sectionNum != this.pictures_.size()-1 ){
				for ( int i = 1; i < fileNames.length; i++ ){
					fis   = new FileInputStream(fileNames[i]);
					image = new Image(fis);
					double xRate = Math.min((positions[i][2]*this.uSize_/image.getWidth()), 1.0);
					double yRate = Math.min((positions[i][3]*this.uSize_/image.getHeight()), 1.0);
					double rate  = Math.min(xRate, yRate);
					xSize  = image.getWidth() * rate;
					ySize  = image.getHeight() * rate;
					double positionX = (positions[i][2]*this.uSize_-xSize) / 2;
					double positionY = (positions[i][3]*this.uSize_-ySize) / 2;
					startX = positions[i][0] * this.uSize_ + positionX;
					startY = positions[i][1] * this.uSize_ + positionY;

					System.out.println(fileNames[i] + " : (" + startX + "," + startY + ")  "  + xSize + ", " + ySize);
					gc.drawImage(image, startX, startY, xSize, ySize);
				}
			}

		// 画像を保
			SnapshotParameters snapParam = new SnapshotParameters();
			WritableImage      wImage    = canvas.snapshot(snapParam, null);
			String             fileName  = "scene_" + sectionNum;
			Log.savePicture(fileName, wImage);

			this.setCenter(canvas);
//			return canvas;

		} catch ( Exception e){
			e.printStackTrace();
		}
	}







	public void createDefaultPagePart(){
		TextArea tArea = new TextArea();

		tArea.setPrefSize(150, 450);
		tArea.setWrapText(true);
		tArea.setText("ここにページ番号が表示される予定です");
		tArea.setStyle("-fx-font-size:16pt;");

		this.setLeft(tArea);
//		return tArea;
	}


	public void createPagePart(int sectionNum){
		TextArea tArea = new TextArea();

		tArea.setPrefSize(150, 450);
		tArea.setWrapText(true);
		String str = "PAGE : " + (sectionNum+1) + " / " + this.stories_.size();
		tArea.setText(str);
		tArea.setStyle("-fx-font-size:16pt;");

		this.setLeft(tArea);
//		return tArea;
	}







	public void createDefaultMoveButton(){
		TextArea bArea = new TextArea();

		bArea.setPrefSize(1180, 100);
		bArea.setWrapText(true);
		bArea.setText("ここに移動用ボタンが表示される予定です");
		bArea.setStyle("-fx-font-size:16pt;");

		this.setBottom(bArea);
//		return tArea;
	}


	public void createMoveButton(){
		HBox box = new HBox(30);
		box.setPrefSize(1180, 100);

		Button backButton = new Button();
		backButton.setPrefSize(100, 40);
		backButton.setId("back");
		backButton.setText("BACK");
		backButton.setStyle("-fx-font-size:10pt;");
		backButton.setOnAction(actionHandler);
		backButton.setLayoutX(460);
		backButton.setLayoutY(30);
		backButton.setAlignment(Pos.CENTER);

		Button nextButton = new Button();
		nextButton.setPrefSize(100, 40);
		nextButton.setId("next");
		nextButton.setText("NEXT");
		nextButton.setStyle("-fx-font-size:10pt;");
		nextButton.setOnAction(actionHandler);
		nextButton.setLayoutX(720);
		nextButton.setLayoutY(30);
		nextButton.setAlignment(Pos.CENTER);

		box.getChildren().add(backButton);
		box.getChildren().add(nextButton);
		box.setAlignment(Pos.CENTER);

		this.setBottom(box);
	}








	public void setStoryInfo(){
		try{
		// 初期化
			this.stories_  = new ArrayList<String[]>();
			this.pictures_ = new ArrayList<String[]>();


		// 文章情報の設定
			String fileName = "LOG/seed" + Parameter.getRandomSeed() + "/story_sentence.txt";
			FileInputStream   sFis = new FileInputStream(fileName);
			InputStreamReader sIsr = new InputStreamReader(sFis, "SJIS");
			BufferedReader    sBr  = new BufferedReader(sIsr);
			String line;
			ArrayList<String> sList = new ArrayList<String>();
			while ( (line=sBr.readLine()) != null ){
				if ( line.equals("") ){
					String[] section = (String[])sList.toArray(new String[0]);
					this.stories_.add(section);
					sList.clear();
				}else{
					sList.add(line);
				}
			}sBr.close();
			sIsr.close();
			sFis.close();

		// 絵の初期設定
			fileName = "LOG/seed" + Parameter.getRandomSeed() + "/story_picture_";
			for ( int i = 0; i < this.stories_.size(); i++ ){
				FileInputStream   pFis = new FileInputStream(fileName +i+".txt");
				InputStreamReader pIsr = new InputStreamReader(pFis, "SJIS");
				BufferedReader    pBr  = new BufferedReader(pIsr);
				ArrayList<String> pList = new ArrayList<String>();
				while ( (line=pBr.readLine()) != null ){
					pList.add(line);
				}
				pBr.close();
				pIsr.close();
				pFis.close();
				String[] section = (String[])pList.toArray(new String[0]);
				this.pictures_.add(section);
			}

		} catch ( Exception e){
			e.printStackTrace();
		}
	}








	EventHandler<ActionEvent> actionHandler = new EventHandler<ActionEvent>(){
		public void handle(ActionEvent e){
			Button target = (Button)e.getTarget();
			if ( target.getId().equals("back") ){
				if ( sectionNum_ > 0 ){
					sectionNum_--;
					RootManager.repaintNarrativeRoot(sectionNum_);
				}else{
					System.out.println("Current section number is minimum.");
				}
			}else if ( target.getId().equals("next") ){
				if ( sectionNum_ < stories_.size()-1 ){
					sectionNum_++;
					RootManager.repaintNarrativeRoot(sectionNum_);
				}else{
					System.out.println("Current section number is maximum.");
				}
			}

		}
	};
}
