package gui;

import generator.PictureGenerator;
import generator.StoryGenerator;
import main.Simulator;
import story_model.ModelMaker;
import util.Log;
import util.MyRandom;
import util.Parameter;
import util.Timer;

public class RootManager {


	public static SettingRoot sRoot_;
	public static NarrativeRoot nRoot_;
	public static long seed_;


	public static void setSettingRoot(SettingRoot sRoot, NarrativeRoot nRoot){
		sRoot_ = sRoot;
		nRoot_ = nRoot;
		seed_  = 0;
	}







	public static void transmitInformation(){
	// 乱数シードの情報
		seed_ = Long.valueOf(sRoot_.randSeed_.getText());
	// 登場人物エージェントの情報
		for ( int i = 0; i < sRoot_.playerName_.length; i++ ){
			sRoot_.playerName_[i]  = sRoot_.pNameArea_[i].getText();
			sRoot_.playerRole_[i]  = Parameter.getRoleNumber(sRoot_.pRoleArea_[i].getValue().toString());
			sRoot_.playerSex_[i]   = Parameter.getSexNumber(sRoot_.pSexArea_[i].getValue().toString());
			sRoot_.playerAge_[i]   = Integer.valueOf(sRoot_.pAgeArea_[i].getText());
			sRoot_.playerPers1_[i] = sRoot_.pPersBar1_[i].getValue();
			sRoot_.playerPers2_[i] = sRoot_.pPersBar2_[i].getValue();

		}
	// アイテムエージェントの情報
		for ( int i = 0; i < sRoot_.itemName_.length; i++ ){
			sRoot_.itemName_[i] = sRoot_.iNameArea_[i].getText();
			sRoot_.itemRole_[i] = Parameter.getRoleNumber((String)sRoot_.iRoleArea_[i].getValue());
		}

		for ( int i = 0; i < sRoot_.playerName_.length; i++ ){
//			System.out.println(sRoot_.playerName_[i] + "-" + sRoot_.playerRole_[i] + "-" + sRoot_.playerSex_[i] +
//					"-" + sRoot_.playerAge_[i]);
		}
		for ( int i = 0; i < sRoot_.itemName_.length; i++ ){
//			System.out.println(sRoot_.itemName_[i] + "-" + sRoot_.itemRole_[i]);
		}
	}







	public static void startSimulation(){
		int pNum = sRoot_.playerName_.length;
		int iNum = sRoot_.itemName_.length;
		int sNum = 3;
		int eNum = 0;

		double[][] personas = {{sRoot_.playerPers1_[0], sRoot_.playerPers2_[0]},
				{sRoot_.playerPers1_[1], sRoot_.playerPers2_[1]},
				{sRoot_.playerPers1_[2], sRoot_.playerPers2_[2]}};

		String[] sNames = {"2_との遭遇", "3_との遭遇", "ゴール"};
		int[]    sRoles = {8, 8, 7};

		for ( int i = 0; i < 1; i++ ){
			System.out.println(seed_ + "," + sRoot_.playerPers1_[0]);
			Parameter.init(seed_, (sRoot_.playerName_.length+sRoot_.itemName_.length+3));
			MyRandom.init();
			MyRandom.getInstance(Parameter.getRandomSeed());
			Log.init("LOG/");

			Timer      t   = new Timer();
			t.getStartTime();
			Simulator  sim = new Simulator(pNum, iNum, sNum, eNum);
			ModelMaker mm  = new ModelMaker(pNum, iNum, sNum);

			sim.setCharacterParameter(sRoot_.playerName_, sRoot_.playerRole_, sRoot_.playerSex_,
					sRoot_.playerAge_, personas);
			sim.setItemParameter(sRoot_.itemName_, sRoot_.itemRole_);
			sim.setSceneParameter(sNames, sRoles);
			sim.prepare();
			t.getStopTime();
			t.showExcuteTime(true);


			int     turn     = 0;
			boolean isFinish = false;
			while ( !isFinish){
				if ( Parameter.getTurn()%100 == 0 ){
					System.out.println("Turn:" + Parameter.getTurn());
					t.getStopTime();
					t.showExcuteTime(true);
				}
				isFinish = sim.iteration(false);

			}
			System.out.println("Turn:" + Parameter.getTurn());
			t.getStopTime();
			t.showExcuteTime(true);

			String dir       = "Log/seed" + Parameter.getRandomSeed() + "/";
			String aFileName = dir + "Action/Action_all.csv";
			String sFileName = dir + "Status/Status_all.csv";
			mm.makeStoryModel(aFileName, sFileName);


			String[] names    = new String[pNum+iNum];
			int[]    roleNums = new int[pNum+iNum];
			for ( int j = 0; j < pNum; j++ ){
				names[j]    = sRoot_.playerName_[j];
				roleNums[j] = sRoot_.playerRole_[j];
			}
			for ( int j = 0; j < iNum; j++ ){
				names[pNum+j]    = sRoot_.itemName_[j];
				roleNums[pNum+j] = sRoot_.itemRole_[j];
			}

			StoryGenerator   sMaker = new StoryGenerator(names, roleNums, mm);
			PictureGenerator pMaker = new PictureGenerator(names, roleNums, mm);
			sMaker.makeStoryFromModels();
			pMaker.makePictureFromModels();



			t.getStopTime();
			t.showExcuteTime(true);
		}
	}







	public static void renewNarrativeRoot(){
		nRoot_.sectionNum_ = 0;
		nRoot_.setStoryInfo();
		repaintNarrativeRoot(nRoot_.sectionNum_);
	}






	public static void repaintNarrativeRoot(int num){
		nRoot_.createStoryPart(num);
		nRoot_.createPicturePart(num);
		nRoot_.createPagePart(num);
		nRoot_.createMoveButton();
	}
}





