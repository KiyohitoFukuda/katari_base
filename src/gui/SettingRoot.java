package gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

public class SettingRoot extends VBox{

	public String[] playerName_;		// プレイヤエージェントの名前
	public int[]    playerRole_;		// プレイヤエージェントの役割
	public int[]    playerSex_;		// プレイヤエージェントの性別
	public int[]    playerAge_;		// プレイヤエージェントの年齢
	public double[] playerPers1_;		// プレイヤエージェントの性格値 (鈍感-敏感)
	public double[] playerPers2_;		// プレイヤエージェントの性格値 (用心深い-好奇心旺盛)

	public String[] itemName_;		// アイテムエージェントの名前
	public int[] itemRole_;		// アイテムエージェントの役割

	public TextField[] pNameArea_;		// プレイヤエージェントの名前の書き込みスペース
	public ComboBox[]  pRoleArea_;		// プレイヤエージェントの役割の選択スペース
	public ComboBox[]  pSexArea_;		// プレイヤエージェントの性別の選択スペース
	public TextField[] pAgeArea_;		// プレイヤエージェントの年齢の書き込みスペース
	public Slider[]    pPersBar1_;		// プレイヤエージェントの性格値バー (鈍感-敏感)
	public Slider[]    pPersBar2_;		// プレイヤエージェントの性格値バー (用心深い-好奇心旺盛)

	public TextField[] iNameArea_;		// アイテムエージェントの名前の書き込みスペース
	public ComboBox[]  iRoleArea_;		// アイテムエージェントの役割の選択スペース

	public TextField   randSeed_;		// 乱数シード



/**
 * コンストラクタ
 */
	public SettingRoot(){
		this.setPrefSize(1180, 700);

	}





	public void createParameterBox(){
		HBox parameters = new HBox(30);
		parameters.setPrefSize(1180, 80);
		parameters.setMaxHeight(120);
		parameters.setAlignment(Pos.CENTER);

		this.randSeed_ = new TextField();
		this.randSeed_.setPrefSize(120, 40);
		this.randSeed_.setStyle("-fx-font-size:10pt;");
		this.randSeed_.setPromptText("乱数シードを入力");

		parameters.getChildren().add(this.randSeed_);
		this.getChildren().add(parameters);
	}





	public void createSettingBox(int playerNum, int itemNum, boolean isRestricted){
		setParameterSize(playerNum, itemNum);
		HBox playerSetting = new HBox(30);
		playerSetting.setPrefSize((320*playerNum+20), 220);
		HBox itemSetting = new HBox(30);
		itemSetting.setPrefSize((320*playerNum+20), 220);

		for ( int i = 0; i < playerNum; i++ ){
			Group pg;
			if ( !isRestricted ){
				pg = getPlayerSettingGroup(i);
			}else{
				pg = getRestrictedPlayerSettingGroup(i);
			}
			playerSetting.getChildren().add(pg);
		}
		for ( int i = 0; i < itemNum; i++ ){
			Group ig;
			if ( !isRestricted ){
				ig = getItemSettingGroup(i);
			}else{
				ig = getRestrictedItemSettingGroup(i);
			}
			itemSetting.getChildren().add(ig);
		}

		ScrollPane pScroll = new ScrollPane();
		pScroll.setContent(playerSetting);
		pScroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		pScroll.setVbarPolicy(ScrollBarPolicy.NEVER);
		this.getChildren().add(pScroll);
		ScrollPane iScroll = new ScrollPane();
		iScroll.setContent(itemSetting);
		iScroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		iScroll.setVbarPolicy(ScrollBarPolicy.NEVER);
		this.getChildren().add(iScroll);
	}





	public void createButtonSet(){
		HBox buttonSet = new HBox(30);
		buttonSet.setPrefSize(1180, 80);

		Button eButton = new Button();
		eButton.setText("生成開始");
		eButton.setPrefSize(120, 30);
		eButton.setStyle("-fx-font-size:10pt;");
		eButton.setOnAction(actionHandler);

		buttonSet.getChildren().add(eButton);
		buttonSet.setAlignment(Pos.CENTER);
		this.getChildren().add(buttonSet);
//		this.setAlignment(Pos.CENTER);
	}





	public Group getPlayerSettingGroup(int num){
		Group sGroup = new Group();
		sGroup.minWidth(300);
		sGroup.minHeight(120);
		sGroup.prefWidth(320);
		sGroup.prefHeight(120);

		this.pNameArea_[num] = new TextField();
		this.pRoleArea_[num] = new ComboBox<String>();
		this.pSexArea_[num]  = new ComboBox<String>();
		this.pAgeArea_[num]  = new TextField();
		this.pPersBar1_[num] = new Slider(-0.5, 0.5, 0.0);
		this.pPersBar2_[num] = new Slider(-0.5, 0.5, 0.0);

	// 名前
		this.pNameArea_[num].setPromptText((num+1) + " 人目の登場人物の名前");
		this.pNameArea_[num].setPrefSize(220, 40);
		this.pNameArea_[num].setStyle("-fx-font-size:14pt;");
		this.pNameArea_[num].setLayoutX(10);
		this.pNameArea_[num].setLayoutY(10);

	// 役割
		ObservableList<String> roles = FXCollections.observableArrayList();
		roles.add("未設定");
		roles.add("主人公");
//		roles.add("ヒロイン");
		roles.add("敵");
		roles.add("仲間");
		this.pRoleArea_[num].setPromptText("未設定");
		this.pRoleArea_[num].setItems(roles);
		this.pRoleArea_[num].setPrefSize(120, 40);
		this.pRoleArea_[num].setStyle("-fx-font-size:10pt;");
		this.pRoleArea_[num].setLayoutX(10);
		this.pRoleArea_[num].setLayoutY(70);

	// 性別
		ObservableList<String> sexes = FXCollections.observableArrayList();
		sexes.add("未設定");
		sexes.add("男性");
		sexes.add("女性");
		this.pSexArea_[num].setPromptText("未設定");
		this.pSexArea_[num].setItems(sexes);
		this.pSexArea_[num].setPrefSize(100, 40);
		this.pSexArea_[num].setStyle("-fx-font-size:10pt;");
		this.pSexArea_[num].setLayoutX(250);
		this.pSexArea_[num].setLayoutY(10);

	// 年齢
		this.pAgeArea_[num].setPromptText((num+1) + " 人目の年齢 (数字のみ)" );
		this.pAgeArea_[num].setPrefSize(200, 40);
		this.pAgeArea_[num].setStyle("-fx-font-size:12pt;");
		this.pAgeArea_[num].setLayoutX(150);
		this.pAgeArea_[num].setLayoutY(70);

	// 性格
		Label minVal1     = new Label("鈍感");
		Label maxVal1     = new Label("敏感");
		this.pPersBar1_[num].setPrefSize(170, 3);
		this.pPersBar1_[num].setLayoutX(90);
		this.pPersBar1_[num].setLayoutY(130);
		minVal1.setPrefSize(70, 30);
		minVal1.setStyle("-fx-font-size:10pt;");
		minVal1.setLayoutX(20);
		minVal1.setLayoutY(130);
		minVal1.setTextAlignment(TextAlignment.RIGHT);
		minVal1.setAlignment(Pos.BASELINE_RIGHT);
		maxVal1.setPrefSize(80, 30);
		maxVal1.setStyle("-fx-font-size:10pt;");
		maxVal1.setLayoutX(260);
		maxVal1.setLayoutY(130);
		maxVal1.setTextAlignment(TextAlignment.LEFT);
		maxVal1.setAlignment(Pos.BASELINE_LEFT);
		Label minVal2     = new Label("用心深い");
		Label maxVal2     = new Label("好奇心旺盛な");
		this.pPersBar2_[num].setPrefSize(170, 3);
		this.pPersBar2_[num].setLayoutX(90);
		this.pPersBar2_[num].setLayoutY(180);
		minVal2.setPrefSize(70, 30);
		minVal2.setStyle("-fx-font-size:10pt;");
		minVal2.setLayoutX(20);
		minVal2.setLayoutY(180);
		minVal2.setTextAlignment(TextAlignment.RIGHT);
		minVal2.setAlignment(Pos.BASELINE_RIGHT);
		maxVal2.setPrefSize(80, 30);
		maxVal2.setStyle("-fx-font-size:10pt;");
		maxVal2.setLayoutX(260);
		maxVal2.setLayoutY(180);
		maxVal2.setTextAlignment(TextAlignment.LEFT);
		maxVal2.setAlignment(Pos.BASELINE_LEFT);


		sGroup.getChildren().add(this.pNameArea_[num]);
		sGroup.getChildren().add(this.pRoleArea_[num]);
		sGroup.getChildren().add(this.pSexArea_[num]);
		sGroup.getChildren().add(this.pAgeArea_[num]);
		sGroup.getChildren().add(this.pPersBar1_[num]);
		sGroup.getChildren().add(minVal1);
		sGroup.getChildren().add(maxVal1);
		sGroup.getChildren().add(this.pPersBar2_[num]);
		sGroup.getChildren().add(minVal2);
		sGroup.getChildren().add(maxVal2);

		return sGroup;
	}





	public Group getRestrictedPlayerSettingGroup(int num){
		Group sGroup = new Group();
		sGroup.minWidth(300);
		sGroup.minHeight(120);
		sGroup.prefWidth(320);
		sGroup.prefHeight(120);

		this.pNameArea_[num] = new TextField();
		this.pRoleArea_[num] = new ComboBox<String>();
		this.pSexArea_[num]  = new ComboBox<String>();
		this.pAgeArea_[num]  = new TextField();
		this.pPersBar1_[num] = new Slider(-0.5, 0.5, 0.0);
		this.pPersBar2_[num] = new Slider(-0.5, 0.5, 0.0);

	// 名前
		this.pNameArea_[num].setPromptText((num+1) + " 人目の登場人物の名前");
		this.pNameArea_[num].setPrefSize(220, 40);
		this.pNameArea_[num].setStyle("-fx-font-size:14pt;");
		this.pNameArea_[num].setLayoutX(10);
		this.pNameArea_[num].setLayoutY(10);

	// 役割・性別
		ObservableList<String> roles = FXCollections.observableArrayList();
		ObservableList<String> sexes = FXCollections.observableArrayList();
		roles.add("未設定");
		sexes.add("未設定");
		switch ( num ){
		case 0:
			roles.add("主人公");
			sexes.add("女性");
			break;
		case 1:
			roles.add("敵");
			sexes.add("男性");
			break;
		case 2:
			roles.add("仲間");
			sexes.add("男性");
			break;
		}
		this.pRoleArea_[num].setPromptText("未設定");
		this.pRoleArea_[num].setItems(roles);
		this.pRoleArea_[num].setPrefSize(120, 40);
		this.pRoleArea_[num].setStyle("-fx-font-size:10pt;");
		this.pRoleArea_[num].setLayoutX(10);
		this.pRoleArea_[num].setLayoutY(70);
		this.pSexArea_[num].setPromptText("未設定");
		this.pSexArea_[num].setItems(sexes);
		this.pSexArea_[num].setPrefSize(100, 40);
		this.pSexArea_[num].setStyle("-fx-font-size:10pt;");
		this.pSexArea_[num].setLayoutX(250);
		this.pSexArea_[num].setLayoutY(10);

	// 年齢
		this.pAgeArea_[num].setPromptText((num+1) + " 人目の年齢 (数字のみ)" );
		this.pAgeArea_[num].setPrefSize(200, 40);
		this.pAgeArea_[num].setStyle("-fx-font-size:12pt;");
		this.pAgeArea_[num].setLayoutX(150);
		this.pAgeArea_[num].setLayoutY(70);

	// 性格
		switch ( num ){
		case 0:
			this.pPersBar1_[num].setValue(-0.3);
			this.pPersBar2_[num].setValue(0.3);
			break;
		case 1:
			this.pPersBar1_[num].setValue(0.3);
			this.pPersBar2_[num].setValue(-0.4);
			break;
		case 2:
			this.pPersBar1_[num].setValue(-0.1);
			this.pPersBar2_[num].setValue(0.2);
			break;
		}
		Label minVal1     = new Label("鈍感");
		Label maxVal1     = new Label("敏感");
		this.pPersBar1_[num].setPrefSize(170, 3);
		this.pPersBar1_[num].setLayoutX(90);
		this.pPersBar1_[num].setLayoutY(130);
		minVal1.setPrefSize(70, 30);
		minVal1.setStyle("-fx-font-size:10pt;");
		minVal1.setLayoutX(20);
		minVal1.setLayoutY(130);
		minVal1.setTextAlignment(TextAlignment.RIGHT);
		minVal1.setAlignment(Pos.BASELINE_RIGHT);
		maxVal1.setPrefSize(80, 30);
		maxVal1.setStyle("-fx-font-size:10pt;");
		maxVal1.setLayoutX(260);
		maxVal1.setLayoutY(130);
		maxVal1.setTextAlignment(TextAlignment.LEFT);
		maxVal1.setAlignment(Pos.BASELINE_LEFT);
		Label minVal2     = new Label("用心深い");
		Label maxVal2     = new Label("好奇心旺盛な");
		this.pPersBar2_[num].setPrefSize(170, 3);
		this.pPersBar2_[num].setLayoutX(90);
		this.pPersBar2_[num].setLayoutY(180);
		minVal2.setPrefSize(70, 30);
		minVal2.setStyle("-fx-font-size:10pt;");
		minVal2.setLayoutX(20);
		minVal2.setLayoutY(180);
		minVal2.setTextAlignment(TextAlignment.RIGHT);
		minVal2.setAlignment(Pos.BASELINE_RIGHT);
		maxVal2.setPrefSize(80, 30);
		maxVal2.setStyle("-fx-font-size:10pt;");
		maxVal2.setLayoutX(260);
		maxVal2.setLayoutY(180);
		maxVal2.setTextAlignment(TextAlignment.LEFT);
		maxVal2.setAlignment(Pos.BASELINE_LEFT);


		sGroup.getChildren().add(this.pNameArea_[num]);
		sGroup.getChildren().add(this.pRoleArea_[num]);
		sGroup.getChildren().add(this.pSexArea_[num]);
		sGroup.getChildren().add(this.pAgeArea_[num]);
		sGroup.getChildren().add(this.pPersBar1_[num]);
		sGroup.getChildren().add(minVal1);
		sGroup.getChildren().add(maxVal1);
		sGroup.getChildren().add(this.pPersBar2_[num]);
		sGroup.getChildren().add(minVal2);
		sGroup.getChildren().add(maxVal2);

		return sGroup;
	}





	public Group getItemSettingGroup(int num){
		Group sGroup = new Group();
		sGroup.minWidth(300);
		sGroup.minHeight(120);

		this.iNameArea_[num] = new TextField();
		this.iRoleArea_[num] = new ComboBox<String>();

		this.iNameArea_[num].setPromptText((num+1) + " 人目のアイテムの名前");
		this.iNameArea_[num].setPrefSize(220, 40);
		this.iNameArea_[num].setStyle("-fx-font-size:16pt;");
		this.iNameArea_[num].setLayoutX(10);
		this.iNameArea_[num].setLayoutY(10);

		ObservableList<String> roles = FXCollections.observableArrayList();
		roles.add("未設定");
		roles.add("食べ物");
		roles.add("武器");
		this.iRoleArea_[num].setPromptText("未設定");
		this.iRoleArea_[num].setItems(roles);
		this.iRoleArea_[num].setPrefSize(120, 40);
		this.iRoleArea_[num].setStyle("-fx-font-size:10pt;");
		this.iRoleArea_[num].setLayoutX(10);
		this.iRoleArea_[num].setLayoutY(70);

		sGroup.getChildren().add(this.iNameArea_[num]);
		sGroup.getChildren().add(this.iRoleArea_[num]);
		return sGroup;
	}





	public Group getRestrictedItemSettingGroup(int num){
		Group sGroup = new Group();
		sGroup.minWidth(300);
		sGroup.minHeight(120);

		this.iNameArea_[num] = new TextField();
		this.iRoleArea_[num] = new ComboBox<String>();

		this.iNameArea_[num].setPromptText((num+1) + " 人目のアイテムの名前");
		this.iNameArea_[num].setPrefSize(220, 40);
		this.iNameArea_[num].setStyle("-fx-font-size:16pt;");
		this.iNameArea_[num].setLayoutX(10);
		this.iNameArea_[num].setLayoutY(10);

		ObservableList<String> roles = FXCollections.observableArrayList();
		roles.add("未設定");
		switch ( num ){
		case 0:
			roles.add("食べ物");
			break;
		case 1:
			roles.add("武器");
			break;
		}
		this.iRoleArea_[num].setPromptText("未設定");
		this.iRoleArea_[num].setItems(roles);
		this.iRoleArea_[num].setPrefSize(120, 40);
		this.iRoleArea_[num].setStyle("-fx-font-size:10pt;");
		this.iRoleArea_[num].setLayoutX(10);
		this.iRoleArea_[num].setLayoutY(70);

		sGroup.getChildren().add(this.iNameArea_[num]);
		sGroup.getChildren().add(this.iRoleArea_[num]);
		return sGroup;
	}







	private void setParameterSize(int pSize, int iSize){
		this.playerName_  = new String[pSize];
		this.playerRole_  = new int[pSize];
		this.playerSex_   = new int[pSize];
		this.playerAge_   = new int[pSize];
		this.playerPers1_ = new double[pSize];
		this.playerPers2_ = new double[pSize];
		this.itemName_    = new String[iSize];
		this.itemRole_    = new int[iSize];

		this.pNameArea_ = new TextField[pSize];
		this.pRoleArea_ = new ComboBox[pSize];
		this.pSexArea_  = new ComboBox[pSize];
		this.pAgeArea_  = new TextField[pSize];
		this.pPersBar1_ = new Slider[pSize];
		this.pPersBar2_ = new Slider[pSize];
		this.iNameArea_ = new TextField[iSize];
		this.iRoleArea_ = new ComboBox[iSize];
	}







	EventHandler<ActionEvent> actionHandler = new EventHandler<ActionEvent>(){
		public void handle(ActionEvent e){
			RootManager.transmitInformation();
			RootManager.startSimulation();
			RootManager.renewNarrativeRoot();
		}
	};


}






