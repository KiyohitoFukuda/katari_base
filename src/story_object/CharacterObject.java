package story_object;

import java.util.ArrayList;

import util.Function;
import util.Parameter;

public class CharacterObject implements Agent{

// 種別
	private final int TYPE_ = 1;
// OID
	private int      oID_;
// 役割
	private int      role_;
// 名前
	private String   name_;
// 性別
	private int      sex_;
// 年齢
	private int      age_;
// 性格
	private double[] persona_;
// 感情
	private double   emotion_;
// 健康状態
	private double   condition_;
// フィールド上の位置
	private int[]    fPos_;
// シーン上の位置
	private int[]    sPos_;
// オブジェクトに対する友好度
	private double[] friendship_;
// オブジェクトに対する認知度
	private double[] familiarity_;
// オブジェクトに対する影響値
	private double[] influence_;
// 所有アイテムリスト
	private ArrayList<Integer> itemList_;

// 直前に接触した登場人物の OID
	private int cID_;
// 直前に通過したシーンの OID
	private int sID_;
// オブジェクトに対するインターバル
	private int[] intervals_;

// 関数
	private Function func_;



	public CharacterObject(){
		this.oID_         = 0;
		this.role_        = 0;
		this.name_        = "No_Name";
		this.sex_         = 0;
		this.age_         = 0;
		this.persona_     = new double[2];
		this.emotion_     = -100;
		this.condition_   = -100;
		this.fPos_        = new int[2];
		this.sPos_        = new int[2];
		this.friendship_  = new double[Parameter.getNumberOfObject()];
		this.familiarity_ = new double[Parameter.getNumberOfObject()];
		this.influence_   = new double[Parameter.getNumberOfObject()];
		this.itemList_    = new ArrayList<Integer>();
		this.cID_         = 0;
		this.sID_         = 0;
		this.intervals_   = new int[Parameter.getNumberOfObject()];
		this.func_        = new Function();

		this.fPos_[0] = -1;
		this.fPos_[1] = -1;
		this.sPos_[0] = -1;
		this.sPos_[1] = -1;
	}







/**
 * 登場人物の初期設定をする.
 * @param oID OID
 * @param role 役割
 * @param name 名前
 * @param sex 性別
 * @param age 年齢
 */
	public void initialize(int oID, int role, String name, int sex, int age){
	// OID, 役割, 名前, 性別, 年齢
		this.setObjectId(oID);
		this.setRole(role);
		this.setName(name);
		this.setSex(sex);
		this.setAge(age);

	// 性格
		for ( int i = 0; i < this.persona_.length; i++ ){
			this.setPersonality(this.func_.randomDouble(0.45, 0.55), i);
		}
	// 感情
		this.setEmotion(this.func_.randomDouble(-0.6, 0.6));
	// 健康状態
		this.setCondition(this.func_.randomDouble(0.75, 1.0));
	// フィールド上の位置
		int x = 0;
		int y = 0;
		while ( (x<(Parameter.getFieldSizeX()/2)) && (y<(Parameter.getFieldSizeY()/2)) ){
			x = this.func_.randomInt(0, Parameter.getFieldSizeX()-1);
			y = this.func_.randomInt(0, Parameter.getFieldSizeY()-1);
			this.setFPositionX(x);
			this.setFPositionY(y);
		}
	// 友好度・認知度・影響値
		for ( int i = 0; i < Parameter.getNumberOfObject(); i++ ){
			if ( this.getObjectID() != (i+1) ){
				this.setFriendshipValue(this.func_.randomDouble(-60, 60), i+1);
				this.setFamiliarityValue(this.func_.randomDouble(-60, 20), i+1);
				this.setInfluenceValue(this.func_.randomDouble(30, 80), i+1);
			}
		}
	}







/**
 * フィールド上の位置を再設定する.
 * 初期設定で他のオブジェクトと位置がかぶってしまった場合を想定している.
 */
	public void resetPosition(){
		int x = 0;
		int y = 0;
		while ( (x<(Parameter.getFieldSizeX()/2)) && (y<(Parameter.getFieldSizeY()/2)) ){
			x = this.func_.randomInt(0, Parameter.getFieldSizeX()-1);
			y = this.func_.randomInt(0, Parameter.getFieldSizeY()-1);
			this.setFPositionX(x);
			this.setFPositionY(y);
		}
	}







/**
 * OID が oID のオブジェクトに対するインターバルを更新する.
 * @param min 下限
 * @param max 上限
 */
	public void resetInterval(int oID, int min, int max){
		this.intervals_[oID-1] = this.func_.randomInt(min, max);
	}







/**
 * 性格の値に personas だけ追加する.
 * @param personas 性格の値の追加量
 */
	public void addPersonality(double[] personas){
		double[] temp = this.getPersonality();
		for ( int i = 0; i < this.persona_.length; i++ ){
			double value = temp[i] + personas[i];
		// 範囲確認
		// 値が範囲外であれば, 最大値もしくは最小値に修正
			if ( value > Parameter.getMaxPersonality() ){
				value = Parameter.getMaxPersonality();
			}
			if ( value < Parameter.getMinPersonality() ){
				value = Parameter.getMinPersonality();
			}
			this.setPersonality(value, i);
		}
	}







/**
 * 健康状態の値に condition だけ追加する.
 * @param condition
 */
	public void addCondition(double condition){
		double value = this.getCondition() + condition;
	// 範囲確認
	// 値が範囲外であれば, 最大値もしくは最小値に修正
		if ( value > Parameter.getMaxCondition() ){
			value = Parameter.getMaxCondition();
		}
		if ( value < Parameter.getMinCondition() ){
			value = Parameter.getMinCondition();
		}
		this.setCondition(value);
	}







/**
 * 感情の値を変更する.
 * @param emotion
 */
	public void changeEmotion(double emotion){
		double value = emotion / 100.0;
		value += this.getEmotion();
	// 範囲確認
	// 値が範囲外であれば, 最大値もしくは最小値に修正
		if ( value > Parameter.getMaxEmotion() ){
			value = Parameter.getMaxEmotion();
		}
		if ( value < Parameter.getMinEmotion() ){
			value = Parameter.getMinEmotion();
		}
		this.setEmotion(value);
	}







/**
 * OID が oID のオブジェクトに対する友好度を変更する.
 * @param frValue
 * @param oID
 */
	public void changeFriendshipValue(double frValue, int oID){
		double value = this.getFriendshipValue(oID) + frValue;
	// 範囲確認
	// 値が範囲外であれば, 最大値もしくは最小値に修正
		if ( value > Parameter.getMaxFriendshipValue() ){
			value = Parameter.getMaxFriendshipValue();
		}
		if ( value < Parameter.getMinFriendshipValue() ){
			value = Parameter.getMinFriendshipValue();
		}
		this.setFriendshipValue(value, oID);
	}







/**
 * OID が oID のオブジェクトに対する認知度を変更する.
 * @param faValue
 * @param oID
 */
	public void changeFamiliarityValue(double faValue, int oID){
		double value = this.getFamiliarityValue(oID) + faValue;
	// 範囲確認
	// 値が範囲外であれば, 最大値もしくは最小値に修正
		if ( value > Parameter.getMaxFamiliarityValue() ){
			value = Parameter.getMaxFamiliarityValue();
		}
		if ( value < Parameter.getMinFamiliarityValue() ){
			value = Parameter.getMinFamiliarityValue();
		}
		this.setFamiliarityValue(value, oID);
	}







/**
 * OID が oID のオブジェクトに対する影響値を変更する.
 *
 * @param iValue
 * @param oID
 */
	public void changeInfluenceValue(double iValue, int oID){
		double value = this.getInfluenceValue(oID) + iValue;
	// 範囲確認
	// 値が範囲外であれば, 最大値もしくは最小値に修正
		if ( value > Parameter.getMaxInfluenceValue() ){
			value = Parameter.getMaxInfluenceValue();
		}
		if ( value < Parameter.getMinInfluenceValue() ){
			value = Parameter.getMinInfluenceValue();
		}
		this.setInfluenceValue(value, oID);
	}







/**
 * 時間経過によって, 他のオブジェクトに対する友好度および自身の健康状態・影響値を更新する.
 * 各種インターバルもここで更新する.
 */
	public void renewValueByTimeCourse(){
	// 他のオブジェクトに対する友好度・影響値および自身の感情の更新
		for ( int i = 0; i < Parameter.getNumberOfObject(); i++ ){
			if ( this.getObjectID() != (i+1) ){
				double myu     = -0.01 * this.getFriendshipValue(i+1) / Math.abs(this.getFriendshipValue(i+1));
				double frValue = this.func_.randomGaussian(myu, 1);
				double iValue  = this.func_.randomGaussian(0, 1);
				this.changeFriendshipValue(frValue, i+1);
				this.changeInfluenceValue(iValue, i+1);
				this.changeEmotion(frValue);
			}
		// オブジェクトに対するインターバル
			if ( this.intervals_[i] > 0 ){
				this.intervals_[i]--;
			}
		}
	// 健康状態の更新
		double cValue = Math.pow(0.5, 0.001) * this.getCondition();
		this.setCondition(cValue);
	}







/**
 * アイテムリストにアイテムを追加する.
 * アイテムは OID で管理する.
 * @param oID
 */
	public void addItem(int oID){
		Integer item = Integer.valueOf(oID);
		if ( !this.itemList_.contains(item) ){
			this.itemList_.add(item);
		}else{
			System.out.println("This item (OID:" + oID + ") is already contained.");
		}
	}


/**
 * アイテムリストからアイテムを削除する.
 * アイテムは OID で管理する.
 * @param oID
 */
	public void removeItem(int oID){
		Integer item = Integer.valueOf(oID);
		if ( this.itemList_.contains(item) ){
			this.itemList_.remove(item);
		}else{
			System.out.println("This item (OID:" + oID + ") is not contained yet.");
		}
	}







/**
 * OID が oID のオブジェクトに対するインターバルを調べる
 * インターバル 1 以上ならば true を返し, そうでなければ false を返す.
 * @param oID
 * @return
 */
	public boolean checkInterval(int oID){
		if ( this.intervals_[oID-1] > 0 ){
			return true;
		}else{
			return false;
		}
	}







	@Override
	public int getObjectID(){
		return this.oID_;
	}


	@Override
	public int getType(){
		return this.TYPE_;
	}


	@Override
	public int getRole() {
		return this.role_;
	}


	@Override
	public String getName() {
		return this.name_;
	}


	@Override
	public int getSex() {
		return this.sex_;
	}


	@Override
	public int getAge() {
		return this.age_;
	}


	@Override
	public double[] getPersonality() {
		return this.persona_;
	}


	@Override
	public double getEmotion() {
		return this.emotion_;
	}


	@Override
	public double getCondition() {
		return this.condition_;
	}


	@Override
	public int[] getFPosition() {
		return this.fPos_;
	}


	@Override
	public int getFPositionX() {
		return this.fPos_[0];
	}


	@Override
	public int getFPositionY() {
		return this.fPos_[1];
	}


	@Override
	public int[] getSPosition() {
		return this.sPos_;
	}


	@Override
	public int getSPositionX() {
		return this.sPos_[0];
	}


	@Override
	public int getSPositionY() {
		return this.sPos_[1];
	}


	@Override
	public double[] getFrinedshipValues() {
		return this.friendship_;
	}


	@Override
	public double getFriendshipValue(int oID) {
		return this.friendship_[oID-1];
	}


	@Override
	public double[] getFamiliarityValues() {
		return this.familiarity_;
	}


	@Override
	public double getFamiliarityValue(int oID) {
		return this.familiarity_[oID-1];
	}


	@Override
	public double[] getInfluenceValues() {
		return this.influence_;
	}


	@Override
	public double getInfluenceValue(int oID) {
		return this.influence_[oID-1];
	}


/**
 * オブジェクトが所有するアイテムリストを返す.
 * @return
 */
	public ArrayList<Integer> getItemList(){
		return this.itemList_;
	}


/**
 * 直前に接触した登場人物の OID を返す.
 * @return
 */
	public int getCharacterID(){
		return this.cID_;
	}


/**
 * 直前に到着したシーンの OID を返す.
 * @return
 */
	public int getSceneID(){
		return this.sID_;
	}







	@Override
	public void setObjectId(int oId) {
		this.oID_ = oId;
	}


	@Override
	public void setType(int type) {
		System.out.println("Object type can not be changed.");
	}


	@Override
	public void setName(String name) {
		this.name_ = name;
	}


	@Override
	public void setSex(int sex) {
		this.sex_ = sex;
	}


	@Override
	public void setAge(int age) {
		this.age_ = age;
	}


	@Override
	public void setPersonality(double[] personas) {
		for ( int i = 0; i < personas.length; i++ ){
			this.persona_[i] = personas[i];
		}
	}


	@Override
	public void setPersonality(double persona, int num) {
		this.persona_[num] = persona;
	}


	@Override
	public void setEmotion(double emotion) {
		this.emotion_ = emotion;
	}


	@Override
	public void setCondition(double condition) {
		this.condition_ = condition;
	}


	@Override
	public void setFPosition(int[] positions) {
		for ( int i = 0; i < positions.length; i++ ){
			this.fPos_[i] = positions[i];
		}
	}


	@Override
	public void setFPositionX(int position) {
		this.fPos_[0] = position;
	}


	@Override
	public void setFPositionY(int position) {
		this.fPos_[1] = position;
	}


	@Override
	public void setSPosition(int[] positions) {
		for ( int i = 0; i < positions.length; i++ ){
			this.sPos_[i] = positions[i];
		}
	}


	@Override
	public void setSPositionX(int position) {
		this.sPos_[0] = position;
	}


	@Override
	public void setSPositionY(int position) {
		this.sPos_[1] = position;
	}


	@Override
	public void setFriendshipValues(double[] fValues) {
		for ( int i = 0; i < fValues.length; i++ ){
			this.friendship_[i] = fValues[i];
		}
	}


	@Override
	public void setFriendshipValue(double fValue, int oID) {
		this.friendship_[oID-1] = fValue;
	}


	@Override
	public void setFamiliarityValues(double[] fValues) {
		for ( int i = 0; i < fValues.length; i++ ){
			this.familiarity_[i] = fValues[i];
		}
	}


	@Override
	public void setFamiliarityValue(double fValue, int oID) {
		this.familiarity_[oID-1] = fValue;
	}


	@Override
	public void setInfluenceValues(double[] iValues) {
		for ( int i = 0; i < this.influence_.length; i++ ){
			this.influence_[i] = iValues[i];
		}
	}


	@Override
	public void setInfluenceValue(double iValue, int oID) {
		this.influence_[oID-1] = iValue;
	}


	@Override
	public void setRole(int role) {
		this.role_ = role;

	}
}





