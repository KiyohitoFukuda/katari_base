package story_object;

import util.Function;
import util.Parameter;

public class Environment {

// EID
	private int   eID_;
// フィールド上の位置
	private int[] fPos_;
// 環境が与える影響値
	private double influence_;

// 関数
	private Function func_;



/**
 * コンストラクタ
 */
	public Environment(){
		this.fPos_      = new int[2];
		this.influence_ = 0.0;
		this.func_      = new Function();

		this.fPos_[0] = -1;
		this.fPos_[1] = -1;
	}







/**
 * 環境エージェントの初期設定をする.
 * @param eID
 */
	public void initialize(int eID){
	// EID
		this.setEnvironmentID(eID);
	// フィールド上の位置
		this.setFPositionX(this.func_.randomInt(0, Parameter.getFieldSizeX()));
		this.setFPositionY(this.func_.randomInt(0, Parameter.getFieldSizeY()));
	// 環境が与える影響値
		this.setInfluenceValue(this.func_.randomDouble(5, 15));
	}







/**
 * フィールド上の位置を再設定する.
 * 初期設定で他のオブジェクトと位置がかぶってしまった場合を想定している.
 */
	public void resetPosition(){
		this.setFPositionX(this.func_.randomInt(0, Parameter.getFieldSizeX()));
		this.setFPositionY(this.func_.randomInt(0, Parameter.getFieldSizeY()));
	}







/**
 * 環境エージェントの EID を返す.
 * 0 : 未設定
 * @return
 */
	public int getEnvironmentID(){
		return this.eID_;
	}


/**
 * 環境エージェントのフィールド上の位置を返す.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @return
 */
	public int[] getFPosition(){
		int[] ans = new int[2];
		ans[0] = this.fPos_[0];
		ans[1] = this.fPos_[1];
		return ans;
	}


/**
 * 環境エージェントのフィールド上の x 座標を返す.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @return
 */
	public int getFPositionX(){
		return this.fPos_[0];
	}


/**
 * 環境エージェントのフィールド上の y 座標を返す.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @return
 */
	public int getFPositionY(){
		return this.fPos_[1];
	}


/**
 * 環境エージェントがオブジェクトに与える影響値を返す.
 * 環境値は[0,100] で実数値かしたものを用いる.
 * @return
 */
	public double getInfluenceValue(){
		return this.influence_;
	}







/**
 * 環境エージェントの EID を設定する.
 * @param eID
 */
	public void setEnvironmentID(int eID){
		this.eID_ = eID;
	}


/**
 * 環境エージェントのフィールド上の位置を設定する.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @param positions
 */
	public void setFPosition(int[] positions){
		this.fPos_[0] = positions[0];
		this.fPos_[1] = positions[1];
	}


/**
 * 環境エージェントのフィールド上の x 軸を設定する.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @param position
 */
	public void setFPositionX(int position){
		this.fPos_[0] = position;
	}


/**
 * 環境エージェントのフィールド上の y 軸を設定する.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @param position
 */
	public void setFPositionY(int position){
		this.fPos_[1] = position;
	}


/**
 * 環境エージェントがオブジェクトに与える影響値を設定する.
 * 環境値は[0,100] で実数値かしたものを用いる.
 * @param influence
 */
	public void setInfluenceValue(double influence){
		this.influence_ = influence;
	}

}






