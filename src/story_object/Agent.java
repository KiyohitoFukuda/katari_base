package story_object;

public interface Agent {

	/**
	 * オブジェクトの OID を返す.
	 * 0 : 未設定
	 * @return OID
	 */
	public int getObjectID();





/**
 * オブジェクトの種別を返す.
 * 1 : 登場人物
 * 2 : アイテム
 * 3 : 場所
 * 0 : 未設定
 * @return
 */
	public int getType();





/**
 * オブジェクトの役割を返す.
 * 1 : 主人公
 * 2 : ヒロイン
 * 3 : 敵
 * 4 : 仲間
 * 5 : 食べ物
 * 6 : 武器
 * 7 : ゴール
 * 8 : イベント
 * 0 : 未設定
 * @return
 */
	public int getRole();





/**
 * オブジェクトの名前を返す.
 * 登場人物なら名前, アイテムならアイテム名, 場所なら地名や場面名を返す.
 * No_Name : 未設定
 * @return
 */
	public String getName();





/**
 * オブジェクトの性別を返す. (登場人物のみ)
 * 1 : 男性
 * 2 : 女性
 * 0 : 人物以外 or 未設定
 * @return
 */
	public int getSex();





/**
 * オブジェクトの年齢を返す. (登場人物のみ)
 * 0 : 人物以外 or 未設定
 * @return
 */
	public int getAge();





/**
 * オブジェクトの性格を返す. (登場人物のみ)
 * 現時点では性格として [-1,1] で実数値化したものを用いる.
 * [0] : 敏感 - 鈍感
 * [1] : 好奇心旺盛 - 用心深い
 * -100 : 人物以外 or 未設定
 * @return
 */
	public double[] getPersonality();





/**
 * オブジェクトの感情を返す. (登場人物のみ)
 * 現時点では感情はポジネガを [-1,1] で実数値化したものを用いる.
 * -100 : 人物以外 or 未設定
 * @return
 */
	public double getEmotion();





///**
// * オブジェクトの姿勢を返す. (登場人物とアイテムのみ)
// * 1 : 座っている (横たわっている)
// * 2 : 立っている
// * 0 : 場所 or 未設定
// * @return
// */
//	public int getPosture();





///**
// * オブジェクトの表情を返す. (登場人物のみ)
// * 1 : 楽しい・嬉しい
// * 2 : 悲しい
// * 3 : 怒り
// * 4 : 通常
// * 0 : 人物以外 or 未設定
// * @return
// */
//	public int getExpression();





/**
 * オブジェクトの健康状態を [0,1] で実数値化したものを返す.
 * -100 : 未設定
 * @return
 */
	public double getCondition();





///**
// * オブジェクトの絵中の位置を返す.
// * 1 (x 軸) : 左	1 (y 軸) : 上
// * 2 (x 軸) : 中央	2 (y 軸) : 中央
// * 3 (x 軸) : 右	3 (y 軸) : 下
// * 0 : 未設定
// * @return
// */
//	public int[] getPlace();
//
//
///**
// * オブジェクトの絵中の x 軸の位置を返す.
// * 1 : 左
// * 2 : 中央
// * 3 : 右
// * 0 : 未設定
// * @return
// */
//	public int getPlaceX();
//
//
///**
// * オブジェクトの絵中の y 軸の位置を返す.
// * 1 : 上
// * 2 : 中央
// * 3 : 下
// * 0 : 未設定
// * @return
// */
//	public int getPlaceY();





/**
 * オブジェクトのフィールド上の位置を返す.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @return
 */
	public int[] getFPosition();


/**
 * オブジェクトのフィールド上の x 座標を返す.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @return
 */
	public int getFPositionX();


/**
 * オブジェクトのフィールド上の y 座標を返す.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @return
 */
	public int getFPositionY();





/**
 * オブジェクトのシーン上の位置を返す.
 * シーン上にいない場合は {-1,-1} を返す.
 * @return
 */
	public int[] getSPosition();


/**
 * オブジェクトのシーン上の x 座標を返す.
 * シーン上にいない場合は -1 を返す.
 * @return
 */
	public int getSPositionX();


/**
 * オブジェクトのシーン上の y 座標を返す.
 * シーン上にいない場合は -1 を返す.
 * @return
 */
	public int getSPositionY();





/**
 * オブジェクトの他のオブジェクトに対する友好度を返す.
 * 友好度は [-100,100] で実数値化したものを用いる.
 * @return
 */
	public double[] getFrinedshipValues();


/**
 * オブジェクトの OID が oID であるオブジェクトに対する友好度を返す.
 * 友好度は [-100,100] で実数値化したものを用いる.
 * @param oID
 * @return
 */
	public double getFriendshipValue(int oID);





/**
 * オブジェクトの他のオブジェクトに対する認知度を返す.
 * 認知度は [-100,100] で実数値化したものを用いる.
 * @return
 */
	public double[] getFamiliarityValues();


/**
 * オブジェクトの OID が oID であるオブジェクトに対する認知度を返す.
 * 認知度は [-100,100] で実数値化したものを用いる.
 * @param oID
 * @return
 */
	public double getFamiliarityValue(int oID);


/**
 * オブジェクトの他のオブジェクトに対する影響値を返す.
 * 影響値は [0,100] で実数値化したものを用いる.
 * @return
 */
	public double[] getInfluenceValues();


/**
 * オブジェクトの OID が oID であるオブジェクトに対する影響値を返す.
 * 認知度は [-100,100] で実数値化したものを用いる.
 * @param oID
 * @return
 */
	public double getInfluenceValue(int oID);









/**
 * オブジェクトの OID を設定する.
 * @param oId
 */
	public void setObjectId(int oId);





/**
 * オブジェクトの種別を設定する.
 * 1 : 登場人物
 * 2 : アイテム
 * 3 : 場所
 * @param type
 */
	public void setType(int type);





/**
 * オブジェクトの役割を設定する.
 * 1 : 主人公
 * 2 : ヒロイン
 * 3 : 敵
 * 4 : 仲間
 * 5 : 食べ物
 * 6 : 武器
 * 7 : ゴール
 * 8 : イベント
 * @param role
 */
	public void setRole(int role);





/**
 * オブジェクトの名前を設定する.
 * @param name
 */
	public void setName(String name);





/**
 * オブジェクトの性別を設定する. (登場人物のみ)
 * 1 : 男性
 * 2 : 女性
 * 0 : 人物以外
 * @param sex
 */
	public void setSex(int sex);





/**
 * オブジェクトの年齢を設定する. (登場人物のみ)
 * @param age
 */
	public void setAge(int age);





/**
 * オブジェクトの性格を設定する. (登場人物のみ)
 * 現時点では性格として BIG5 を [-1,1] で実数値化したものを用いる.
 * @param personas
 */
	public void setPersonality(double[] personas);


/**
 * オブジェクトの性格を設定する. (登場人物のみ)
 * 現時点では性格として BIG5 を [-1,1] で実数値化したものを用いる.
 * BIG5 の num 番目を設定する.
 * @param persona
 * @param num
 */
	public void setPersonality(double persona, int num);





/**
 * オブジェクトの感情を設定する. (登場人物のみ)
 * 現時点では感情はポジネガを [-1,1] で実数値化したものを用いる.
 * @param emotion
 */
	public void setEmotion(double emotion);





///**
// * オブジェクトの姿勢を設定する. (登場人物とアイテムのみ)
// * 1 : 座っている (横たわっている)
// * 2 : 立っている
// * @param posture
// */
//	public void setPosture(int posture);
//
//
//
//
//
///**
// * オブジェクトの表情を設定する. (登場人物のみ)
// * 1 : 楽しい・嬉しい
// * 2 : 悲しい
// * 3 : 怒り
// * 4 : 通常
// * @param express
// */
//	public void setExpression(int express);





/**
 * オブジェクトの健康状態を設定する.
 * 健康状態は [0,1] で実数値化したものを用いる.
 * @return
 */
	public void setCondition(double condition);





///**
// * オブジェクトの絵中の位置を設定する.
// * 1 (x 軸) : 左	1 (y 軸) : 上
// * 2 (x 軸) : 中央	2 (y 軸) : 中央
// * 3 (x 軸) : 右	3 (y 軸) : 下
// * @param places
// */
//	public void setPlace(int[] places);
//
//
///**
// * オブジェクトの絵中の x 軸の位置を設定する.
// * 1 : 左
// * 2 : 中央
// * 3 : 右
// * @param place
// */
//	public void setPlaceX(int place);
//
//
///**
// * オブジェクトの絵中の y 軸の位置を設定する.
// * 1 : 上
// * 2 : 中央
// * 3 : 下
// * @param place
// */
//	public void setPlaceY(int place);





/**
 * オブジェクトのフィールド上の位置を設定する.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @param positions
 */
	public void setFPosition(int[] positions);


/**
 * オブジェクトのフィールド上の x 軸を設定する.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @param position
 */
	public void setFPositionX(int position);


/**
 * オブジェクトのフィールド上の y 軸を設定する.
 * フィールドは 100 × 100 の 2 次元空間である.
 * @param position
 */
	public void setFPositionY(int position);





/**
 * オブジェクトのシーン上の位置を設定する.
 * シーン上にいない場合は {-1,-1} に設定推奨.
 * @param positions
 */
	public void setSPosition(int[] positions);


/**
 * オブジェクトのシーン上の x 軸を設定する.
 * シーン上にいない場合は -1 に設定推奨.
 * @param position
 */
	public void setSPositionX(int position);


/**
 * オブジェクトのシーン上の y 軸を設定する.
 * シーン上にいない場合は -1 に設定推奨.
 * @param position
 */
	public void setSPositionY(int position);





/**
 * オブジェクトの他のオブジェクトに対する友好度を設定する.
 * 友好度は [-100,100] で実数値化したものを用いる.
 * @param fValues
 */
	public void setFriendshipValues(double[] fValues);


/**
 * オブジェクトの OID が oID であるオブジェクトに対する友好度を設定する.
 * 友好度は [-100,100] で実数値化したものを用いる.
 * @param fValue
 * @param oID
 */
	public void setFriendshipValue(double fValue, int oID);





/**
 * オブジェクトの他のオブジェクトに対する認知度を設定する.
 * 認知度は [-100,100] で実数値化したものを用いる.
 * @param fValues
 */
	public void setFamiliarityValues(double[] fValues);


/**
 * オブジェクトの OID が oID であるオブジェクトに対する認知度を設定する.
 * 認知度は [-100,100] で実数値化したものを用いる.
 * @param fValue
 * @param oID
 */
	public void setFamiliarityValue(double fValue, int oID);


/**
 * オブジェクトの他のオブジェクトに対する影響値を設定する.
 * 影響値は [0,100] で実数値化したものを用いる.
 *
 * @param iValues
 */
	public void setInfluenceValues(double[] iValues);


/**
 * オブジェクトの OID が oID であるオブジェクトに対する影響値を設定する.
 * 影響値は [0,100] で実数値化したものを用いる.
 *
 * @param iValue
 * @param OID
 */
	public void setInfluenceValue(double iValue, int OID);
}









