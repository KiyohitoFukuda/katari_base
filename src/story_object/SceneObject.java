package story_object;

import util.Function;
import util.Parameter;

public class SceneObject implements Agent{

// 種別
	private final int TYPE_ = 3;
// OID
	private int      oID_;
// 役割
	private int      role_;
// 名前
	private String   name_;
// 性別
	private int      sex_;
// 年齢
	private int      age_;
// 性格
	private double[] persona_;
// 感情
	private double   emotion_;
// 健康状態
	private double   condition_;
// フィールド上の位置
	private int[]    fPos_;
// シーン上の位置
	private int[]    sPos_;
// オブジェクトに対する友好度
	private double[] friendship_;
// オブジェクトに対する認知度
	private double[] familiarity_;
// オブジェクトに対する影響値
	private double[] influence_;

// 関数
	private Function func_;



	public SceneObject(){
		this.oID_         = 0;
		this.role_        = 0;
		this.name_        = "No_Name";
		this.sex_         = 0;
		this.age_         = 0;
		this.persona_     = new double[5];
		this.emotion_     = -100;
		this.condition_   = -100;
		this.fPos_        = new int[2];
		this.sPos_        = new int[2];
		this.friendship_  = new double[Parameter.getNumberOfObject()];
		this.familiarity_ = new double[Parameter.getNumberOfObject()];
		this.influence_   = new double[Parameter.getNumberOfObject()];
		this.func_        = new Function();
	}







/**
 * シーンの初期設定をする.
 * @param oID OID
 * @param role 役割
 * @param name 名前
 */
	public void initialize(int oID, int role, String name){
	// OID, 役割, 名前
		this.setObjectId(oID);
		this.setRole(role);
		this.setName(name);

	// 健康状態
		this.setCondition(this.func_.randomDouble(0.75, 1.0));
	// フィールド上の位置
	// 役割が 7 である場合とそれ以外で区別する
		if ( this.getRole() == Parameter.getRoleNumber("ゴール") ){
			this.setFPositionX(this.func_.randomInt(0, Parameter.getFieldSizeX()/5));
			this.setFPositionY(this.func_.randomInt(0, Parameter.getFieldSizeY()/5));
		}else{
			this.setFPositionX(this.func_.randomInt(0, Parameter.getFieldSizeX()/2));
			this.setFPositionY(this.func_.randomInt(0, Parameter.getFieldSizeY()/2));
		}


	// 影響値
		for ( int i = 0; i < Parameter.getNumberOfObject(); i++ ){
			if ( this.getObjectID() != (i+1) ){
				if ( this.getRole() == Parameter.getRoleNumber("ゴール") ){
					this.setInfluenceValue(1.0, i+1);
				}else{
					this.setInfluenceValue(this.func_.randomDouble(40, 80), i+1);
				}
			}
		}
	}







/**
 * フィールド上の位置を再設定する.
 * 初期設定で他のオブジェクトと位置がかぶってしまった場合を想定している.
 */
	public void resetPosition(){
	// 役割が 7 である場合とそれ以外で区別する
		if ( this.getRole() == Parameter.getRoleNumber("ゴール") ){
			this.setFPositionX(this.func_.randomInt(0, Parameter.getFieldSizeX()/5));
			this.setFPositionY(this.func_.randomInt(0, Parameter.getFieldSizeY()/5));
		}else{
			this.setFPositionX(this.func_.randomInt(0, Parameter.getFieldSizeX()/2));
			this.setFPositionY(this.func_.randomInt(0, Parameter.getFieldSizeY()/2));
		}
	}







/**
 * OID が oID のオブジェクトに対する影響値を変更する.
 *
 * @param iValue
 * @param oID
 */
	public void changeInfluenceValue(double iValue, int oID){
		double value = this.getInfluenceValue(oID) + iValue;
	// 役割がゴールである場合, 影響値の上限を定めない
		if ( this.getRole() != Parameter.getRoleNumber("ゴール") ){
		// 範囲確認
		// 値が範囲外であれば, 最大値もしくは最小値に修正
			if ( value > Parameter.getMaxInfluenceValue() ){
				value = Parameter.getMaxInfluenceValue();
			}
			if ( value < Parameter.getMinInfluenceValue() ){
				value = Parameter.getMinInfluenceValue();
			}
		}
		this.setInfluenceValue(value, oID);
	}







/**
 * 時間経過によって, 他のオブジェクトに与える影響値を更新する.
 * 役割がゴールの場合, 影響値は時間経過によって増加していく.
 * その他の役割であれば, ランダムに変化していく.
 */
	public void renewValueByTimeCourse(){
	// 他のオブジェクトに対する影響値の更新
		for ( int i = 0; i < Parameter.getNumberOfObject(); i++ ){
			if ( this.getObjectID() != (i+1)){
				if ( this.getRole() == Parameter.getRoleNumber("ゴール") ){
//					double iValue = this.func_.randomDouble(0.01, 1.0);
					double iValue = this.getInfluenceValue(i+1) * Math.pow(10.0, 0.001);
					this.setInfluenceValue(iValue, i+1);
				}else{
					double iValue = this.func_.randomGaussian(0, 1);
					this.changeInfluenceValue(iValue, i+1);
				}
			}
		}
	}






	@Override
	public int getObjectID(){
		return this.oID_;
	}


	@Override
	public int getType(){
		return this.TYPE_;
	}


	@Override
	public int getRole() {
		return this.role_;
	}


	@Override
	public String getName() {
		return this.name_;
	}


	@Override
	public int getSex() {
		return this.sex_;
	}


	@Override
	public int getAge() {
		return this.age_;
	}


	@Override
	public double[] getPersonality() {
		return this.persona_;
	}


	@Override
	public double getEmotion() {
		return this.emotion_;
	}


//	@Override
//	public int getPosture() {
//		return this.posture_;
//	}
//
//
//	@Override
//	public int getExpression() {
//		return this.express_;
//	}


	@Override
	public double getCondition() {
		return this.condition_;
	}


//	@Override
//	public int[] getPlace() {
//		return this.place_;
//	}
//
//
//	@Override
//	public int getPlaceX() {
//		return this.place_[0];
//	}
//
//
//	@Override
//	public int getPlaceY() {
//		return this.place_[1];
//	}


	@Override
	public int[] getFPosition() {
		return this.fPos_;
	}


	@Override
	public int getFPositionX() {
		return this.fPos_[0];
	}


	@Override
	public int getFPositionY() {
		return this.fPos_[1];
	}


	@Override
	public int[] getSPosition() {
		return this.sPos_;
	}


	@Override
	public int getSPositionX() {
		return this.sPos_[0];
	}


	@Override
	public int getSPositionY() {
		return this.sPos_[1];
	}


	@Override
	public double[] getFrinedshipValues() {
		return this.friendship_;
	}


	@Override
	public double getFriendshipValue(int oID) {
		return this.friendship_[oID-1];
	}


	@Override
	public double[] getFamiliarityValues() {
		return this.familiarity_;
	}


	@Override
	public double getFamiliarityValue(int oID) {
		return this.familiarity_[oID-1];
	}


	@Override
	public double[] getInfluenceValues() {
		return this.influence_;
	}


	@Override
	public double getInfluenceValue(int oID) {
		return this.influence_[oID-1];
	}







	@Override
	public void setObjectId(int oId) {
		this.oID_ = oId;
	}


	@Override
	public void setType(int type) {
		System.out.println("Object type can not be changed.");
	}


	@Override
	public void setName(String name) {
		this.name_ = name;
	}


	@Override
	public void setSex(int sex) {
		this.sex_ = sex;
	}


	@Override
	public void setAge(int age) {
		this.age_ = age;
	}


	@Override
	public void setPersonality(double[] personas) {
		for ( int i = 0; i < personas.length; i++ ){
			this.persona_[i] = personas[i];
		}
	}


	@Override
	public void setPersonality(double persona, int num) {
		this.persona_[num] = persona;
	}


	@Override
	public void setEmotion(double emotion) {
		this.emotion_ = emotion;
	}


//	@Override
//	public void setPosture(int posture) {
//		this.posture_ = posture;
//	}
//
//
//	@Override
//	public void setExpression(int express) {
//		this.express_ = express;
//	}


	@Override
	public void setCondition(double condition) {
		this.condition_ = condition;
	}


//	@Override
//	public void setPlace(int[] places) {
//		for ( int i = 0; i < places.length; i++ ){
//			this.place_[i] = places[i];
//		}
//	}
//
//
//	@Override
//	public void setPlaceX(int place) {
//		this.place_[0] = place;
//	}
//
//
//	@Override
//	public void setPlaceY(int place) {
//		this.place_[1] = place;
//	}


	@Override
	public void setFPosition(int[] positions) {
		for ( int i = 0; i < positions.length; i++ ){
			this.fPos_[i] = positions[i];
		}
	}


	@Override
	public void setFPositionX(int position) {
		this.fPos_[0] = position;
	}


	@Override
	public void setFPositionY(int position) {
		this.fPos_[1] = position;
	}


	@Override
	public void setSPosition(int[] positions) {
		for ( int i = 0; i < positions.length; i++ ){
			this.sPos_[i] = positions[i];
		}
	}


	@Override
	public void setSPositionX(int position) {
		this.sPos_[0] = position;
	}


	@Override
	public void setSPositionY(int position) {
		this.sPos_[1] = position;
	}


	@Override
	public void setFriendshipValues(double[] fValues) {
		for ( int i = 0; i < fValues.length; i++ ){
			this.friendship_[i] = fValues[i];
		}
	}


	@Override
	public void setFriendshipValue(double fValue, int oID) {
		this.friendship_[oID-1] = fValue;
	}


	@Override
	public void setFamiliarityValues(double[] fValues) {
		for ( int i = 0; i < fValues.length; i++ ){
			this.familiarity_[i] = fValues[i];
		}
	}


	@Override
	public void setFamiliarityValue(double fValue, int oID) {
		this.familiarity_[oID-1] = fValue;
	}


	@Override
	public void setInfluenceValues(double[] iValues) {
		for ( int i = 0; i < this.influence_.length; i++ ){
			this.influence_[i] = iValues[i];
		}
	}


	@Override
	public void setInfluenceValue(double iValue, int oID) {
		this.influence_[oID-1] = iValue;
	}


	@Override
	public void setRole(int role) {
		this.role_ = role;
	}

}





