package main;

public class AnalyzerMain {

	public static void main(String[] args) {
		Analyzer a = new Analyzer();

		a.analyzeEndTypeRate(1000);
		a.analyzeValueTransition(1000);
	}

}
