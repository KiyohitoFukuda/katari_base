package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import util.Log;

public class Analyzer {

	int      max_   = 500;	// 解析に用いる各エンドの最大データ数
	String[] types_ = {"SimpleEnd", "HappyEnd", "BadEnd", "NonsenseEnd"};	// エンドの種類
	int[]    tSize_ = new int[4];	// 各エンドの数
	String[] names_ = {"赤ずきん", "オオカミ", "狩人"};	// 名前の種類


	public Analyzer(){
		Log.init("Log/");
	}


	public Analyzer(String dir){
		Log.init(dir);
	}







/**
 * 生成されたストーリーの種類とそのシーン数を解析する.
 * @param seedNum
 */
	public void analyzeEndTypeRate(int seedNum){
		try{
			int[]    endType   = new int[4];
			double[] sceneNum1 = new double[4];
			double[] sceneNum2 = new double[4];
			String[] storyNum  = new String[4];
			for ( int i = 0; i < storyNum.length; i++ ){
				storyNum[i] = "_";
			}

			for ( int i = 0; i < seedNum; i++ ){
				String dirName = "Log/seed" + i + "/";
				File   dir     = new File(dirName);
				String[] list = dir.list();
				for ( int j = 0; j < list.length; j++ ){
//					System.out.println(list[j]);
					if ( list[j].contains("end")){
						String num = list[j].split("\\.")[0].split("-")[1];
						endType[0]++;
						sceneNum1[0] += Double.parseDouble(num);
						sceneNum2[0] += Double.parseDouble(num) * Double.parseDouble(num);
						storyNum[0]  += i + "_";
					}else if ( list[j].contains("happy")){
						String num = list[j].split("\\.")[0].split("-")[1];
						endType[1]++;
						sceneNum1[1] += Double.parseDouble(num);
						sceneNum2[1] += Double.parseDouble(num) * Double.parseDouble(num);
						storyNum[1]  += i + "_";
					}else if ( list[j].contains("bad")){
						String num = list[j].split("\\.")[0].split("-")[1];
						endType[2]++;
						sceneNum1[2] += Double.parseDouble(num);
						sceneNum2[2] += Double.parseDouble(num) * Double.parseDouble(num);
						storyNum[2]  += i + "_";
					}else if ( list[j].contains("nonsense")){
						String num = list[j].split("\\.")[0].split("-")[1];
						endType[3]++;
						sceneNum1[3] += Double.parseDouble(num);
						sceneNum2[3] += Double.parseDouble(num) * Double.parseDouble(num);
						storyNum[3]  += i + "_";
					}
				}
			}
//			for ( int i = 0; i < endType.length; i++ ){
//				System.out.println(endType[i] + " --- " + sceneNum1[i] + " --- " + sceneNum2[i]);
//			}
			ArrayList<String[]> data = new ArrayList<String[]>();
			for ( int i = 0; i < endType.length; i++ ){
				String[] str = new String[5];

				this.tSize_[i] = endType[i];
				double mean1    = sceneNum1[i] / endType[i];
				double mean2    = sceneNum2[i] / endType[i];
				double variance = mean2 - (mean1*mean1);
				str[0] = this.types_[i];
				str[1] = String.valueOf(endType[i]);
				str[2] = String.format("%.5f", mean1);
				str[3] = String.format("%.5f", Math.sqrt(variance));
				str[4] = storyNum[i];
				data.add(str);
				System.out.println(str[0] + " , " + str[1] + " , " + str[2] + " , " + str[3]);
				System.out.println();
			}

			String fileName = "EndTypeRate-" + seedNum;
			Log.saveAnalysis(fileName, data);

		} catch ( Exception e){
			e.printStackTrace();
		}
	}







	public void analyzeValueTransition(int seedNum){
		try{
			ArrayList[] endType1 = new ArrayList[4];
			ArrayList[] endType2 = new ArrayList[4];
			ArrayList[] reachNum = new ArrayList[4];
			ArrayList[] finish1  = new ArrayList[4];
			ArrayList[] finish2  = new ArrayList[4];
			for ( int i = 0; i < reachNum.length; i++ ){
				endType1[i] = new ArrayList<double[]>();
				endType2[i] = new ArrayList<double[]>();
				reachNum[i] = new ArrayList<Integer>();
				finish1[i]  = new ArrayList<Double>();
				finish2[i]  = new ArrayList<Double>();
			}
			int[] dataSize = new int[this.types_.length];

			int type = 0;
			for ( int i = 0; i < seedNum; i++ ){
				if ( (i%100) == 0 ){
					System.out.println(i);
				}

				String dirName = "Log/seed" + i + "/";
				File   dir     = new File(dirName);
				String[] list = dir.list();
				for ( int j = 0; j < list.length; j++ ){
					if ( list[j].contains("end")){
						type = 0;
					}else if ( list[j].contains("happy")){
						type = 1;
					}else if ( list[j].contains("bad")){
						type = 2;
					}else if ( list[j].contains("nonsense")){
						type = 3;
					}
				}
				dataSize[type]++;
				double[] fValues = new double[4*this.names_.length];
				for ( int j = 0; j < this.names_.length; j++ ){
					if ( dataSize[type] > this.max_ ){
						dataSize[type] = this.max_;
						break;
					}

					FileInputStream   fis = new FileInputStream(dirName + "Status/Status_" + this.names_[j] + ".csv");
					InputStreamReader isr = new InputStreamReader(fis, "SJIS");
					BufferedReader    br  = new BufferedReader(isr);

					int    size  = reachNum[type].size();
					int    count = 0;
					String line  = br.readLine();
					while ( (line=br.readLine()) != null ){
						String[] data   = line.split(",");
						double[] values = new double[4];
						if ( j == 0 ){
							values[0]  = Double.parseDouble(data[14]);
							values[1]  = Double.parseDouble(data[15]);
							values[2]  = Double.parseDouble(data[22]);
							values[3]  = Double.parseDouble(data[23]);
							fValues[4*j+0] = Double.parseDouble(data[14]);
							fValues[4*j+1] = Double.parseDouble(data[15]);
							fValues[4*j+2] = Double.parseDouble(data[22]);
							fValues[4*j+3] = Double.parseDouble(data[23]);
						}else if ( j == 1 ){
							values[0] = Double.parseDouble(data[13]);
							values[1] = Double.parseDouble(data[15]);
							values[2] = Double.parseDouble(data[21]);
							values[3] = Double.parseDouble(data[23]);
							fValues[4*j+0] = Double.parseDouble(data[13]);
							fValues[4*j+1] = Double.parseDouble(data[15]);
							fValues[4*j+2] = Double.parseDouble(data[21]);
							fValues[4*j+3] = Double.parseDouble(data[23]);
						}else if ( j == 2 ){
							values[0] = Double.parseDouble(data[13]);
							values[1] = Double.parseDouble(data[14]);
							values[2] = Double.parseDouble(data[21]);
							values[3] = Double.parseDouble(data[22]);
							fValues[4*j+0] = Double.parseDouble(data[13]);
							fValues[4*j+1] = Double.parseDouble(data[14]);
							fValues[4*j+2] = Double.parseDouble(data[21]);
							fValues[4*j+3] = Double.parseDouble(data[22]);
						}
						if ( size <= count ){
							double[] dataSet1 = new double[this.names_.length*values.length];
							double[] dataSet2 = new double[this.names_.length*values.length];
							for ( int k = 0; k < values.length; k++ ){
								dataSet1[4*j+k] = values[k];
								dataSet2[4*j+k] = values[k] * values[k];
							}
							endType1[type].add(dataSet1);
							endType2[type].add(dataSet2);
							reachNum[type].add(1);
						}else{
							for ( int k = 0; k < values.length; k++ ){
								((double[])endType1[type].get(count))[4*j+k] += values[k];
								((double[])endType2[type].get(count))[4*j+k] += values[k] * values[k];
							}
							int val = (int)reachNum[type].get(count) + 1;
							reachNum[type].set(count, val);
						}
						count++;
					}
					br.close();
					isr.close();
					fis.close();
				}
				if ( finish1[type].size() == 0 ){
					for ( int j = 0; j < fValues.length; j++ ){
						finish1[type].add(fValues[j]);
						finish2[type].add(fValues[j]*fValues[j]);
					}
				}else{
					for ( int j = 0; j < fValues.length; j++ ){
						double val1 = (double)finish1[type].get(j) + fValues[j];
						double val2 = (double)finish2[type].get(j) + (fValues[j]*fValues[j]);
						finish1[type].set(j, val1);
						finish2[type].set(j, val2);
					}
				}
			}
			System.out.println(seedNum + "\n");
//			for ( int i = 0; i < this.types_.length; i++ ){
//				if ( reachNum[i].size() > 0 ){
//					System.out.println(((int)reachNum[i].get(0)/3));
//				}
//			}

			ArrayList<String[]> fData  = new ArrayList<String[]>();
			for ( int i = 0; i < this.types_.length; i++ ){
				ArrayList<String[]> data = new ArrayList<String[]>();
				for ( int j = 0; j < endType1[i].size(); j++ ){
					double[] values1  = (double[])endType1[i].get(j);
					double[] values2  = (double[])endType2[i].get(j);
					double[] mean1    = new double[values1.length];
					double[] mean2    = new double[values1.length];
					double[] variance = new double[values1.length];
					int      count    = (int)reachNum[i].get(j) / this.names_.length;
					String[] str      = new String[2*values1.length+1];

					for ( int k = 0; k < values1.length; k++ ){
						mean1[k]    = values1[k] / count;
						mean2[k]    = values2[k] / count;
						variance[k] = mean2[k] - (mean1[k]*mean1[k]);
						str[2*k]    = String.format("%.5f", mean1[k]);
						str[2*k+1]  = String.format("%.5f", Math.sqrt(variance[k]));
					}
					str[2*values1.length] = String.valueOf((double)count);
					data.add(str);
				}
				ArrayList<Double>   fValues1 = (ArrayList<Double>)finish1[i];
				ArrayList<Double>   fValues2 = (ArrayList<Double>)finish2[i];
				String[] str = new String[2*fValues1.size()+1];
				str[0] = this.types_[i];
				for ( int j = 0; j < fValues1.size(); j++ ){
					double mean1    = fValues1.get(j) / dataSize[i];
					double mean2    = fValues2.get(j) / dataSize[i];
					double variance = mean2 - (mean1*mean1);
					str[2*j+1]   = String.format("%.5f", mean1);
					str[2*(j+1)] = String.format("%.5f", Math.sqrt(variance));
				}
				fData.add(str);

				String fileName = this.types_[i] + "Analysis-" + seedNum + "-" + this.max_;
				Log.saveAnalysis(fileName, data);
			}
			for ( int i = 0; i < this.types_.length-1; i++ ){
				for ( int j = i+1; j < this.types_.length; j++ ){
					String[] str1 = new String[fData.get(0).length];
//					String[] str2 = new String[fData.get(0).length];
					str1[0] = this.types_[i] + "-" + this.types_[j] + " (tValue--v)";
//					str2[0] = this.types_[i] + "-" + this.types_[j] + "-v";
					int size = str1.length / 2;
					for ( int k = 0; k < size; k++ ){
						double m = dataSize[i];
						double n = dataSize[j];
						double x = Double.valueOf(fData.get(i)[2*k+1]);
						double y = Double.valueOf(fData.get(j)[2*k+1]);
						double ux = Math.pow(Double.valueOf(fData.get(1)[2*(k+1)]), 2) * (m/(m+1));
						double uy = Math.pow(Double.valueOf(fData.get(2)[2*(k+1)]), 2) * (n/(n+1));

						double t = Math.abs(x-y) / Math.sqrt((ux/m)+(uy/n));
						double v = Math.pow(((ux/m)+(uy/n)), 2) / (((ux*ux)/(m*m*(m-1)))+((uy*uy)/(n*n*(n-1))));
						str1[2*k+1]   = String.format("%.5f", t);
						str1[2*(k+1)] = String.format("%.5f", v);
//						str2[2*k+1]   = String.format("%.5f", v);
//						str2[2*(k+1)] = "";
					}
					fData.add(str1);
//					fData.add(str2);
				}
			}

			String fileName2 = "EndTypeAnalysis-" + seedNum + "-" + this.max_;
			Log.saveAnalysis(fileName2, fData);


			double m = dataSize[1];
			double n = dataSize[2];
			double x = Double.valueOf(fData.get(1)[9]);
			double y = Double.valueOf(fData.get(2)[9]);
			double ux = Math.pow(Double.valueOf(fData.get(1)[10]), 2) * (m/(m+1));
			double uy = Math.pow(Double.valueOf(fData.get(2)[10]), 2) * (n/(n+1));

			double t = Math.abs(x-y) / Math.sqrt((ux/m)+(uy/n));
			double v = Math.pow(((ux/m)+(uy/n)), 2) / (((ux*ux)/(m*m*(m-1)))+((uy*uy)/(n*n*(n-1))));

			System.out.println(m + "," + n + "," + x + "," + y + "," + ux + "," + uy);
			System.out.println(t + "---" + v);



		} catch ( Exception e){
			e.printStackTrace();
		}
	}
}
