package main;

import java.util.ArrayList;

import story_object.Agent;
import story_object.CharacterObject;
import story_object.Environment;
import story_object.ItemObject;
import story_object.SceneObject;
import util.Function;
import util.Log;
import util.Parameter;
import action.Interaction;
import action.Move;

public class Simulator {

// 登場人物数
	private int cNum_;
// アイテム数
	private int iNum_;
// シーン数
	private int sNum_;
// 環境エージェント数
	private int eNum_;
// 種別
	private int[]      types_;
// 名前
	private String[]   names_;
// 役割
	private int[]      roles_;
// 性別
	private int[]      sexes_;
// 年齢
	private int[]      ages_;
// 性格 (登場人物のみ. 追加分)
	private double[][] personas_;

// シミュレーションマネージャー
	private SimulationManager sm_;
// 移動
	private Move        move_;
// 相互作用
	private Interaction interact_;
// 関数
	private Function    func_;



	public Simulator(int cNum, int iNum, int sNum, int eNum){
		int all = cNum + iNum + sNum;

		this.cNum_     = cNum;
		this.iNum_     = iNum;
		this.sNum_     = sNum;
		this.eNum_     = eNum;
		this.types_    = new int[all];
		this.names_    = new String[all];
		this.roles_    = new int[all];
		this.sexes_    = new int[cNum];
		this.ages_     = new int[cNum];
		this.personas_ = new double[cNum][2];
		for ( int i = 0; i < cNum; i++ ){
			this.types_[i] = 1;
		}
		for ( int i = cNum; i < cNum+iNum; i++ ){
			this.types_[i] = 2;
		}
		for ( int i = cNum+iNum; i < all; i++ ){
			this.types_[i] = 3;
		}

		this.sm_       = new SimulationManager();
		this.move_     = new Move();
		this.interact_ = new Interaction();
		this.func_     = new Function();
	}







/**
 * 各登場人物の各種設定を設定する.
 * @param names 名前
 * @param roles 役割
 * @param sexes 性別
 * @param ages 年齢
 * @param personas 性格
 */
	public void setCharacterParameter(String[] names, int[] roles, int[] sexes, int[] ages, double[][] personas){
	// 名前の設定
		if ( names.length == this.cNum_ ){
			for ( int i = 0; i < names.length; i++ ){
				this.names_[i] = names[i];
			}
		}else{
			System.out.println("Number of names is wrong! Please input " + this.cNum_ + "names.");
			System.exit(-1);
		}
	// 役割の設定
		if ( roles.length == this.cNum_ ){
			for ( int i = 0; i < roles.length; i++ ){
				this.roles_[i] = roles[i];
			}
		}else{
			System.out.println("Number of roles is wrong! Please input " + this.cNum_ + "roles.");
			System.exit(-1);
		}
	// 性別の設定
		if ( sexes.length == this.cNum_ ){
			for ( int i = 0; i < sexes.length; i++ ){
				this.sexes_[i] = sexes[i];
			}
		}else{
			System.out.println("Number of sexes is wrong! Please input " + this.cNum_ + "sexes.");
			System.exit(-1);
		}
	// 年齢の設定
		if ( ages.length == this.cNum_ ){
			for ( int i = 0; i < ages.length; i++ ){
				this.ages_[i] = ages[i];
			}
		}else{
			System.out.println("Number of ages is wrong! Please input " + this.cNum_ + "ages.");
			System.exit(-1);
		}
	// 性格の設定
		if ( personas.length == this.cNum_ ){
			for ( int i = 0; i < personas.length; i++ ){
				this.personas_[i][0] = personas[i][0];
				this.personas_[i][1] = personas[i][1];
			}
		}else{
			System.out.println("Number of personalities is wrong! Please input " + this.cNum_ + "personalities.");
			System.exit(-1);
		}
	}



/**
 * 各登場アイテムの各種設定を設定する.
 * @param names 名前
 * @param roles 役割
 */
	public void setItemParameter(String[] names, int[] roles){
	// 名前の設定
		if ( names.length == this.iNum_ ){
			for ( int i = 0; i < names.length; i++ ){
				this.names_[this.cNum_+i] = names[i];
			}
		}else{
			System.out.println("Number of names is wrong! Please input " + this.iNum_ + "names.");
			System.exit(-1);
		}
	// 役割の設定
		if ( roles.length == this.iNum_ ){
			for ( int i = 0; i < roles.length; i++ ){
				this.roles_[this.cNum_+i] = roles[i];
			}
		}else{
			System.out.println("Number of roles is wrong! Please input " + this.iNum_ + "roles.");
			System.exit(-1);
		}
	}



/**
 * 各シーンの各種設定を設定する.
 * @param names 名前
 * @param roles 役割
 */
	public void setSceneParameter(String[] names, int[] roles){
	// 名前の設定
		if ( names.length == this.sNum_ ){
			for ( int i = 0; i < names.length; i++ ){
				this.names_[this.iNum_+this.cNum_+i] = names[i];
			}
		}else{
			System.out.println("Number of names is wrong! Please input " + this.sNum_ + "names.");
			System.exit(-1);
		}
	// 役割の設定
		if ( roles.length == this.sNum_ ){
			for ( int i = 0; i < roles.length; i++ ){
				this.roles_[this.iNum_+this.cNum_+i] = roles[i];
			}
		}else{
			System.out.println("Number of roles is wrong! Please input " + this.sNum_ + "roles.");
			System.exit(-1);
		}
	}







/**
 * シミュレーションを開始するための準備をする.
 */
	public void prepare(){
	// エージェントリストの構築
		this.sm_.createAgentList(this.types_, this.names_, this.roles_, this.sexes_, this.ages_, this.personas_);
	// 環境エージェントリストの構築
		this.sm_.createEnvironmentList(this.eNum_);
	// マップの構築
		this.sm_.constructFieldMap();
	// ログ作成の準備
		Log.setManager(this.sm_);
	}







	public boolean iteration(boolean showStatus){
	// エージェントリスト
		ArrayList<Agent> aList = this.sm_.getAgentList();
	// 環境エージェントリスト
		ArrayList<Environment> eList = this.sm_.getEnvironmentList();

	// ステータス表示
		if ( showStatus ){
			Log.showObjectStatus(aList);
		}
		Log.saveObjectStatus(aList);


	// エージェント → 環境エージェントの順で行動する.
	// それぞれの内部では OID の若い順番で行動する.

	// エージェント
//		ArrayList<Integer> aOrder = this.sm_.createRandomOrder(aList.size());
//		System.out.print(aOrder.get(0));
//		for ( int i = 1; i < aOrder.size(); i++ ){
//			System.out.print(" → " + aOrder.get(i));
//		}
//		System.out.println();
//		for ( int i = 0; i < aOrder.size(); i++ ){
//			Agent agent = aList.get(aOrder.get(i));
		for ( int i = 0; i < aList.size(); i++ ){
			Agent agent = aList.get(i);

		// 移動
			this.sm_.resetFieldMap(agent);
			if ( agent.getType() == 1 ){
				this.move_.CharacterMove((CharacterObject)agent, aList);
			}
			if ( agent.getType() == 2 ){
				this.move_.ItemMove((ItemObject)agent, aList);
			}
			this.sm_.renewFieldMap(agent);

		// 条件を満たした場合に相互作用 (相互作用するのはエージェントでは登場人物のみ)
			if ( agent.getType() == 1 ){
				CharacterObject    cAgent = (CharacterObject)agent;
				ArrayList<Integer> items  = cAgent.getItemList();
			// 条件 1-1: 自分以外のエージェントがフィールド上の同じ位置に存在する.
				String pos = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";
				if ( this.sm_.checkFieldPosition(pos) ){
					ArrayList<Integer> idList = this.sm_.getIDList(pos);
					ArrayList<Integer> cList  = new ArrayList<Integer>();
					ArrayList<Integer> iList  = new ArrayList<Integer>();
					ArrayList<Integer> sList  = new ArrayList<Integer>();
					for ( int j = 0; j < idList.size(); j++ ){
						if ( idList.get(j) != agent.getObjectID() ){
						// 条件 1-2: 登場人物がいる場合は優先する.
							if ( idList.get(j) <= this.cNum_ ){
								if ( !cAgent.checkInterval(idList.get(j)) ){
									cList.add(idList.get(j));
								}
							}else if ( idList.get(j) <= (this.cNum_+this.iNum_) ){
								if ( !items.contains(idList.get(j)) ){
									iList.add(idList.get(j));
								}
							}else if ( idList.get(j) <= (this.cNum_+this.iNum_+this.sNum_) ){
								sList.add(idList.get(j));
							}
						}
					}
					if ( cList.size() > 0 ){
					// 登場人物との相互作用
						CharacterObject oAgent = (CharacterObject)aList.get(cList.get(this.func_.randomInt(0, cList.size()-1))-1);
						this.interact_.characterInteraction((CharacterObject)agent, oAgent, aList);
					}else if ( iList.size() > 0 ){
					// アイテムとの相互作用 (アイテムを拾う)
						ItemObject oAgent = (ItemObject)aList.get(iList.get(this.func_.randomInt(0, iList.size()-1))-1);
						if ( oAgent.getOwnerID() == 0 ){
							this.interact_.itemInteraction((CharacterObject)agent, (ItemObject)oAgent);
						}
					}else if ( sList.size() > 0 ){
						cAgent.resetInterval(sList.get(0), 1000, 1500);
					}
			// 条件 2: 健康状態が悪く, 食事アイテムを持っている.
				}else{
					if ( (cAgent.getCondition()<0.5) && (items.size()>0) ){
						for ( int j = 0; j < items.size(); j++ ){
							if ( !cAgent.checkInterval(items.get(j)) ){
								if ( aList.get(items.get(j)-1).getRole() == 5 ){
								// アイテムとの相互作用 (アイテムを使う)
									Agent oAgent = aList.get(items.get(j)-1);
									this.sm_.resetFieldMap(oAgent);
									this.interact_.itemUse((CharacterObject)agent, (ItemObject)oAgent);
									this.sm_.renewFieldMap(oAgent);
									break;
								}
							}
						}
					}
				}
			}
		}

	// 環境エージェント
		ArrayList<Integer> eOrder = this.sm_.createRandomOrder(eList.size());
		for ( int i = 0; i < eOrder.size(); i++ ){
			Environment env = eList.get(eOrder.get(i));

		// 移動
			this.sm_.resetFieldMap(env);
			this.move_.EnvironmentMove(env);
			this.sm_.renewFieldMap(env);

		// 登場人物がフィールド上の同じ位置に存在する場合に相互作用
			String pos = "(" + env.getFPositionX() + "," + env.getFPositionY() + ")";
			if ( this.sm_.checkFieldPosition(pos) ){
				ArrayList<Integer> idList = this.sm_.getIDList(pos);
				ArrayList<Integer> cList  = new ArrayList<Integer>();
				for ( int j = 0; j < idList.size(); j++ ){
					if ( idList.get(j) <= this.cNum_ ){
						cList.add(idList.get(j));
					}
				}
				if ( cList.size() > 0 ){
				// 登場人物との相互作用
					Agent oAgent = aList.get(cList.get(this.func_.randomInt(0, cList.size()-1))-1);
					this.interact_.environmentInteraction(env, (CharacterObject)oAgent);
				}
			}
		}

	// 時間経過による変化
		Parameter.setTurn(Parameter.getTurn()+1);
		for ( int i = 0; i < aList.size(); i++ ){
			Agent agent = aList.get(i);
			if ( agent.getType() == 1 ){
				CharacterObject cAgent = (CharacterObject)agent;
				cAgent.renewValueByTimeCourse();
			}
			if ( agent.getType() == 2 ){
				ItemObject iAgent = (ItemObject)agent;
				iAgent.renewValueByTimeCourse();
			}
			if ( agent.getType() == 3 ){
				SceneObject sAgent = (SceneObject)agent;
				sAgent.renewValueByTimeCourse();
			}
		}

	// 終了条件の確認
		boolean isFinish = false;
		for ( int i = 0; i < aList.size(); i++ ){
			Agent agent = aList.get(i);
			if ( agent.getRole() == 1 ){
				String pos = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";
				ArrayList<Integer> idList = this.sm_.getIDList(pos);
				for ( int j = 0; j < idList.size(); j++ ){
					if ( idList.get(j) <= this.cNum_+this.iNum_+this.sNum_ ){
						if ( aList.get(idList.get(j)-1).getRole() == 7 ){
							isFinish = true;
						}
					}
				}
			}
		}

		if ( isFinish ){
			if ( showStatus ){
				Log.showObjectStatus(aList);
			}
			Log.saveObjectStatus(aList);

		}

		return isFinish;
	}

}






