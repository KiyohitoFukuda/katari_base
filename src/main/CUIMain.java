package main;

import generator.PictureGenerator;
import generator.StoryGenerator;
import story_model.ModelMaker;
import util.Log;
import util.MyRandom;
import util.Parameter;
import util.Timer;

public class CUIMain {

	public static void main(String[] args){

		int cNum = 3;
		int iNum = 2;
		int sNum = 3;
		int eNum = 0;

//		int[]      types = {1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3};
		String[]   cNames = {"赤ずきん", "オオカミ", "狩人"};
		String[]   iNames = {"お肉", "銃"};
		String[]   sNames = {"2_との遭遇", "3_との遭遇", "ゴール"};
		int[]      cRoles = {1, 3, 4};
		int[]      iRoles = {5, 6};
		int[]      sRoles = {8, 8, 7};
		int[]      sexes = {2, 1, 1};
		int[]      ages  = {10, 8, 30};
		double[][] personas = {{-0.3, 0.3}, {0.3, -0.4}, {-0.1, 0.2}};


		for ( int i = 0; i < 1000; i++ ){
			Parameter.init(i, (cNum+iNum+sNum));
			MyRandom.init();
			MyRandom.getInstance(Parameter.getRandomSeed());
			Log.init("Log/");

			Timer      t   = new Timer();
			t.getStartTime();
			Simulator  sim = new Simulator(cNum, iNum, sNum, eNum);
			ModelMaker mm  = new ModelMaker(cNum, iNum, sNum);

			sim.setCharacterParameter(cNames, cRoles, sexes, ages, personas);
			sim.setItemParameter(iNames, iRoles);
			sim.setSceneParameter(sNames, sRoles);
			sim.prepare();
			t.getStopTime();
			t.showExcuteTime(true);


			int     turn     = 0;
			boolean isFinish = false;
			while ( !isFinish){
//				if ( Parameter.getTurn()%100 == 0 ){
//					System.out.println("Turn:" + Parameter.getTurn());
//					t.getStopTime();
//					t.showExcuteTime(true);
//				}
				isFinish = sim.iteration(false);
			}
			System.out.println("Seed : " + Parameter.getRandomSeed() +  "   Turn : " + Parameter.getTurn());
			t.getStopTime();
			t.showExcuteTime(true);

			String dir       = "Log/seed" + Parameter.getRandomSeed() + "/";
			String aFileName = dir + "Action/Action_all.csv";
			String sFileName = dir + "Status/Status_all.csv";
			mm.makeStoryModel(aFileName, sFileName);


			String[] names    = new String[cNum+iNum];
			int[]    roleNums = new int[cNum+iNum];
			for ( int j = 0; j < cNum; j++ ){
				names[j]    = cNames[j];
				roleNums[j] = cRoles[j];
			}
			for ( int j = 0; j < iNum; j++ ){
				names[cNum+j]    = iNames[j];
				roleNums[cNum+j] = iRoles[j];
			}

			StoryGenerator   sMaker = new StoryGenerator(names, roleNums, mm);
			PictureGenerator pMaker = new PictureGenerator(names, roleNums, mm);
			sMaker.makeStoryFromModels();
			pMaker.makePictureFromModels();


			t.getStopTime();
			t.showExcuteTime(true);

			System.out.println();
		}
	}
}
