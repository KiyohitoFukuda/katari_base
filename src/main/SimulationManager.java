package main;

import java.util.ArrayList;
import java.util.HashMap;

import story_object.Agent;
import story_object.CharacterObject;
import story_object.Environment;
import story_object.ItemObject;
import story_object.SceneObject;
import util.Function;
import util.Parameter;

public class SimulationManager {

// 使用した ID の総数
	private int ids_;
// エージェントリスト
	private ArrayList<Agent> agentList_;
// 環境エージェントリスト
	private ArrayList<Environment> environmentList_;
// シミュレーション環境マップ
	HashMap<String, ArrayList<Integer>> agentMap_;
// 地名マップ
	HashMap<String, String> positionMap_;

// 関数
	private Function func_;



/**
 * コンストラクタ
 */
	public SimulationManager(){
		this.ids_             = 0;
		this.agentList_       = new ArrayList<Agent>();
		this.environmentList_ = new ArrayList<Environment>();
		this.agentMap_        = new HashMap<String, ArrayList<Integer>>();
		this.positionMap_     = new HashMap<String, String>();
		this.func_            = new Function();

		for ( int x = 0; x < Parameter.getFieldSizeX(); x++ ){
			for ( int y = 0; y < Parameter.getFieldSizeY(); y++ ){
				String             pos  = "(" + x + "," + y + ")";
				ArrayList<Integer> list = new ArrayList<Integer>();
				this.agentMap_.put(pos, list);
				this.positionMap_.put(pos, "森の中");
			}
		}
	}







/**
 * エージェントリストを構築する.
 * @param types
 * @param names
 * @param roles
 * @param sexes
 * @param ages
 * @param personas
 */
	public void createAgentList(int[] types, String[] names, int[] roles, int[] sexes, int[] ages, double[][] personas){
		int cNum = 0;
		int iNum = 0;
		int sNum = 0;

		for ( int i = 0; i < types.length; i++ ){
		// 登場人物の場合
			if ( types[i] == 1 ){
				CharacterObject chara = new CharacterObject();
				chara.initialize((i+1), roles[i], names[i], sexes[i], ages[i]);
				chara.addPersonality(personas[i]);
				this.agentList_.add(chara);
				cNum++;
			}
		// 登場アイテムの場合
			if ( types[i] == 2 ){
				ItemObject item = new ItemObject();
				item.initialize((i+1), roles[i], names[i]);
				this.agentList_.add(item);
				iNum++;
			}
		// シーンの場合
			if ( types[i] == 3 ){
				SceneObject scene = new SceneObject();
				scene.initialize((i+1), roles[i], names[i]);
				this.agentList_.add(scene);
				sNum++;
			}
		}

		this.ids_ += (cNum+iNum+sNum);
		System.out.println("Create agent-list. (character:" + cNum + ", item:" + iNum + ", scene:" + sNum + ")");
	}







/**
 * 環境エージェントリストを構築する.
 * @param eNum
 */
	public void createEnvironmentList(int eNum){
		for ( int i = 0; i < eNum; i++ ){
			Environment env = new Environment();
			env.initialize(this.ids_+i+1);
			this.environmentList_.add(env);
		}

		System.out.println("Create environment-list. (environment-agent:" + eNum + ")");
	}







// マップ上に各エージェントの位置を記憶させる.
	public void constructFieldMap(){
	//エージェント
		for ( int i = 0; i < this.agentList_.size(); i++ ){
			Agent agent = this.agentList_.get(i);
			Integer id    = agent.getObjectID();
			String  pos   = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";
			while ( this.checkFieldPosition(pos) ){
				if ( agent.getType() == 1 ){
					CharacterObject cAgent = (CharacterObject)agent;
					cAgent.resetPosition();
					pos = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";
				}
				if ( agent.getType() == 2 ){
					ItemObject iAgent = (ItemObject)agent;
					iAgent.resetPosition();
					pos = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";
				}
				if ( agent.getType() == 3 ){
					SceneObject sAgent = (SceneObject)agent;
					sAgent.resetPosition();
					pos = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";
				}
			}
			this.agentMap_.get(pos).add(id);
			if ( agent.getType() == 1 ){
				if ( agent.getRole() == 1 ){
					this.positionMap_.put(pos, "スタート");
				}
			}
			if ( agent.getType() == 3 ){
				SceneObject sAgent = (SceneObject)agent;
				this.positionMap_.put(pos, sAgent.getName());
			}
		}
	// 環境エージェント
		for (int i = 0; i < this.environmentList_.size(); i++ ){
			Environment env = this.environmentList_.get(i);
			Integer     id  = env.getEnvironmentID();
			String pos = "(" + env.getFPositionX() + "," + env.getFPositionY() + ")";
			while ( this.checkFieldPosition(pos) ){
				env.resetPosition();
				pos = "(" + env.getFPositionX() + "," + env.getFPositionY() + ")";
			}
			this.agentMap_.get(pos).add(id);
		}

		System.out.println("Construct simulation map. (agent:" + this.agentList_.size() +
				", environment:" + this.environmentList_.size() + ")");
	}







/**
 * size までのランダムな順番を作成する.
 * @param size
 * @return
 */
	public ArrayList<Integer> createRandomOrder(int size){
		ArrayList<Integer> ans = new ArrayList<Integer>();
		ArrayList<Integer> ori = new ArrayList<Integer>();

		for ( int i = 0; i < size; i++ ){
			ori.add(i);
		}
		for ( int i = 0; i < size; i++ ){
			int num   = this.func_.randomInt(0, ori.size()-1);
			int order = ori.get(num);
			ans.add(order);
			ori.remove(num);
		}

		return ans;
	}







/**
 * 移動前にエージェント agent の位置情報をリセットする.
 * @param agent
 */
	public void resetFieldMap(Agent agent){
		String pos = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";

		this.agentMap_.get(pos).remove(Integer.valueOf(agent.getObjectID()));
	}



/**
 * 移動前に環境エージェント env の位置情報をリセットする.
 * @param env
 */
	public void resetFieldMap(Environment env){
		String pos = "(" + env.getFPositionX() + "," + env.getFPositionY() + ")";

		this.agentMap_.get(pos).remove(Integer.valueOf(env.getEnvironmentID()));
	}







/**
 * 移動後, エージェント agent の位置情報を更新する.
 * @param agent
 */
	public void renewFieldMap(Agent agent){
		String pos = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";

		this.agentMap_.get(pos).add(Integer.valueOf(agent.getObjectID()));
	}



/**
 * 移動後, 環境エージェント env の位置情報を更新する.
 * @param env
 */
	public void renewFieldMap(Environment env){
		String pos = "(" + env.getFPositionX() + "," + env.getFPositionY() + ")";

		this.agentMap_.get(pos).add(Integer.valueOf(env.getEnvironmentID()));
	}







/**
 * フィールド上の位置 pos に他のエージェントが存在しているか調べる.
 * 存在していれば true を返し, 存在していなければ false を返す.
 * @param pos
 * @return
 */
	public boolean checkFieldPosition(String pos){
		if ( this.agentMap_.get(pos).size() > 1 ){
			return true;
		}else{
			return false;
		}
	}







// ゲッター関数

/**
 * エージェントリストを返す.
 * @return
 */
	public ArrayList<Agent> getAgentList(){
		return this.agentList_;
	}


/**
 * 環境エージェントリストを返す.
 * @return
 */
	public ArrayList<Environment> getEnvironmentList(){
		return this.environmentList_;
	}


/**
 * フィールド上の位置 pos にいるオブジェクトの OID を返す.
 * @param pos
 * @return
 */
	public ArrayList<Integer> getIDList(String pos){
		return this.agentMap_.get(pos);
	}


/**
 * フィールド上の位置 pos の場所名を返す.
 * @param pos
 * @return
 */
	public String getPositionName(String pos){
		return this.positionMap_.get(pos);
	}






}






