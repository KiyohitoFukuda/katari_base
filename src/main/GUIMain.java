package main;

import gui.NarrativeRoot;
import gui.RootManager;
import gui.SettingRoot;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

public class GUIMain extends Application{


	public static void main(String[] args){
		launch(args);
	}


	public void start(Stage stage) throws Exception {
		SettingRoot sRoot = new SettingRoot();
		sRoot.createParameterBox();
		sRoot.createSettingBox(3, 2, true);
		sRoot.createButtonSet();

		NarrativeRoot nRoot = new NarrativeRoot();
		nRoot.createDefaultStoryPart();
		nRoot.createDefaultPicturePart();
		nRoot.createDefaultPagePart();
		nRoot.createDefaultMoveButton();

		RootManager.setSettingRoot(sRoot, nRoot);

		Tab[]   tabs = new Tab[2];
		for ( int i = 0; i < tabs.length; i++ ){
			tabs[i] = new Tab("Tab " + (i+1));
		}
		tabs[0].setContent(sRoot);
		tabs[1].setContent(nRoot);
		TabPane tPane = new TabPane(tabs);
		tPane.setPrefSize(1180, 700);

		Scene scene = new Scene(tPane, 1180, 700);
		stage = new Stage();
//		stage.setResizable(false);
		stage.setTitle("Simulation-based Story Generation Ver1.2");
		stage.setScene(scene);
		stage.show();
	}
}
