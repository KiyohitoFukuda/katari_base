package generator;

import java.util.ArrayList;
import java.util.HashMap;

import story_model.ModelMaker;
import util.Log;
import util.Parameter;

public class PictureGenerator {

	private String[] sParams_;	// シーンパラメータ
	private String[] oParams_;	// オブジェクトパラメータ
	private String[] rParams_;	// 関係性パラメータ
	private HashMap<String, String[]> sModels_;	// ストーリーモデル (シーン)
	private HashMap<String, String[][]> oModels_;	// ストーリーモデル (オブジェクト)
	private HashMap<String, String[][]> rModels_;	// ストーリーモデル (関係性)
	private ArrayList<String> nList_;	// 名前リスト
	private ArrayList<String> rList_;	// 役割リスト

	private ArrayList<String> pictures_;		// 生成された画像の保存先



	public PictureGenerator(String[] names, int[] roleNums, String[] sParams, String[] oParams, String[] rParams){
		this.setParameters(sParams, oParams, rParams);
		this.sModels_   = new HashMap<String, String[]>();
		this.oModels_   = new HashMap<String, String[][]>();
		this.rModels_   = new HashMap<String, String[][]>();
		this.nList_     = new ArrayList<String>();
		this.rList_     = new ArrayList<String>();

		for ( int i = 0; i < names.length; i++ ){
			this.nList_.add(names[i]);
			this.rList_.add(Parameter.getRoleName(roleNums[i]));
		}
	}

	public PictureGenerator(String[] names, int[] roleNums, ModelMaker mm){
		this.sParams_   = mm.getSceneParameter();
		this.oParams_   = mm.getObjectParameter();
		this.rParams_   = mm.getRelationParameter();
		this.sModels_   = mm.getSceneModel();
		this.oModels_   = mm.getObjectModel();
		this.rModels_   = mm.getRelationModel();
		this.nList_     = new ArrayList<String>();
		this.rList_     = new ArrayList<String>();

		for ( int i = 0; i < names.length; i++ ){
			this.nList_.add(names[i]);
			this.rList_.add(Parameter.getRoleName(roleNums[i]));
		}
	}





	public void makePictureFromModels(){
		try{
			String dir = "DATA/PIC/";

			for ( int i = 0; i < this.sModels_.size(); i++ ){
				String sKey = "S-" + (i+1);
				String oKey = "O-" + (i+1);
				String rKey = "R-" + (i+1);
				String[]   sModel = this.sModels_.get(sKey);
				String[][] oModel = this.oModels_.get(oKey);
				String[][] rModel = this.rModels_.get(rKey);

			// 場面決定
				String scene = sModel[1];
				for ( int j = 0; j < this.nList_.size(); j++ ){
					if ( scene.contains(this.nList_.get(j)) ){
						scene = scene.replace(this.nList_.get(j), this.rList_.get(j));
					}
				}

			// 絵パーツの取得
				ArrayList<String>   partsList    = new ArrayList<String>();
				ArrayList<String[]> positionList = new ArrayList<String[]>();
				String fileName;
				if ( scene.contains("スタート") ){
					fileName = "background/start.png";
				}else if ( scene.contains("遭遇") ){
					fileName = "background/in-the-forest.png";
				}else if ( scene.contains("取得") ){
					fileName = "background/in-the-forest.png";
				}else if ( scene.contains("襲撃") ){
					fileName = "background/in-the-forest.png";
				}else if ( scene.contains("到着") ){
					fileName = "background/arriving.png";
				}else if ( scene.contains("和解") ){
					fileName = "background/in-the-forest.png";
				}else if ( scene.contains("ハッピー") ){
					fileName = "background/happy-end.png";
				}else if ( scene.contains("バッド") ){
					fileName = "background/bad-end.png";
				}else if ( scene.contains("意外") ){
					fileName = "background/unexpected-end2.png";
				}else{
					fileName = "background/end.png";
				}
//				fileName += ".png";
				String[] position = {"0", "0", "10", "10", "100"};
				partsList.add(dir+fileName);
				positionList.add(position);

				for ( int j = 0; j < oModel.length; j++ ){
					String[] object = oModel[j];
				// オブジェクトの種類
					if ( object[2].equals("主人公") ){
						fileName = "hero/";
					}else if ( object[2].equals("敵") ){
						fileName = "enemy/";
					}else if ( object[2].equals("仲間") ){
						fileName = "ally/";
					}else if ( object[2].equals("食べ物") ){
						fileName = "food/";
					// 名前
						if ( object[3].contains("肉") ){
							fileName += "meat";
						}
					}else if ( object[2].equals("武器") ){
						fileName = "weapon/";
					// 名前
						if ( object[3].contains("銃") ){
							fileName += "gun";
						}
					}
				// 性格・場面
					if ( scene.contains("襲撃") ){
						if ( object[2].equals("主人公") ){
							fileName += "cry";
						}else if ( object[2].equals("敵") ){
							fileName += "attack";
						}else if ( object[2].equals("仲間") ){
							fileName += "guard";
						}
					}else if ( scene.contains("和解") ){
						if ( object[2].equals("主人公") ){
							fileName += "ok";
						}else if ( object[2].equals("敵") ){
							fileName += "sorry";
						}
					}else if ( scene.contains("スタート") ){
						if ( object[2].equals("主人公") ){
							fileName += "normal";
						}
					}else if ( object[9].equals("楽しい") ){
						fileName += "enjoyable";
					}else if ( object[9].equals("悲しい") ){
						fileName += "sad";
					}else if ( object[9].equals("怒り") ){
						fileName += "anger";
					}else if ( object[9].equals("デフォルト") ){
						fileName += "normal";
					}
					fileName += ".png";
					position = object[11].split("-");
					partsList.add(dir+fileName);
					positionList.add(position);
				}

			// 書き込み
//				for ( int j = 0; j < partsList.size()-1; j++ ){
//					System.out.println(partsList.get(j));
//				}
				Log.savePicturePath(partsList, positionList, i);


			}
		} catch ( Exception e){
			e.printStackTrace();
		}
	}





	// セッター関数

	/**
	 * 各パラメータを一度に設定する.
	 * @param sParams
	 * @param oParams
	 * @param rParams
	 */
		public void setParameters(String[] sParams, String[] oParams, String[] rParams){
			this.sParams_ = sParams;
			this.oParams_ = oParams;
			this.rParams_ = rParams;
		}



	/**
	 * シーンパラメータを設定する.
	 * @param sParams
	 */
		public void setSceneParameter(String[] sParams){
			this.sParams_ = sParams;
		}



	/**
	 * オブジェクトパラメータを設定する.
	 * @param oParams
	 */
		public void setObjectParameter(String[] oParams){
			this.oParams_ = oParams;
		}



	/**
	 * 関係性パラメータを設定する.
	 * @param rParams
	 */
		public void setRelationParameter(String[] rParams){
			this.rParams_ = rParams;
		}




	/**
	 * ストーリーモデルを設定する.
	 * @param sModels
	 * @param oModels
	 * @param rModels
	 */
		public void setStoryModels(HashMap<String, String[]> sModels, HashMap<String, String[][]> oModels, HashMap<String, String[][]> rModels){
			this.sModels_ = sModels;
			this.oModels_ = oModels;
			this.rModels_ = rModels;
		}


	/**
	 * ストーリーモデル (シーン) を設定する.
	 * @param sModels
	 */
		public void setSceneModel(HashMap<String, String[]> sModels){
			this.sModels_ = sModels;
		}



	/**
	 * ストーリーモデル (オブジェクト) を設定する.
	 * @param oModels
	 */
		public void setObjectModel(HashMap<String, String[][]> oModels){
			this.oModels_ = oModels;
		}



	/**
	 * ストーリーモデル (関係性) を設定する.
	 * @param rModels
	 */
		public void setRelationModel(HashMap<String, String[][]> rModels){
			this.rModels_ = rModels;
		}
}
