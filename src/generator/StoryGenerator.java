package generator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import story_model.ModelMaker;
import util.Log;
import util.Parameter;

public class StoryGenerator {

	private String[] sParams_;	// シーンパラメータ
	private String[] oParams_;	// オブジェクトパラメータ
	private String[] rParams_;	// 関係性パラメータ
	private HashMap<String, String[]> sModels_;	// ストーリーモデル (シーン)
	private HashMap<String, String[][]> oModels_;	// ストーリーモデル (オブジェクト)
	private HashMap<String, String[][]> rModels_;	// ストーリーモデル (関係性)
	private ArrayList<String> nList_;	// 名前リスト
	private ArrayList<String> rList_;	// 役割リスト

	private String basicSentenceFile_;	// 基本文章テンプレートの保存先
	private String additionalSentenceFile_; 	// 分岐要素文章テンプレートの保存先

	private ArrayList<String[]> sentences_;	// 生成された文章



/**
 * コンストラクタ
 * @param sParams
 * @param oParams
 * @param rParams
 */
	public StoryGenerator(String[] names, int[] roleNums, String[] sParams, String[] oParams, String[] rParams){
		this.setParameters(sParams, oParams, rParams);
		this.sModels_   = new HashMap<String, String[]>();
		this.oModels_   = new HashMap<String, String[][]>();
		this.rModels_   = new HashMap<String, String[][]>();
		this.nList_     = new ArrayList<String>();
		this.rList_     = new ArrayList<String>();
		this.sentences_ = new ArrayList<String[]>();

		this.basicSentenceFile_      = "DATA/basic.csv";
		this.additionalSentenceFile_ = "DATA/additional.csv";

		for ( int i = 0; i < names.length; i++ ){
			this.nList_.add(names[i]);
			this.rList_.add(Parameter.getRoleName(roleNums[i]));
		}
	}

/**
 * コンストラクタ
 * @param names
 * @param roleNums
 * @param mm
 */
	public StoryGenerator(String[] names, int[] roleNums, ModelMaker mm){
		this.sParams_   = mm.getSceneParameter();
		this.oParams_   = mm.getObjectParameter();
		this.rParams_   = mm.getRelationParameter();
		this.sModels_   = mm.getSceneModel();
		this.oModels_   = mm.getObjectModel();
		this.rModels_   = mm.getRelationModel();
		this.nList_     = new ArrayList<String>();
		this.rList_     = new ArrayList<String>();
		this.sentences_ = new ArrayList<String[]>();

		this.basicSentenceFile_      = "DATA/basic.csv";
		this.additionalSentenceFile_ = "DATA/additional.csv";

		for ( int i = 0; i < names.length; i++ ){
			this.nList_.add(names[i]);
			this.rList_.add(Parameter.getRoleName(roleNums[i]));
		}
	}





	public void makeStoryFromModels(){
		try{
		// 基本文章テンプレートの読み込み
			HashMap<String, ArrayList<String>> bTemps = new HashMap<String, ArrayList<String>>();
			FileInputStream   bFis = new FileInputStream(this.basicSentenceFile_);
			InputStreamReader bIsr = new InputStreamReader(bFis, "SJIS");
			BufferedReader    bBr  = new BufferedReader(bIsr);
			String bLine = bBr.readLine();
			while ( (bLine=bBr.readLine()) != null ){
				String[] data = bLine.split(",");
				ArrayList<String> temps = new ArrayList<String>();
				for ( int i = 1; i < data.length; i++ ){
					temps.add(data[i]);
				}
				bTemps.put(data[0], temps);
			}
			bBr.close();
			bIsr.close();
			bFis.close();

		// 分岐要素文章テンプレートの読み込み
			HashMap<String, ArrayList<String>> aTemps = new HashMap<String, ArrayList<String>>();
			FileInputStream   aFis = new FileInputStream(this.additionalSentenceFile_);
			InputStreamReader aIsr = new InputStreamReader(aFis, "SJIS");
			BufferedReader    aBr  = new BufferedReader(aIsr);
			String aLine = aBr.readLine();
			while ( (aLine=aBr.readLine()) != null ){
				String[] data = aLine.split(",");
				String   key  = data[0] + "-" + data[1];
				ArrayList<String> temps = new ArrayList<String>();
				for ( int i = 2; i < data.length; i++ ){
					if ( !data[i].equals("null") ){
						temps.add(data[i]);
					}else{
						temps.add("");
					}
				}
				aTemps.put(key, temps);
			}
			aBr.close();
			aIsr.close();
			aFis.close();

		// ストーリーモデルから文章の生成
			int   sCount = 0;	// 場所カウント
			int   eCount = 0;	// 感情変化
			String space = "";	// 前回の場所
			String emo   = "";	// 前回の感情
			int    wolf  = 0;	// 以前にオオカミとであったか
			for ( int i = 0; i < this.sModels_.size(); i++ ){
				String sKey = "S-" + (i+1);
				String oKey = "O-" + (i+1);
				String rKey = "R-" + (i+1);
				String[]   sModel = this.sModels_.get(sKey);
				String[][] oModel = this.oModels_.get(oKey);
				String[][] rModel = this.rModels_.get(rKey);
			// 場面決定
				String scene = sModel[1];
				for ( int j = 0; j < this.nList_.size(); j++ ){
					if ( scene.contains(this.nList_.get(j)) ){
						scene = scene.replace(this.nList_.get(j), this.rList_.get(j));
					}
				}
			// 文章の作成
				ArrayList<String> template = bTemps.get(scene);	// 基本文章テンプレートの取得
				ArrayList<String> sentence = new ArrayList<String>();
				if ( space.equals(sModel[3]) ){
					sCount++;
				}else{
					sCount = 1;
				}
				space = sModel[3];
				for ( int l = 0; l < oModel.length; l++ ){
					if ( oModel[l][2].equals("主人公")){
						if ( !emo.equals(oModel[l][9]) ){
							if ( oModel[l][9].equals("楽しい") ){
								eCount = 1;
							}else if ( oModel[l][9].equals("怒り") ){
								eCount = 2;
							}else if ( oModel[l][9].equals("悲しい") ){
								eCount = 3;
							}else{
								eCount = 4;
							}
						}else{
							eCount = 0;
						}
						emo = oModel[l][9];
					}
				}
				if ( sModel[1].equals("敵との遭遇") ){
					wolf = 1;
				}
				for ( int j = 0; j < template.size(); j++ ){
					String   s1   = "";
					String   str1 = template.get(j);
				// 分岐要素の埋め込み
					String[] ele1 = str1.split(":");
					for ( int k = 0; k < ele1.length; k++ ){
//						System.out.println("1 " + ele1[k]);
						if ( k%2 == 0 ){
							s1 += ele1[k];
						}else{
							String            key       = "";
							ArrayList<String> additional = new ArrayList<String>();
							switch ( ele1[k] ){
							case "場所カウント":
								key        = "場所カウント-" + sCount;
								additional = aTemps.get(key);
								for ( int l = 0; l < additional.size(); l++ ){
									s1 += additional.get(l);
								}
								break;
							case "感情変化":
								key        = "感情変化-" + eCount;
								additional = aTemps.get(key);
								for ( int l = 0; l < additional.size(); l++ ){
									s1 += additional.get(l);
								}
								break;
							case "敵邂逅":
								key        = "敵邂逅-" + wolf;
								additional = aTemps.get(key);
								for ( int l = 0; l < additional.size(); l++ ){
									s1 += additional.get(l);
								}
								break;
							case "敵会話":
								for ( int l = 0; l < rModel.length; l++ ){
									if ( rModel[l][0].equals("1_2")){
										if ( rModel[l][4].equals("仲が良くなる") ){
											key = "敵会話-" + 1;
										}else if ( rModel[l][4].equals("仲が悪くなる") ){
											key = "敵会話-" + 2;
										}else{
											key = "敵会話-" + 0;
										}
									}
								}
								additional = aTemps.get(key);
								for ( int l = 0; l < additional.size(); l++ ){
									s1 += additional.get(l);
								}
								break;
							case "仲間会話":
								for ( int l = 0; l < rModel.length; l++ ){
									if ( rModel[l][0].equals("1_3")){
										if ( rModel[l][4].equals("仲が良くなる") ){
											key = "仲間会話-" + 1;
										}else if ( rModel[l][4].equals("仲が悪くなる") ){
											key = "仲間会話-" + 2;
										}else{
											key = "仲間会話-" + 0;
										}
									}
								}
								additional = aTemps.get(key);
								for ( int l = 0; l < additional.size(); l++ ){
									s1 += additional.get(l);
								}
								break;
							case "食べ物譲渡":
								for ( int l = 0; l < rModel.length; l++ ){
									if ( rModel[l][0].equals("1_2")){
										if ( rModel[l][1].equals("食べ物を渡す") ){
											key = "食べ物譲渡-" + 1;
										}else{
											key = "食べ物譲渡-" + 0;
										}
									}
								}
								additional = aTemps.get(key);
								for ( int l = 0; l < additional.size(); l++ ){
									s1 += additional.get(l);
								}
								break;
							case "武器譲渡":
								for ( int l = 0; l < rModel.length; l++ ){
									if ( rModel[l][0].equals("1_3")){
										if ( rModel[l][1].equals("武器を渡す") ){
											key = "武器譲渡-" + 1;
										}else{
											key = "武器譲渡-" + 0;
										}
									}
								}
								additional = aTemps.get(key);
								for ( int l = 0; l < additional.size(); l++ ){
									s1 += additional.get(l);
								}
								break;
							case "襲撃":
								for ( int l = 0; l < rModel.length; l++ ){
									if ( rModel[l][0].equals("2_1")){
										if ( rModel[l][4].equals("襲撃成功") ){
											key = "襲撃-" + 1;
										}else if ( rModel[l][4].equals("襲撃失敗") ){
											key = "襲撃-" + 0;
										}
									}
								}
								additional = aTemps.get(key);
								for ( int l = 0; l < additional.size(); l++ ){
									s1 += additional.get(l);
								}
								break;
							default:
								System.out.println("ここに来ることはないはずなので, エラーです");
								System.exit(-1);
							}
						}
					}

				// 文章テンプレートの埋め込み
					String   s2   = "";
					String[] ele2 = s1.split("_");
					for ( int k = 0; k < ele2.length; k++){
						if ( k%2 == 0 ){
							s2 += ele2[k];
						}else{
							switch ( ele2[k] ){
							case "主人公":
								for ( int l = 0; l < this.rList_.size(); l++ ){
									if ( this.rList_.get(l).equals("主人公") ){
										s2 += this.nList_.get(l);
									}
								}
								break;
							case "敵":
								for ( int l = 0; l < this.rList_.size(); l++ ){
									if ( this.rList_.get(l).equals("敵") ){
										s2 += this.nList_.get(l);
									}
								}
								break;
							case "仲間":
								for ( int l = 0; l < this.rList_.size(); l++ ){
									if ( this.rList_.get(l).equals("仲間") ){
										s2 += this.nList_.get(l);
									}
								}
								break;
							case "食べ物":
								for ( int l = 0; l < this.rList_.size(); l++ ){
									if ( this.rList_.get(l).equals("食べ物") ){
										s2 += this.nList_.get(l);
									}
								}
								break;
							case "武器":
								for ( int l = 0; l < this.rList_.size(); l++ ){
									if ( this.rList_.get(l).equals("武器") ){
										s2 += this.nList_.get(l);
									}
								}
								break;
							case "主人公性格":
								String[] seikaku = oModel[0][6].split("-");
								if ( seikaku[0].equals(" ") ){
									s2 += seikaku[1];
								}else if ( seikaku[1].equals(" ") ){
									s2 += seikaku[0];
								}else{
									s2 += seikaku[0] + "で" + seikaku[1];
								}
								break;
							case "場所":
								s2 += sModel[3];
								break;
							case "敵感情":
								for ( int l = 0; l < oModel.length; l++ ){
									if ( oModel[l][2].equals("敵")){
										if ( oModel[l][9].equals("楽しい") ){
											s2 += "ワクワク";
										}else if ( oModel[l][9].equals("悲しい") ){
											s2 += "しょんぼり";
										}else if ( oModel[l][9].equals("怒り") ){
											s2 += "イライラ";
										}else{
											s2 += "のんびり";
										}
									}
								}
								break;
							case "仲間感情":
								for ( int l = 0; l < oModel.length; l++ ){
									if ( oModel[l][2].equals("仲間")){
										if ( oModel[l][9].equals("楽しい") ){
											s2 += "ワクワク";
										}else if ( oModel[l][9].equals("悲しい") ){
											s2 += "しょんぼり";
										}else if ( oModel[l][9].equals("怒り") ){
											s2 += "イライラ";
										}else{
											s2 += "のんびり";
										}
									}
								}
								break;
							case "主人公年齢性別":
								if ( oModel[0][5].equals("女性") ){
									if ( Integer.valueOf(oModel[0][4]) >= 18 ){
										s2 += "女性";
									}else{
										s2 += "女の子";
									}
								}else if ( oModel[0][5].equals("男性") ){
									if ( Integer.valueOf(oModel[0][4]) >= 18 ){
										s2 += "男性";
									}else{
										s2 += "男の子";
									}
								}
								break;
							default:
								System.out.println("ここに来ることはないはずなので, エラーです");
								System.exit(-1);
							}
						}
					}
					sentence.add(s2);
				}
				String[] ans = (String[])sentence.toArray(new String[0]);
				this.sentences_.add(ans);
			}

//			for ( int i = 0; i < this.sentences_.size(); i++ ){
//				String[] sentences = this.sentences_.get(i);
//				for ( int j = 0; j < sentences.length; j++ ){
//					System.out.println(sentences[j]);
//				}
//				System.out.println();
//			}

			Log.saveStory(this.sentences_);



		} catch ( Exception e ){
			e.printStackTrace();
		}
	}







// ゲッター関数

/**
 * 生成した文章を返す.
 * @return
 */
	public ArrayList<String[]> getStory(){
		return this.sentences_;
	}







// セッター関数

/**
 * 各パラメータを一度に設定する.
 * @param sParams
 * @param oParams
 * @param rParams
 */
	public void setParameters(String[] sParams, String[] oParams, String[] rParams){
		this.sParams_ = sParams;
		this.oParams_ = oParams;
		this.rParams_ = rParams;
	}



/**
 * シーンパラメータを設定する.
 * @param sParams
 */
	public void setSceneParameter(String[] sParams){
		this.sParams_ = sParams;
	}



/**
 * オブジェクトパラメータを設定する.
 * @param oParams
 */
	public void setObjectParameter(String[] oParams){
		this.oParams_ = oParams;
	}



/**
 * 関係性パラメータを設定する.
 * @param rParams
 */
	public void setRelationParameter(String[] rParams){
		this.rParams_ = rParams;
	}




/**
 * ストーリーモデルを設定する.
 * @param sModels
 * @param oModels
 * @param rModels
 */
	public void setStoryModels(HashMap<String, String[]> sModels, HashMap<String, String[][]> oModels, HashMap<String, String[][]> rModels){
		this.sModels_ = sModels;
		this.oModels_ = oModels;
		this.rModels_ = rModels;
	}


/**
 * ストーリーモデル (シーン) を設定する.
 * @param sModels
 */
	public void setSceneModel(HashMap<String, String[]> sModels){
		this.sModels_ = sModels;
	}



/**
 * ストーリーモデル (オブジェクト) を設定する.
 * @param oModels
 */
	public void setObjectModel(HashMap<String, String[][]> oModels){
		this.oModels_ = oModels;
	}



/**
 * ストーリーモデル (関係性) を設定する.
 * @param rModels
 */
	public void setRelationModel(HashMap<String, String[][]> rModels){
		this.rModels_ = rModels;
	}
}









