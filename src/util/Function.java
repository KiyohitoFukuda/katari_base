package util;

import java.util.Random;

public class Function {

// 乱数
	private Random random_;



/**
 * コンストラクタ
 */
	public Function(){
		this.random_ = MyRandom.getInstance();
	}







/**
 * 整数乱数を返す.
 * @return
 */
	public int randomInt(){
		return this.random_.nextInt();
	}



/**
 * min 以上 max 以下の整数乱数を返す.
 * @param min 下限
 * @param max 上限
 * @return
 */
	public int randomInt(int min, int max){
		int ans = this.random_.nextInt(max-min+1) + min;
		return ans;
	}







/**
 * [0,1) の範囲の実数乱数を返す.
 * @return
 */
	public double randomDouble(){
		return this.random_.nextDouble();
	}



/**
 * min 以上 max 未満の実数乱数を返す.
 * @param min 下限
 * @param max 上限
 * @return
 */
	public double randomDouble(double min, double max){
		double ans = (max-min) * this.random_.nextDouble() + min;
		return ans;
	}







/**
 * 平均が mean, 標準偏差が sDev の正規分布に従う実数乱数を返す.
 * @param mean 平均
 * @param sDev 標準偏差
 * @return
 */
	public double randomGaussian(double mean, double sDev){
		double ans = sDev * this.random_.nextGaussian() + mean;
		return ans;
	}


}
