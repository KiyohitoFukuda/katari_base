package util;

public class Timer {

	private static long start_ = 0;
	private static long stop_ = 0;

	//開始時刻を記録
	public void getStartTime(){
		start_ = System.currentTimeMillis();
	}





	//終了時刻を記録
	public void getStopTime(){
		stop_ = System.currentTimeMillis();
	}





	//実行時間を○○時間○○分○○秒で表示
	public void showExcuteTime(boolean isDetailed){
		long time  = stop_ - start_;
		int second = (int)time / 1000;
		int minute = 0;
		int hour   = 0;

		double dSecond = (double)time / 1000.0;

		minute = second / 60;
		second = second % 60;

		hour   = minute / 60;
		minute = minute % 60;

//		System.out.println(time);
		if ( isDetailed ){
			System.out.println("実行時間 : " + String.format("%.2f", dSecond) + " 秒");
		}else{
			System.out.println("実行時間 : " + hour + " 時間 " + minute + " 分 " + second + " 秒");
		}
	}
}
