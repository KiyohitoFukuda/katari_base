package util;

public class Parameter {

// 乱数シード
	private static long     seed_   = 0;
// オブジェクト数
	private static int      objNum_ = 0;
// フィールドサイズ
	private static int[]    fSize_  = {100, 100};
// シーンサイズ
	private static int[]    sSize_  = {7, 7};
// 性格の値の範囲
	private static double[] pRange_ = {0.0, 1.0};
// 感情の範囲
	private static double[] eRange_ = {-1.0, 1.0};
// 健康状態の値の範囲
	private static double[] cRange_ = {0.0, 1.0};
// 友好度の範囲
	private static double[] frRange_ = {-100.0, 100.0};
// 認知度の範囲
	private static double[] faRange_ = {-100.0, 100.0};
// 影響値の範囲
	private static double[] iRange_  = {0, 100};
// 行動番号
	private static int      actNum_  = 0;
// ターン数
	private static int      turn_    = 0;



// 初期化
	public static void init(long seed, int objNum){
		seed_     = seed;
		objNum_   = objNum;
		fSize_[0] = 100;
		fSize_[1] = 100;
		sSize_[0] = 7;
		sSize_[1] = 7;
		actNum_   = 0;
		turn_     = 0;
	}



// オブジェクトの種別名
	private static String[] objType_    = {"未設定", "登場人物", "アイテム", "場所"};
// オブジェクトの役割名
	private static String[] objRole_    = {"未設定", "主人公", "ヒロイン", "敵", "仲間", "食べ物", "武器", "ゴール", "イベント"};
// オブジェクトの性別名
	private static String[] objSex_     = {"未設定", "男性", "女性"};
// オブジェクトの姿勢名
	private static String[] objPosture_ = {"未設定", "座る", "横たわる"};
// オブジェクトの表情名
	private static String[] objLook_ = {"未設定", "楽しい・嬉しい", "悲しい", "怒り", "普通"};



// ゲッター関数

/**
 * 乱数シードを返す.
 * @return
 */
	public static long getRandomSeed(){
		return seed_;
	}





/**
 * オブジェクトの数を返す.
 * @return
 */
	public static int getNumberOfObject(){
		return objNum_;
	}





/**
 * フィールドサイズを返す.
 * @return
 */
	public static int[] getFieldSize(){
		int[] ans = new int[2];
		ans[0] = fSize_[0];
		ans[1] = fSize_[1];
		return ans;
	}


/**
 * フィールドの x 軸のサイズを返す.
 * @return
 */
	public static int getFieldSizeX(){
		return fSize_[0];
	}


/**
 * フィールドの y 軸のサイズを返す.
 * @return
 */
	public static int getFieldSizeY(){
		return fSize_[1];
	}





/**
 * シーンサイズを返す.
 * @return
 */
	public static int[] getSceneSize(){
		int[] ans = new int[2];
		ans[0] = sSize_[0];
		ans[1] = sSize_[1];
		return ans;
	}


/**
 * シーンの x 軸のサイズを返す.
 * @return
 */
	public static int getSceneSizeX(){
		return sSize_[0];
	}


/**
 * シーンの y 軸のサイズを返す.
 * @return
 */
	public static int getSceneSizeY(){
		return sSize_[1];
	}





/**
 * 性格の値の下限を返す.
 * @return
 */
	public static double getMinPersonality(){
		return pRange_[0];
	}


/**
 * 性格の値の上限を返す.
 * @return
 */
	public static double getMaxPersonality(){
		return pRange_[1];
	}





/**
 * 感情の値の下限を返す.
 * @return
 */
	public static double getMinEmotion(){
		return eRange_[0];
	}


/**
 * 感情の値の上限を返す.
 * @return
 */
	public static double getMaxEmotion(){
		return eRange_[1];
	}





/**
 * 健康状態の値の下限を返す.
 * @return
 */
	public static double getMinCondition(){
		return cRange_[0];
	}


/**
 * 健康状態の値の上限を返す.
 * @return
 */
	public static double getMaxCondition(){
		return cRange_[1];
	}





/**
 * 友好度の下限を返す.
 * @return
 */
	public static double getMinFriendshipValue(){
		return frRange_[0];
	}


/**
 * 友好度の上限を返す.
 * @return
 */
	public static double getMaxFriendshipValue(){
		return frRange_[1];
	}





/**
 * 認知度の下限を返す.
 * @return
 */
	public static double getMinFamiliarityValue(){
		return faRange_[0];
	}


/**
 * 認知度の上限を返す.
 * @return
 */
	public static double getMaxFamiliarityValue(){
		return faRange_[1];
	}





/**
 * 影響値の下限を返す.
 * @return
 */
	public static double getMinInfluenceValue(){
		return iRange_[0];
	}


/**
 * 認知度の上限を返す.
 * @return
 */
	public static double getMaxInfluenceValue(){
		return iRange_[1];
	}





/**
 * 行動番号を返す.
 * @return
 */
	public static int getActionNumber(){
		return actNum_;
	}





/**
 * 現在のターン数を返す.
 * @return
 */
	public static int getTurn(){
		return turn_;
	}





	public static String getTypeName(int type){
		return objType_[type];
	}

	public static int getTypeNumber(String type){
		for ( int i = 0; i < objType_.length; i++ ){
			if ( objType_[i].equals(type) ){
				return i;
			}
		}
		return -1;
	}





	public static String getRoleName(int role){
		return objRole_[role];
	}

	public static int getRoleNumber(String role){
		for ( int i = 0; i < objRole_.length; i++ ){
			if ( objRole_[i].equals(role) ){
				return i;
			}
		}
		return -1;
	}





	public static String getSexName(int sex){
		return objSex_[sex];
	}

	public static int getSexNumber(String sex){
		for ( int i = 0; i < objSex_.length; i++ ){
			if ( objSex_[i].equals(sex) ){
				return i;
			}
		}
		return -1;
	}





	public static String getLookName(int look){
		return objLook_[look];
	}

	public static int getLookNumber(String look){
		for ( int i = 0; i < objLook_.length; i++ ){
			if ( objLook_[i].equals(look) ){
				return i;
			}
		}
		return -1;
	}










// セッター関数

/**
 * 行動番号を設定する.
 * @param actNum
 */
	public static void setActionNumber(int actNum){
		actNum_ = actNum;
	}





/**
 * 現在のターン数を設定する.
 * @param turn
 */
	public static void setTurn(int turn){
		turn_ = turn;
	}

}







