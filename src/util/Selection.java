package util;

import java.util.Random;

public class Selection {

// 乱数
	private Random r_;


// コンストラクタ
	public Selection(){
		r_ = MyRandom.getInstance();
	}







/**
 * ルーレット選択
 * @param fitness
 * @return
 */
	public int rouletteSelection(double[] fitness){
		double all    = 0.0;
		double needle = r_.nextDouble();
		for ( int i = 0; i < fitness.length; i++){
			all += fitness[i];
		}
		needle *= all;

		if ( needle <= 0.0 ){
			int rand = r_.nextInt(fitness.length);
			return rand;
		}
		for ( int i = 0; i < fitness.length; i++){
			needle -= fitness[i];
			if ( needle <= 0){
				return i;
			}
		}

		return -1;
	}







/**
 * トーナメント選択
 * @param fitness
 * @param size トーナメントサイズ
 * @param isRestored 復元抽出をするかどうか
 * @return
 */
	public int tournamentSelection(double[] fitness, int size, boolean isRestored){
	// 候補選択
		int[] candidate = new int[size];
		for ( int i = 0; i < size; i++ ){
			int     num       = r_.nextInt(fitness.length);
			boolean isCovered = false;
			if ( !isRestored ){
				for ( int j = 0; j < i; j++ ){
					if ( candidate[j] == num ){
						isCovered = true;
						break;
					}
				}
				if ( isCovered ){
					i--;
				}
			}
			candidate[i] = num;
		}

		int    maxNumber  = -1;
		double maxFitness = 0.0;
		for ( int i = 0; i < size; i++ ){
			if ( maxFitness < fitness[candidate[i]] ){
				maxNumber  = i;
				maxFitness = fitness[candidate[i]];
			}
		}

		return maxNumber;
	}

}







