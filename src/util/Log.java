package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;

import main.SimulationManager;
import story_object.Agent;
import story_object.CharacterObject;

public class Log {

// ログファイルの保存ディレクトリ
	private static String    logDir_;
// 保存ファイル
	private static String[]  fileNames_;
// アクションログの初期保存フラグ
	private static boolean[] isFirstAction_;
// ステータスログの初期保存フラグ
	private static boolean[] isFirstStatus_;
// シミュレーションマネージャ
	private static SimulationManager sm_;


// ステータスパラメータ
	private static String[] staParams_ = {"ターン数", "OID", "種別", "名前", "役割", "性別", "年齢", "性格[0]", "性格[1]",
		"感情", "健康状態", "位置", "場所", "友好度-", "認知度-", "影響値-", "アイテムリスト"};
// アクションパラメータ
	private static String[] actParams_ = {"ターン数", "行動", "行動番号", "主体 OID", "客体 OID", "位置", "場所",
		"位置変化量(主体)", "友好度変化量(主体)", "認知度変化量(主体)", "感情変化量(主体)", "アイテム変化量(主体)",
		"位置変化量(客体)", "友好度変化量(客体)", "認知度変化量(客体)", "感情変化量(客体)", "アイテム変化量(客体)"};



	public static void init(String logDir){
		logDir_        = logDir;
		fileNames_     = new String[Parameter.getNumberOfObject()+1];
		isFirstAction_ = new boolean[Parameter.getNumberOfObject()+1];
		isFirstStatus_ = new boolean[Parameter.getNumberOfObject()+1];

		for ( int i = 0; i < Parameter.getNumberOfObject()+1; i++ ){
			isFirstAction_[i] = true;
			isFirstStatus_[i] = true;
		}

	}







	public static void setManager(SimulationManager sm){
		sm_ = sm;
	}







	public static String makeDirectory(String dirName,  String logType){
		try{
			String directory = logDir_ + dirName + "/" + logType;
			File   file      = new File(directory);
			file.mkdirs();

			return directory + "/";

		} catch ( Exception e){
			e.printStackTrace();
			return null;
		}
	}







	public static void showObjectStatus(ArrayList<Agent> aList){
		for ( int i = 0; i < aList.size(); i++ ){
			Agent  agent = aList.get(i);
			String pos   = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";
			System.out.print(" " + staParams_[0] + ":" + Parameter.getTurn());
			System.out.print(" " + staParams_[1] + ":" + agent.getObjectID());
			System.out.print(" " + staParams_[2] + ":" + agent.getType());
			System.out.print(" " + staParams_[3] + ":" + agent.getName());
			System.out.print(" " + staParams_[4] + ":" + agent.getRole());
			System.out.print(" " + staParams_[5] + ":" + agent.getSex());
			System.out.print(" " + staParams_[6] + ":" + agent.getAge());
			System.out.print(" " + staParams_[7] + ":" + String.format("%.3f", agent.getPersonality()[0]));
			System.out.print(" " + staParams_[8] + ":" + String.format("%.3f", agent.getPersonality()[1]));
			System.out.print(" " + staParams_[9] + ":" + String.format("%.3f", agent.getEmotion()));
			System.out.print(" " + staParams_[10] + ":" + String.format("%.3f", agent.getCondition()));
			System.out.print(" " + staParams_[11] + ":" + pos);
			System.out.print(" " + staParams_[12] + ":" + sm_.getPositionName(pos));
			for ( int j = 1; j < Parameter.getNumberOfObject()+1; j++ ){
				System.out.print(" " + staParams_[13] + j + ":" + String.format("%.3f", agent.getFriendshipValue(j)));
			}
			for ( int j = 1; j < Parameter.getNumberOfObject()+1; j++ ){
				System.out.print(" " + staParams_[14] + j + ":" + String.format("%.3f", agent.getFamiliarityValue(j)));
			}
			for ( int j = 1; j < Parameter.getNumberOfObject()+1; j++ ){
				System.out.print(" " + staParams_[15] + j + ":" + String.format("%.3f", agent.getInfluenceValue(j)));
			}

			System.out.print(" " + staParams_[16] + ":[");
			if ( agent.getType() == 1 ){
				CharacterObject    cAgent = (CharacterObject)agent;
				ArrayList<Integer> items  = cAgent.getItemList();
				if ( items.size() > 0 ){
					for ( int j = 0; j < items.size()-1; j++ ){
						System.out.print(items.get(j) + ",");
					}
					System.out.print(items.get(items.size()-1) + ",");
				}
			}
			System.out.println("]");
		}
		System.out.println();
	}







	public static void saveObjectStatus(ArrayList<Agent> aList){
		try{
			String seed = "seed" + Parameter.getRandomSeed();
			String dir  = makeDirectory(seed, "Status");

			String fileName0 = dir + "Status_all.csv";
			FileOutputStream   fos0 = new FileOutputStream(fileName0, true);
			OutputStreamWriter osw0 = new OutputStreamWriter(fos0, "SJIS");
			BufferedWriter     bw0  = new BufferedWriter(osw0);
		// パラメータ種別
			if ( isFirstStatus_[0] ){
				bw0.write(staParams_[0] + "," + staParams_[1] + "," + staParams_[2] + ",");
				bw0.write(staParams_[3] + "," + staParams_[4] + "," + staParams_[5] + ",");
				bw0.write(staParams_[6] + "," + staParams_[7] + "," + staParams_[8] + ",");
				bw0.write(staParams_[9] + "," + staParams_[10] + "," + staParams_[11] + ",");
				bw0.write(staParams_[12] + ",");
				for ( int i = 1; i < aList.size()+1; i++ ){
					bw0.write(staParams_[13] + i + ",");
				}
				for ( int i = 1; i < aList.size()+1; i++ ){
					bw0.write(staParams_[14] + i + ",");
				}
				for ( int i = 1; i < aList.size()+1; i++ ){
					bw0.write(staParams_[15] + i + ",");
				}
				bw0.write(staParams_[16]);
				bw0.newLine();
				isFirstStatus_[0] = false;
			}

			for ( int i = 0; i < aList.size(); i++ ){
				Agent  agent = aList.get(i);
				String pos   = "(" + agent.getFPositionX() + "," + agent.getFPositionY() + ")";
				String pos1  = agent.getFPositionX() + "_" + agent.getFPositionY();

				String fileName1 = dir + "Status_" + agent.getName() + ".csv";
				FileOutputStream   fos1 = new FileOutputStream(fileName1, true);
				OutputStreamWriter osw1 = new OutputStreamWriter(fos1, "SJIS");
				BufferedWriter     bw1  = new BufferedWriter(osw1);
			// パラメータ種別
				if ( isFirstStatus_[i+1] ){
					bw1.write(staParams_[0] + "," + staParams_[1] + "," + staParams_[2] + ",");
					bw1.write(staParams_[3] + "," + staParams_[4] + "," + staParams_[5] + ",");
					bw1.write(staParams_[6] + "," + staParams_[7] + "," + staParams_[8] + ",");
					bw1.write(staParams_[9] + "," + staParams_[10] + "," + staParams_[11] + ",");
					bw1.write(staParams_[12] + ",");
					for ( int j = 1; j < aList.size()+1; j++ ){
						bw1.write(staParams_[13] + j + ",");
					}
					for ( int j = 1; j < aList.size()+1; j++ ){
						bw1.write(staParams_[14] + j + ",");
					}
					for ( int j = 1; j < aList.size()+1; j++ ){
						bw1.write(staParams_[15] + j + ",");
					}
					bw1.write(staParams_[16]);
					bw1.newLine();
					isFirstStatus_[i+1] = false;
				}

			// パラメータ
				bw0.write(Parameter.getTurn() + ",");
				bw0.write(agent.getObjectID() + ",");
				bw0.write(agent.getType() + ",");
				bw0.write(agent.getName() + ",");
				bw0.write(agent.getRole() + ",");
				bw0.write(agent.getSex() + ",");
				bw0.write(agent.getAge() + ",");
				bw0.write(String.format("%.3f", agent.getPersonality()[0]) + ",");
				bw0.write(String.format("%.3f", agent.getPersonality()[1]) + ",");
				bw0.write(String.format("%.3f", agent.getEmotion()) + ",");
				bw0.write(String.format("%.3f", agent.getCondition()) + ",");
				bw0.write(pos1 + ",");
				bw0.write(sm_.getPositionName(pos) + ",");
				for ( int j = 1; j < aList.size()+1; j++ ){
					bw0.write(String.format("%.3f", agent.getFriendshipValue(j)) + ",");
				}
				for ( int j = 1; j < aList.size()+1; j++ ){
					bw0.write(String.format("%.3f", agent.getFamiliarityValue(j)) + ",");
				}
				for ( int j = 1; j < aList.size()+1; j++ ){
					bw0.write(String.format("%.3f", agent.getInfluenceValue(j)) + ",");
				}
				bw0.write("[");
				if ( agent.getType() == 1 ){
					CharacterObject    cAgent = (CharacterObject)agent;
					ArrayList<Integer> items  = cAgent.getItemList();
					if ( items.size() > 0 ){
						for ( int j = 0; j < items.size()-1; j++ ){
							bw0.write(items.get(j) + "_");
						}
						bw0.write(String.valueOf(items.get(items.size()-1)));
					}
				}
				bw0.write("]");
				bw0.newLine();

				bw1.write(Parameter.getTurn() + ",");
				bw1.write(agent.getObjectID() + ",");
				bw1.write(agent.getType() + ",");
				bw1.write(agent.getName() + ",");
				bw1.write(agent.getRole() + ",");
				bw1.write(agent.getSex() + ",");
				bw1.write(agent.getAge() + ",");
				bw1.write(String.format("%.3f", agent.getPersonality()[0]) + ",");
				bw1.write(String.format("%.3f", agent.getPersonality()[1]) + ",");
				bw1.write(String.format("%.3f", agent.getEmotion()) + ",");
				bw1.write(String.format("%.3f", agent.getCondition()) + ",");
				bw1.write(pos1 + ",");
				bw1.write(sm_.getPositionName(pos) + ",");
				for ( int j = 1; j < aList.size()+1; j++ ){
					bw1.write(String.format("%.3f", agent.getFriendshipValue(j)) + ",");
				}
				for ( int j = 1; j < aList.size()+1; j++ ){
					bw1.write(String.format("%.3f", agent.getFamiliarityValue(j)) + ",");
				}
				for ( int j = 1; j < aList.size()+1; j++ ){
					bw1.write(String.format("%.3f", agent.getInfluenceValue(j)) + ",");
				}
				bw1.write("[");
				if ( agent.getType() == 1 ){
					CharacterObject    cAgent = (CharacterObject)agent;
					ArrayList<Integer> items  = cAgent.getItemList();
					if ( items.size() > 0 ){
						for ( int j = 0; j < items.size()-1; j++ ){
							bw1.write(items.get(j) + "_");
						}
						bw1.write(String.valueOf(items.get(items.size()-1)));
					}
				}
				bw1.write("]");
				bw1.newLine();

				bw1.close();
				osw1.close();
				fos1.close();
			}
			bw0.close();
			osw0.close();
			fos0.close();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}







	public static void saveObjectAction(Agent agent1, int oID, int actType, int actNum, String[] change){
		try{
			String seed = "seed" + Parameter.getRandomSeed();
			String dir  = makeDirectory(seed, "Action");

			String fileName0 = dir + "Action_all.csv";
			FileOutputStream   fos0 = new FileOutputStream(fileName0, true);
			OutputStreamWriter osw0 = new OutputStreamWriter(fos0, "SJIS");
			BufferedWriter     bw0  = new BufferedWriter(osw0);
			String fileName1 = dir + "Action_" + agent1.getName() + ".csv";
			FileOutputStream   fos1 = new FileOutputStream(fileName1, true);
			OutputStreamWriter osw1 = new OutputStreamWriter(fos1, "SJIS");
			BufferedWriter     bw1  = new BufferedWriter(osw1);
		// パラメータ種別
			if ( isFirstAction_[0] ){
				bw0.write(actParams_[0] + "," + actParams_[1] + "," + actParams_[2] + ",");
				bw0.write(actParams_[3] + "," + actParams_[4] + "," + actParams_[5] + ",");
				bw0.write(actParams_[6] + "," + actParams_[7] + "," + actParams_[8] + ",");
				bw0.write(actParams_[9] + "," + actParams_[10] + "," + actParams_[11] + ",");
				bw0.write(actParams_[12] + "," + actParams_[13] + "," + actParams_[14] + ",");
				bw0.write(actParams_[15] + "," + actParams_[16]);
				bw0.newLine();
				isFirstAction_[0] = false;
			}
			if ( isFirstAction_[agent1.getObjectID()] ){
				bw1.write(actParams_[0] + "," + actParams_[1] + "," + actParams_[2] + ",");
				bw1.write(actParams_[3] + "," + actParams_[4] + "," + actParams_[5] + ",");
				bw1.write(actParams_[6] + "," + actParams_[7] + "," + actParams_[8] + ",");
				bw1.write(actParams_[9] + "," + actParams_[10] + "," + actParams_[11] + ",");
				bw1.write(actParams_[12] + "," + actParams_[13] + "," + actParams_[14] + ",");
				bw1.write(actParams_[15] + "," + actParams_[16]);
				bw1.newLine();
				isFirstAction_[agent1.getObjectID()] = false;
			}

		// パラメータ
			String pos  = "(" + agent1.getFPositionX() + "," + agent1.getFPositionY() + ")";
			String pos1 = agent1.getFPositionX() + "_" + agent1.getFPositionY();
			bw0.write(Parameter.getTurn() + ",");
			bw0.write(actType + ",");
			bw0.write(actNum + ",");
			bw0.write(agent1.getObjectID() + ",");
			bw0.write(oID + ",");
			bw0.write(pos1 + ",");
			bw0.write(sm_.getPositionName(pos) + ",");
			bw0.write(change[0] + "," + change[1] + "," + change[2] + ",");
			bw0.write(change[3] + "," + change[4] + "," + change[5] + ",");
			bw0.write(change[6] + "," + change[7] + "," + change[8] + ",");
			bw0.write(change[9]);
			bw0.newLine();
			bw1.write(Parameter.getTurn() + ",");
			bw1.write(actType + ",");
			bw1.write(actNum + ",");
			bw1.write(agent1.getObjectID() + ",");
			bw1.write(oID + ",");
			bw1.write(pos1 + ",");
			bw1.write(sm_.getPositionName(pos) + ",");
			bw1.write(change[0] + "," + change[1] + "," + change[2] + ",");
			bw1.write(change[3] + "," + change[4] + "," + change[5] + ",");
			bw1.write(change[6] + "," + change[7] + "," + change[8] + ",");
			bw1.write(change[9]);
			bw1.newLine();

			bw0.close();
			osw0.close();
			fos0.close();
			bw1.close();
			osw1.close();
			fos1.close();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}







	public static void saveSceneModel(String[] sParams, HashMap<String, String[]> sModels, int modelNum){
		try{
			String seed = "seed" + Parameter.getRandomSeed();
			String dir  = makeDirectory(seed, "StoryModel");

			String fileName = dir + "Scene.csv";
			FileOutputStream   fos = new FileOutputStream(fileName, false);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);

			for ( int i = 0; i < sParams.length-1; i++ ){
				bw.write(sParams[i] + ",");
			}
			bw.write(sParams[sParams.length-1]);
			bw.newLine();

			int count = 0;
			for ( int i = 0; i < modelNum; i++ ){
				String key = "S-" + (i+1);
				String[] sModel = sModels.get(key);
				if ( sModel == null ){
					continue;
				}
				count++;
				for ( int j = 0; j < sModel.length-1; j++ ){
					bw.write(sModel[j] + ",");
				}
				bw.write(sModel[sParams.length-1]);
				bw.newLine();
				bw.newLine();
			}
			bw.close();
			osw.close();
			fos.close();
			System.out.println("Save " + count + "scene models.");

		} catch ( Exception e){
			e.printStackTrace();
		}
	}



	public static void saveObjectModel(String[] oParams, HashMap<String, String[][]> oModels, int modelNum){
		try{
			String seed = "seed" + Parameter.getRandomSeed();
			String dir  = makeDirectory(seed, "StoryModel");

			String fileName = dir + "Object.csv";
			FileOutputStream   fos = new FileOutputStream(fileName, false);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);

			for ( int i = 0; i < oParams.length-1; i++ ){
				bw.write(oParams[i] + ",");
			}
			bw.write(oParams[oParams.length-1]);
			bw.newLine();

			int count = 0;
			for ( int i = 0; i < modelNum; i++ ){
				String key = "O-" + (i+1);
				String[][] oModel = oModels.get(key);
				if ( oModel == null ){
					continue;
				}
				count++;
				for ( int j = 0; j < oModel.length; j++ ){
					for ( int k = 0; k < oModel[j].length-1; k++ ){
						bw.write(oModel[j][k] + ",");
					}
					bw.write(oModel[j][oParams.length-1]);
					bw.newLine();
				}
				bw.newLine();
			}
			bw.close();
			osw.close();
			fos.close();
			System.out.println("Save " + count + "object models.");

		} catch ( Exception e){
			e.printStackTrace();
		}
	}



	public static void saveRelationModel(String[] rParams, HashMap<String, String[][]> rModels, int modelNum){
		try{
			String seed = "seed" + Parameter.getRandomSeed();
			String dir  = makeDirectory(seed, "StoryModel");

			String fileName = dir + "Relation.csv";
			FileOutputStream   fos = new FileOutputStream(fileName, false);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);

			for ( int i = 0; i < rParams.length-1; i++ ){
				bw.write(rParams[i] + ",");
			}
			bw.write(rParams[rParams.length-1]);
			bw.newLine();

			int count = 0;
			for ( int i = 0; i < modelNum; i++ ){
				String key = "R-" + (i+1);
				String[][] rModel = rModels.get(key);
				if ( rModel == null ){
					continue;
				}
				count++;
				for ( int j = 0; j < rModel.length; j++ ){
					for ( int k = 0; k < rModel[j].length-1; k++ ){
						bw.write(rModel[j][k] + ",");
					}
					bw.write(rModel[j][rParams.length-1]);
					bw.newLine();
				}
				bw.newLine();
			}
			bw.close();
			osw.close();
			fos.close();
			System.out.println("Save " + count + "relation models.");

		} catch ( Exception e){
			e.printStackTrace();
		}
	}



	public static void saveEndType(int type, int sceneNum){
		try{
			String seed = "seed" + Parameter.getRandomSeed();
			String dir  = makeDirectory(seed, "");

			String fileName = dir;
			if ( type == 1 ){
				fileName += "end-" + sceneNum + ".txt";
			}else if ( type == 2 ){
				fileName += "bad-" + sceneNum + ".txt";
			}else if ( type == 3 ){
				fileName += "happy-" + sceneNum + ".txt";
			}else if ( type == 4 ){
				fileName += "nonsense-" + sceneNum + ".txt";
			}

			FileOutputStream   fos = new FileOutputStream(fileName, false);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);
			bw.write("");
			bw.newLine();
			bw.close();
			osw.close();
			fos.close();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}



	public static void saveStory(ArrayList<String[]> sentences){
		try{
			String seed     = "seed" + Parameter.getRandomSeed();
			String dir      = makeDirectory(seed, "");
			String fileName = dir + "story_sentence.txt";

			FileOutputStream   fos = new FileOutputStream(fileName, false);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);

			for ( int i = 0; i < sentences.size(); i++ ){
				String[] sentence = sentences.get(i);
				for ( int j = 0; j < sentence.length; j++ ){
					bw.write(sentence[j]);
					bw.newLine();
				}
				bw.newLine();
			}
			bw.close();
			osw.close();
			fos.close();

		} catch ( Exception e ){
			e.printStackTrace();
		}
	}



	public static void savePicturePath(ArrayList<String> parts, ArrayList<String[]> positions, int number){
		try{
			String seed     = "seed" + Parameter.getRandomSeed();
			String dir      = makeDirectory(seed, "");
			String fileName = dir + "story_picture_" + number + ".txt";

			FileOutputStream   fos = new FileOutputStream(fileName, false);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
			BufferedWriter     bw  = new BufferedWriter(osw);

			while ( positions.size() > 0 ){
				int max = -100;
				int num = 0;
				for ( int i = 0; i < positions.size(); i++ ){
					if ( max < Integer.valueOf(positions.get(i)[4]) ){
						max = Integer.valueOf(positions.get(i)[4]);
						num = i;
					}
				}
				String pos = positions.get(num)[0] + "-"+ positions.get(num)[1] + "-"
						+ positions.get(num)[2] + "-" + positions.get(num)[3];
				bw.write(parts.get(num) + ",");
				bw.write(pos);
				bw.newLine();

				parts.remove(num);
				positions.remove(num);
			}
			bw.close();
			osw.close();
			fos.close();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}



	public static void savePicture(String fName, WritableImage image){
		try{
			String seed     = "seed" + Parameter.getRandomSeed();
			String dir      = makeDirectory(seed, "");
			String fileName = dir + fName + ".png";

			OutputStream os  = new FileOutputStream(fileName);
			ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", os);
			os.close();

		} catch ( Exception e){
			e.printStackTrace();
		}
	}



	public static void saveAnalysis(String fileName, ArrayList<String[]> dataSet){
		try{
			String dir = makeDirectory("Analysis", "");
			fileName = dir + fileName + ".csv";

			File file = new File(fileName);
			if ( !file.exists() ){
				FileOutputStream   fos = new FileOutputStream(fileName, false);
				OutputStreamWriter osw = new OutputStreamWriter(fos, "SJIS");
				BufferedWriter     bw  = new BufferedWriter(osw);

				for ( int i = 0; i < dataSet.size(); i++ ){
					String[] data = dataSet.get(i);
					for ( int j = 0; j < data.length; j++ ){
						bw.write(data[j] + ",");
					}
					bw.newLine();
				}
				bw.close();
				osw.close();
				fos.close();
			}

		} catch ( Exception e){
			e.printStackTrace();
		}
	}

}









