package util;

import java.util.Random;

public class MyRandom {

	private static Random random_ = null;

//シード未指定の場合
	public static Random getInstance(){
		if ( random_ == null){
			random_ = new Random();
		}

		return random_;
	}


//シード指定の場合
	public static Random getInstance(long seed){
		if ( random_ == null){
			random_ = new Random(seed);
		}

		return random_;
	}


//初期化
	public static void init(){
		if ( random_ != null){
			random_ = null;
			System.out.println("MyRandom を初期化しました.");
		}
	}
}
